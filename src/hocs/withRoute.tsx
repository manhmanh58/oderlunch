import { FC, useEffect, useState } from 'react'
import { useRouter } from 'next/router'

interface Options {
  isProtected: boolean
  isHome?: boolean
}

const withRoute = (
  WrappedComponent: FC,
  { isProtected, isHome }: Options = { isProtected: true, isHome: false }
): any => {
  // eslint-disable-next-line react/display-name
  return () => {
    const [mounted, setMounted] = useState(false)
    const router = useRouter()

    // Set mounted status when window loaded
    useEffect(() => {
      setMounted(true)
    }, [])

    if (mounted) {
      const accessToken = localStorage['token']

      //check if accessToken is token and isProtected true then home page
      if (!accessToken && !isProtected) {
        return <WrappedComponent />
      }

      // check if accessToken not token then login
      if (!accessToken) {
        router.push('/login')
        return <div>Loading...</div>
      }

      if (accessToken && !isProtected) {
        const old_url = localStorage.getItem('old_url')

        if (old_url && !isHome) {
          router.push(old_url)
          return <div>Loading...</div>
        }

        if (isHome) {
          return <WrappedComponent />
        }

        return <div>Loading...</div>
      }
      return <WrappedComponent />
    }
  }
}

export default withRoute
