import { BankNumber } from './../styles/profile/edit/index'
import axiosClient from '../api/api'

interface SignUpAuth {
  email: string
  password: string
  fullName?: string
  username?: string
}
interface LoginAuth {
  email: string
  password: string
}
interface SocialAuth {
  idToken: string
  providerId: string | null
}
interface Profile {
  email: string
  username: string
  fullName: string
  avatar: string
  qrCode: string
  bankName: string
  bankNumber: string
  bankOwner: string
  phoneNumber: string
}

const authAPI = {
  login: (data: LoginAuth) => {
    const url = `/api/v1/login`
    return axiosClient.post(url, data)
  },
  register: (data: SignUpAuth) => {
    const url = `/api/v1/signup`
    return axiosClient.post(url, data)
  },
  withSocial: (data: SocialAuth) => {
    const url = `/api/v1/login/social`
    return axiosClient.post(url, data)
  },
  updateProfile: (data: Profile) => {
    const url = `/api/v1/users/me`
    return axiosClient.patch(url, data)
  },

  getProfile: () => {
    const url = `/api/v1/users/me`
    return axiosClient.get(url)
  },
  getUserProfile: (id: string) => {
    const url = `/api/v1/users/${id}`
    return axiosClient.get(url)
  },
}

export default authAPI
