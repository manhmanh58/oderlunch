import axiosClient from '../api/api'

const reportAPI = {
  getPosted: () => {
    const url = `/api/v1/users/me/posts`
    return axiosClient.get(url)
  },
  getBooked: () => {
    const url = `/api/v1/users/me/orders`
    return axiosClient.get(url)
  },
  getPostsSaved: ()=>{
    const url = `/api/v1/users/me/posts?saved=true`
    return axiosClient.get(url)
  }
}

export default reportAPI
