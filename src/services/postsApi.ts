import axiosClient from '../api/api'
import { Dishes } from 'utils/type/dishes'

interface CreatePost {
  title: string
  endTime: string
  thumbnail?: {
    url?: string | null
  }
  dishes: Array<{
    name: string
    price: number
    image?: {
      url?: any
    }
  }>
}

interface Image {
  url: string
  type: string
}

interface UploadImage {
  data: Array<Image>
  id: string
}

interface CreateOrder {
  note: string
  dishes: Array<{
    dishId: number
    quantity: number
  }>
}

const postsApi = {
  create: (data: CreatePost) => {
    const url = `/api/v1/posts`
    return axiosClient.post(url, data)
  },
  uploadImage: ({ data, id }: UploadImage) => {
    const url = `/api/v1/${id}/images`
    return axiosClient.post(url, data)
  },
  getList: (page: number) => {
    const url = `/api/v1/posts?page=${page}`
    return axiosClient.get(url)
  },
  getDetail: (id: string) => {
    const url = `/api/v1/posts/${id}`
    return axiosClient.get(url)
  },
  createOrder: (orders: { postId: string; data: CreateOrder }) => {
    const url = `/api/v1/posts/${orders.postId}/orders`
    return axiosClient.post(url, orders.data)
  },
  getOrdersOfUserByPost: (id: string) => {
    const url = `/api/v1/orders/${id}`
    return axiosClient.get(url)
  },
  getOrdered: (id: string) => {
    const url = `/api/v1/posts/${id}/orders`
    return axiosClient.get(url)
  },
  changeStatus: (id: string, body: { status: string }) => {
    const url = `/api/v1/orders/${id}`
    return axiosClient.patch(url, body)
  },
  deletePost: (id: number) => {
    const url = `/api/v1/posts/${id}`
    return axiosClient.delete(url)
  },
  updatePost: (
    id: string,
    body: {
      title?: string
      endTime?: string
      thumbnail?: {
        url?: string | null
      }
      saved?: boolean
    }
  ) => {
    const url = `/api/v1/posts/${id}`
    return axiosClient.patch(url, body)
  },
  deleteDishes: (id: number) => {
    const url = `/api/v1/posts/dishes/${id}`
    return axiosClient.delete(url)
  },
  deleteOrder: (id: string) => {
    const url = `/api/v1/orders/${id}`
    return axiosClient.delete(url)
  },
  updateDishes: (
    id: number,
    body: {
      name: string
      price: number
      image: {
        url?: string | null
      }
    }
  ) => {
    const url = `/api/v1/posts/dishes/${id}`
    return axiosClient.patch(url, body)
  },
}

export default postsApi
