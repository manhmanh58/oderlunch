// this script is used for running the server with https
import { createServer } from 'https'
import fs from 'fs'
import next from 'next'
import { parse } from 'url'
import { join } from 'path'

const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()
const httpsOptions = {
  key: fs.readFileSync(join(process.cwd(), 'src/ssl/orderofficelunch.pem')),
  cert: fs.readFileSync(join(process.cwd(), 'src/ssl/orderofficelunch.crt')),
}

app.prepare().then(() => {
  createServer(httpsOptions, (req: any, res) => {
    const parsedUrl = parse(req.url, true)
    handle(req, res, parsedUrl)
  }).listen(8080, () => {
    console.log('> Server started on https://localhost:8080')
  })
})
