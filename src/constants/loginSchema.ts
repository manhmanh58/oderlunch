import * as yup from 'yup'

export const loginSchema_ = yup.object().shape({
  email: yup
    .string()
    .email('registerSchema.email')
    .required('registerSchema.emailRequired')
    .max(40, 'registerSchema.emailMax')
    .min(8, 'registerSchema.emailMin')
    .matches(/^[\w\.]+@([\w]+\.)+[\w]{2,4}$/g, 'registerSchema.email'),

  password: yup
    .string()
    .required('loginSchema.password')
    .min(8, 'loginSchema.passwordMin')
    .max(20, 'loginSchema.passwordMax')
    .matches(
      /^(?!.* )(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*])(?=.{8,})/,
      'loginSchema.passwordFormat'
    ),
})
