import * as yup from 'yup'
import { VALIDATE_STRING_VN } from '../Regex/regex'

export const registerSchema = yup.object().shape({
  fullName: yup
    .string()
    .required('registerSchema.fullName')
    .max(50, 'registerSchema.fullNameMax')
    .min(3, 'registerSchema.fullNameMin')
    .matches(VALIDATE_STRING_VN, 'registerSchema.fullNameMax'),

  username: yup
    .string()
    .required('registerSchema.userName')
    .min(8, 'registerSchema.userNameMin')
    .max(50, 'registerSchema.useNameMax')
    .matches(/^[a-zA-Z0-9]+$/, 'registerSchema.nameValidate'),
  email: yup
    .string()
    .email('registerSchema.email')
    .required('registerSchema.emailRequired')
    .max(40, 'registerSchema.emailMax')
    .min(8, 'registerSchema.emailMin')
    .matches(/^[\w\.]+@([\w]+\.)+[\w]{2,4}$/g, 'registerSchema.email'),

  password: yup
    .string()
    .required('registerSchema.password')
    .min(8, 'registerSchema.passwordMin')
    .max(20, 'registerSchema.passwordMax')
    .matches(
      /^(?!.* )(?=.*[!@#$%^&*])(?=.*[A-Z])(?=.*[a-z]).{8,20}$/,

      'registerSchema.passwordFormat'
    ),
  confirmPassword: yup
    .string()
    .required('registerSchema.comfirmRequired')
    .oneOf([yup.ref('password'), null], 'registerSchema.comfirmPwd'),
})
