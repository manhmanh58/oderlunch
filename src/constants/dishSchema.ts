import * as yup from 'yup'
import { VALIDATE_STRING_VN } from '../Regex/regex'

export const dishSchema = yup.object({
  name: yup
    .string()
    .required('createPost.food')
    .min(8, 'postSchema.min')
    .max(50, 'postSchema.max')
    .matches(VALIDATE_STRING_VN, 'postSchema.max'),

  price: yup
    .string()
    .required('createPost.priceRequire')
    .matches(/^(?!0)[0-9]{1,}$/g, 'createPost.invalidPrice'),
})
