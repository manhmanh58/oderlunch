import * as yup from 'yup'
import valid from 'card-validator'
import { VALIDATE_STRING_VN } from '../Regex/regex'

export const editSchema = yup.object().shape({
  username: yup
    .string()
    .required('registerSchema.userName')
    .min(8, 'registerSchema.userNameMin')
    .max(50, 'registerSchema.useNameMax')
    .matches(/^[a-zA-Z0-9]+$/, 'registerSchema.nameValidate'),
  fullName: yup
    .string()
    .required('registerSchema.fullName')
    .max(50, 'registerSchema.fullNameMax')
    .min(3, 'registerSchema.fullNameMax')
    .matches(VALIDATE_STRING_VN, 'registerSchema.fullNameMax'),
  email: yup.string().email('editSchema.email').max(40, 'editSchema.max'),
  phoneNumber: yup
    .string()
    .required('editSchema.phoneNum')
    .matches(/(84|0[1|2|3|5|7|8|9])+([0-9]{8})\b/, {
      message: 'editSchema.phone',
      excludeEmptyString: true,
    })
    .max(10, 'editSchema.phoneMax'),
  bankNumber: yup
    .string()
    .required('editSchema.bankNum')
    .matches(/^(\d{3,15})$/, {
      message: 'Số tài khoản không hợp lệ',
      excludeEmptyString: true,
    }),
  bankOwner: yup
    .string()
    .required('editSchema.bankOwner')
    .min(8, 'editSchema.bankVali')
    .max(50, 'editSchema.bankVali')
    .matches(/^(?!\s)[A-Z ]+$/, 'editSchema.bankVali'),
  bankName: yup
    .string()
    .required('editSchema.bank')
    .max(50, 'editSchema.bankName')
    .min(3, 'editSchema.bankName')
    .matches(/^(?!\s)[A-Z ]+$/, 'editSchema.bankName'),
})
