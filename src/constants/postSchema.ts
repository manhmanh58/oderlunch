import * as yup from 'yup'
import { VALIDATE_STRING_VN } from '../Regex/regex'

const timeCheck = (value: string | undefined) => {
  if (value) {
    let currentTime = new Date()
    let hourNow = currentTime.getHours()
    let minutesNow = +currentTime.getMinutes() + +hourNow * 60
    let [hourSplit, minutesSplit] = value.split(':')
    let minutesPost = +hourSplit * 60 + +minutesSplit
    return minutesPost - minutesNow <= 30 ? false : true
  }
  return false
}

export const postSchema = yup.object({
  title: yup
    .string()
    .required('createPost.required')
    .min(8, 'postSchema.min')
    .max(50, 'postSchema.max')
    .matches(VALIDATE_STRING_VN, 'postSchema.max'),
  endTime: yup
    .string()
    .required('postSchema.time')
    .test('endTime', 'postSchema.validate', (value: string | undefined) => {
      return timeCheck(value)
    }),
})
