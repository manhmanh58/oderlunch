import styled from 'styled-components'

export const FormLogin = styled.form`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
  background-color: gray;
  background-image: -webkit-linear-gradient(top left, #a802e0, #f7676a);
`
export const StyleLogin = styled.div`
  padding: 0 60px;
  margin: 20px auto;
  text-align: center;
  width: 100%;
  max-width: 450px;
  min-height: 500px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  border-radius: 25px;
  box-shadow: 0 2px 8px rgb(0 0 0 / 50%);
  background-color: #fff;
`

export const LoginDiv = styled.div`
  margin-bottom: 40px;
  font-weight: bold;
  height: 50px;
  font-size: 40px;
  color: #000;
  -webkit-box-reflect: below -10px linear-gradient(transparent, rgba(0, 0, 0, 0.3));
  letter-spacing: 0.1em;
`
export const SpanLogin = styled.span`
  display: inline-block;
  animation: wave-anim 1s infinite;
  animation-delay: calc(0.1s * var(--item));
  @keyframes wave-anim {
    0%,
    40%,
    100% {
      transform: translateY(0);
    }
    20% {
      transform: translateY(-20px);
    }
  }
`
export const Heading = styled.div`
  padding: 7.5px 0;
`

export const LabelLogin = styled.div`
  display: block;
  font-size: 17px;
  color: #000000;
  font-weight: 500;
  text-align: left;
  padding-bottom: 5px;
`
export const ContentInput = styled.div`
  white-space: nowrap;
  position: relative;
`
export const Input = styled.input`
  width: 100%;
  border: none;
  border-bottom: 2px solid #000000;
  outline: none;
  font-size: 14px;
  background-color: transparent;
  color: #000000;
  text-align: left;
`
export const SpanEye = styled.p`
  position: absolute;
  z-index: 1;
  left: 90%;
  top: 0;
  cursor: pointer;
  svg {
    height: 17px;
  }
`
export const Button = styled.button`
  margin: 15px auto;
  border: none;
  outline: none;
  width: 100%;
  height: 100%;
  text-align: center;
  padding: 5px 0;
  color: #ffffff;
  font-size: 20px;
  letter-spacing: 1px;
  background-color: #004e9a;
  cursor: pointer;
  border-radius: 50px;
  font-weight: bold;
  letter-spacing: 0.08em;
  transition: all 500ms ease-in-out;
  &:hover {
    background-color: #428cd4;
    color: #000;
  }
`
export const StyleIcons = styled.div``
export const SignUp = styled.div`
  color: #050831;
  font-weight: bold;
  font-size: 16px;
  padding-bottom: 15px;
`
export const Icons = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  gap: 30px;
  margin: auto;
  width: 35px;
  height: 35px;
  text-align: center;
`
export const IconsFB = styled.div`
  color: #ffffff;
  background-color: blue;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid blue;
  border-radius: 10px;
  padding: 5px 10px;
  transition: all 500ms ease-in-out;
  &:hover {
    color: blue;
    background-color: #ffffff;
    border: 1px solid orange;
  }
`
export const IconsGG = styled.div`
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid #ff0000;
  border-radius: 10px;
  padding: 5px 25px;
  color: #ffffff;
  background-color: #ff0000;
  box-shadow: 0 2px 8px rgb(0 0 0 / 20%);
  transition: all 500ms ease-in-out;
  &:hover {
    color: #ff0000;
    background-color: #fff;
    border: 1px solid orange;
  }
`

export const BtnLogin = styled.div`
  display: flex;
  justify-content: center;
  padding-top: 15px;
`
export const Option = styled.div`
  color: #000000;
  padding: 10px 0;
`

export const Question = styled.div`
  font-size: 13px;
  display: flex;
  justify-content: flex-end;
  align-items: center;
`
export const P = styled.p`
  padding: 0 0 0 5px;
  margin: 0;
`
export const LinkAuth = styled.div`
  color: #7e1037;
  font-weight: 700;
  letter-spacing: 0.5px;
  margin-left: 5px;
  cursor: pointer;
  transition: all 400ms ease-in-out;
  &:hover {
    color: red;
  }
`
