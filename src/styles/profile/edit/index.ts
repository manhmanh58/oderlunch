import { TextField } from '@mui/material'
import styled from 'styled-components'
import Button from '@mui/material/Button'

export const ContainerProfile = styled.div`
  margin: 0 20px;
  height: 100%;
  display: flex;
`
export const ContentRight = styled.div`
  width: 35%;
  margin: auto 0;
  display: flex;
  justify-content: center;
  align-items: center;
`
export const ContentLeft = styled.div`
  width: 65%;
  border-right: 0.0625rem solid #efefef;
  height: 100%;
  padding-right: 10px;
`
export const StyleTitle = styled.div`
  border-bottom: 0.0625rem solid #efefef;
  padding: 10px 0;
`
export const TextInfo = styled.div`
  font-size: 14px;
  line-height: 1.0625rem;
  color: #888;
`
export const Title = styled.h1`
  padding-bottom: 3px;
  font-size: 1.3rem;
  font-weight: 700;
  line-height: 1.5rem;
  color: rgb(51, 51, 51);
`
export const FormEdit = styled.form`
  padding: 10px 0;
`
export const Edit = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  padding: 0 200px 5px 0;
`
export const EditInput = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;
  padding: 10px 0;
`
export const FieldInputInfo = styled(TextField)`
  width: 80%;
  && {
    .Mui-disabled {
      color: #060606;
      -webkit-text-fill-color: #060606;
    }
  }
`
export const FieldInput = styled(TextField)`
  width: 80%;
  && {
    .Mui-disabled {
      color: #060606;
      -webkit-text-fill-color: #060606;
    }
  }
`
export const Label = styled.div`
  font-size: 13px;
  width: 19%;
  text-align: right;
  color: rgba(85, 85, 85, 0.8);
  text-transform: capitalize;
  overflow: hidden;
  padding-right: 10px;
`
export const LabelUser = styled.div`
  font-size: 13px;
  width: 26%;
  text-align: right;
  color: rgba(85, 85, 85, 0.8);
  text-transform: capitalize;
  overflow: hidden;
  padding-right: 10px;
`
export const Text = styled.span`
  width: 75%;
  font-size: 0.875rem;
  color: #333;
`
export const TextEmail = styled.span`
  width: 75%;
  font-size: 0.875rem;
  color: #333;
  padding-left: 32px;
`
export const TitleBank = styled.div`
  text-align: center;
  font-weight: bold;
  font-size: 17px;
`
export const Input = styled(TextField)`
  font-size: 0.875rem;
  color: #333;
  width: 75%;
`
export const InputBank = styled.input`
  margin-left: 10px;
  font-size: 0.875rem;
  color: #333;
  width: 60%;
`
export const Image = styled.img`
  width: 280px;
  height: 250px;
`
export const StyleButton = styled.div`
  display: flex;
  justify-content: center;
  margin: 5px auto;
`
export const ButtonSave = styled.button`
  color: #fff;
  overflow: visible;
  outline: 0px;
  background-color: orange;
  width: 180px;
  height: 40px;
  text-overflow: ellipsis;
  -webkit-line-clamp: 1;
  flex-direction: column;
  font-size: 14px;
  box-sizing: border-box;
  box-shadow: rgb(0 0 0 / 9%) 0px 1px 1px 0px;
  border: 0px;
  display: flex;
  align-items: center;
  justify-content: center;
  text-transform: capitalize;
  outline: 0px;
  cursor: pointer;
  font-weight: bold;
  transition: all 400ms ease-in-out;
  border-radius: 20px;
  &:hover {
    background: #fff;
    color: orange;
    border: 2px solid orange;
  }
`
export const ButtonDisable = styled.button`
  color: #fff;
  overflow: visible;
  outline: 0px;
  background-color: #c6c6c6;
  width: 180px;
  height: 40px;
  text-overflow: ellipsis;
  -webkit-line-clamp: 1;
  flex-direction: column;
  font-size: 14px;
  box-sizing: border-box;
  box-shadow: rgb(0 0 0 / 9%) 0px 1px 1px 0px;
  border: 0px;
  display: flex;
  align-items: center;
  justify-content: center;
  text-transform: capitalize;
  outline: 0px;
  cursor: pointer;
  font-weight: bold;
  transition: all 400ms ease-in-out;
  border-radius: 20px;
`
export const Bank = styled.div`
  width: 100%;
  display: flex;
  border-top: 0.0625rem solid #efefef;
  padding: 10px 0;
`
export const BankNumber = styled.div`
  width: 100%;
`
export const BankCode = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 100%;
`

export const StyleImage = styled.div`
  display: flex;
  justify-content: center;
  padding-top: 15px;
`
export const StyleButtonImage = styled(StyleButton)`
  display: flex;
  justify-content: center;
`
export const ButtonImage = styled(Button)`
  width: 150px;
  outline: 0px;
  background: rgb(255, 255, 255);
  color: rgb(85, 85, 85);
  border: 1px solid rgba(0, 0, 0, 0.09);
  box-shadow: rgb(0 0 0 / 3%) 0px 1px 1px 0px;
  overflow: visible;
  font-weight: 0;
  border-radius: 10px;
  text-align: center;
`
export const InputFile = styled.input`
  width: 200px;
`
export const InputQR = styled.input`
  width: 200px;
  padding-top: 10px;
  margin: auto;
`
export const Warning = styled.div`
  margin-top: 0.75rem;
  display: block;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
`
export const Type = styled.div`
  display: block;
  width: 76%;
  color: red;
  font-size: 13.5px;
  padding: 5px 0;
`
export const DataFile = styled.div`
  color: #b6b4c2;
  font-size: 0.875rem;
  line-height: 1.25rem;
`
export const TextSpan = styled.span`
  font-size: 10px;
  width: 100%;
`
export const StyleIcon = styled.div`
  position: absolute;
  left: 44%;
  display: flex;
  align-items: center;
  cursor: pointer;
  color: #05a;
  font-size: 0.8125rem;
  text-transform: capitalize;
  &:hover {
    color: red;
  }
`
export const ButtonImg = styled.button`
  cursor: pointer;
  padding: 0;
  transition: all 200ms;
  background-color: #fff;
  border: 0.0625rem solid #efefef;
  &:hover {
    opacity: 0.3;
    transform: scale(0.95);
  }
`

export const Error = styled.div`
  display: block;
  width: 76%;
  color: red;
  font-size: 13.5px;
  padding: 5px 0;
`
export const EditError = styled.div`
  display: block;
  width: 76%;
  color: red;
  font-size: 13.5px;
  padding: 5px 0;
  margin-left: -25px;
`
