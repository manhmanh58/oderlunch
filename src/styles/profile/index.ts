import styled from 'styled-components';

export const Container = styled.div`
  height: 88vh;
  padding: 20px 0 20px;
  width: 100%;
  display: flex;
  background-color: #d9ecf2;
`;

export const Details = styled.div`
  width: 23%;
  box-shadow: 0 2px 8px rgb(0 0 0 / 15%);
  border-radius: 20px;
  margin-left: 20px;
  background-color: #fafafa;
`;

export const Content = styled.div`
  background-color: #fafafa;
  margin: 0 15px;
  background-repeat: no-repeat;
  width: 77%;
  padding: 10px 0;
  box-shadow: 0 2px 8px rgb(0 0 0 / 25%);
  border-radius: 15px;
  overflow-y: auto ;
`;
