import styled from 'styled-components';

export const Info = styled.div`
  width: 100%;
  height: 100%;
  margin: auto;
  font-family: 'Inter';
`;
export const StyleContent = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
`;
export const StyleImage = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 150px;
`;
export const Image = styled.img`
  height: 140px;
  border-radius: 50%;
`;
export const ContentInfo = styled.div`
  font-family: 'Poppins';
`;
export const Title = styled.span`
  padding-left: 70px;
  color: gray;
`;
export const TitleContent = styled.h1`
  font-size: 25px;
  text-align: center;
  font-weight: bold;
`;
export const InfoTitle = styled.h1`
  text-align: center;
`;
