import styled from 'styled-components'
import TableCell from '@mui/material/TableCell'

export const StyleAddImage = styled.label`
  margin: 0;
  border-radius: 8px;
  display: flex;
  align-items: center;
  justify-content: center;
  background: #cecece;
  height: 50px;
  width: 75px;
  cursor: pointer;
  img {
    width: 75px;
    height: 50px;
    border-radius: 8px;
    object-fit: cover;
  }
  input {
    display: none;
  }
`

export const StyleTableCell = styled(TableCell)`
  text-align: left;
`

export const StyleTableCellName = styled.div`
  font-family: 'Roboto', 'Helvetica', 'Arial', sans-serif;
  font-weight: 400;
  font-size: 0.875rem;
  line-height: 1.43rem;
  letter-spacing: 0.01071em;
  display: table-cell;
  vertical-align: inherit;
  border-bottom: 1px solid rgba(224, 224, 224, 1);
  text-align: left;
  padding: 6px 16px;
  color: rgba(0, 0, 0, 0.87);
  font-weight: 700;
`

export const Title = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-left: 8px;
  font-weight: 600;
`

export const Order = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
`
export const Icons = styled.div`
  display: flex;
  flex-direction: row;
`
