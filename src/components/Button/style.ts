import { Button } from "@mui/material";
import styled from "styled-components";

const StyleButton = styled(Button)`
  background-color: ${(props) => !props.color && props.theme.colors.primary}; 
  color: #FFF;
  &:hover {
    background-color: ${(props) => !props.color && props.theme.colors.primary};
  }
`;

export { StyleButton };
