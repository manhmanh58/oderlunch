import React from "react";
import { StyleButton } from "./style";

export default function Button(props: any) {
  return <StyleButton {...props}>{props.children}</StyleButton>;
}
