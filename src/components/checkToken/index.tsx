import React from 'react'
import Login from '@/src/pages/login'
import LandingPage from '../pages/LandingPage'

export default function CheckToken() {
  const isLoggedIn = localStorage['token']

  return (
    <div>
      {!isLoggedIn ? (
        <div>
          <Login />
        </div>
      ) : (
        <div>
          <LandingPage />
        </div>
      )}
    </div>
  )
}
