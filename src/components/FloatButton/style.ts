import styled from "styled-components";

const StyleFloatButton = styled.div`
  position: fixed;
  bottom: 30px;
  right: 30px;
  width: 60px;
  height: 60px;
  background: ${(props: any) => props.theme.colors.primary};
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  opacity: 0.9;
  transition: all 0.5s ease-in-out;
  box-shadow: 0 0 30px rgb(97 96 136 / 12%);
  svg {
    fill: #fff;
    font-size: 30px;
  }
  &:hover {
    opacity: 1;
  }
`;

export { StyleFloatButton };
