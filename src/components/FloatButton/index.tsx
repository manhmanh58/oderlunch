import React from "react";
import { StyleFloatButton } from "./style";
import AddIcon from "@mui/icons-material/Add";
import { useRouter } from "next/router";

export default function FloatButton() {
  const router = useRouter()
  return (
    <StyleFloatButton onClick={() => router.push("/create-post")}>
      <AddIcon />
    </StyleFloatButton>
  );
}
