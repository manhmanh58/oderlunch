import React, { ReactNode, useRef, useState } from 'react'
import { StyleDropdown, StyleDropdownItem } from './style'
import Link from 'next/link'
import { useRouter } from 'next/router'
import useOnClickOutside from '@/src/hooks/useOnClickOutside'

interface Dropdown {
  children: ReactNode
}
const Dropdown = ({ children }: Dropdown) => {
  const [isOpen, setIsOpen] = useState(false)
  const { locale, locales, asPath } = useRouter()
  const refMore = useRef(null)

  useOnClickOutside(refMore, () => {
    setIsOpen(false)
  })

  return (
    <div
      onClick={() => setIsOpen(!isOpen)}
      style={{ display: 'flex' }}
      ref={refMore}
    >
      {children}
      {isOpen && (
        <StyleDropdown>
          {locales?.map((lang: any, i: number) => {
            return (
              <span key={i}>
                <StyleDropdownItem>
                  <Link href={asPath} locale={lang}>
                    {lang === 'vi' ? 'Vietnamese' : 'English'}
                  </Link>
                </StyleDropdownItem>
              </span>
            )
          })}
        </StyleDropdown>
      )}
    </div>
  )
}

export default Dropdown
