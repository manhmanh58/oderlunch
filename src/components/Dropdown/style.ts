import styled from 'styled-components'

const StyleDropdown = styled.div`
  overflow: visible;
  z-index: 12;
  top: 65px;
  left: 0px;
  box-sizing: content-box;
  display: block;
  position: absolute;
  background-color: #fff;
  border: 1px solid rgba(0, 0, 0, 0.15);
  border-radius: 0.25rem;
  &::before {
    content: ' ';
    position: absolute;
    top: -16px;
    left: 4px;
    border: solid transparent;
    border-width: 8px;
    border-bottom-color: #fff;
  }
`
const StyleDropdownItem = styled.div`
  color: black;
  box-sizing: border-box;
  cursor: pointer;
  font-size: 1rem;
  margin: 8px 0;
  z-index: 1;
  &:hover {
    background-color: #f8f9fa;
  }
  a {
    color: #000;
    font-weight: 400;
    padding: 3px 1.5rem;
    text-align: left;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
`
export { StyleDropdown, StyleDropdownItem }
