import styled from "styled-components";

const StyleContainer = styled.div`
    position: relative;
    width: 1280px;
    padding: 40px 24px;
    margin: auto;
`;

export {
    StyleContainer
}