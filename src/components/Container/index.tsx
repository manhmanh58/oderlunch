import React from 'react'
import { StyleContainer } from './style'

export default function index({children}:{children:any}) {
  return (
    <StyleContainer>{children}</StyleContainer>
  )
}
