import styled from "styled-components";

const StyleSelect = styled.select`
  border: 1px solid ${props=>props.theme.colors.primary};
  border-radius: 10px;
  padding: 10px;
  &:focus{
    outline:none;
  }
`

export {
  StyleSelect
}