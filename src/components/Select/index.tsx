import React from 'react'
import { StyleSelect } from './styles'

type SelectProps = {
  children: JSX.Element
  [x:string]: any;
}

export default function Select({children, ...others}:SelectProps) {
  return (
    <StyleSelect {...others}>
      {children}
    </StyleSelect>
  )
}
