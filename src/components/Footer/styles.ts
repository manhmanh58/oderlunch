import styled from 'styled-components'

const StyleFooter = styled.div`
  background: ${(props) => props.theme.colors.background};

  padding: 30px;
`

const StyleCopyRight = styled.div`
  font-size: ${(props) => props.theme.fontSize.sm};
  display: flex;
  align-items: center;
  justify-content: space-between;
`

const StyleSocialIcons = styled.div`
  display: flex;
  align-items: center;
  a {
    background: ${(props) => props.theme.colors.text};
    padding: 8px;
    border-radius: 5px;
    display: flex;
    align-items: center;
    svg {
      width: 20px;
      height: 20px;
      fill: #fff;
    }
  }
  a + a {
    margin-left: 10px;
  }
`

export { StyleFooter, StyleCopyRight, StyleSocialIcons }
