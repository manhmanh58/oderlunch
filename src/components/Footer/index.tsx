import React from 'react'
import { StyleFooter, StyleCopyRight, StyleSocialIcons } from './styles'
import FacebookIcon from '@mui/icons-material/Facebook'
import InstagramIcon from '@mui/icons-material/Instagram'
export default function Footer() {
  return (
    <StyleFooter>
      <StyleCopyRight>
        <p>© 2022 All Rights Reserved.</p>
        <StyleSocialIcons>
          <a
            href="https://www.facebook.com/"
            target={'_blank'}
            rel={'noreferrer'}
          >
            <FacebookIcon />
          </a>
          <a
            href="https://www.instagram.com/"
            target={'_blank'}
            rel={'noreferrer'}
          >
            <InstagramIcon />
          </a>
        </StyleSocialIcons>
      </StyleCopyRight>
    </StyleFooter>
  )
}
