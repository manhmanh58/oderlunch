import React from "react";
import {
  StyleSearch,
  StyleIntruction,
  StyleKeyWordContainer,
  StyleKeyWord,
} from "./styles";
import CloseIcon from "@mui/icons-material/Close";
import SearchIcon from "@mui/icons-material/Search";
import Popup from "../Popup";

interface SerachModalProps {
  setModalOpen: Function;
}

const SearchModal = ({ setModalOpen }: SerachModalProps) => {

  return (
    <Popup setModalOpen={setModalOpen}>
      <StyleSearch>
        <SearchIcon />
        <input type="text" placeholder="Tìm Kiếm" autoFocus />
        <CloseIcon onClick={() => setModalOpen(false)} />
      </StyleSearch>
      <StyleIntruction>Từ khóa phổ biến</StyleIntruction>
      <StyleKeyWordContainer>
        <StyleKeyWord>Test 1</StyleKeyWord>
        <StyleKeyWord>Test 2</StyleKeyWord>
      </StyleKeyWordContainer>
    </Popup>
  );
};

export default SearchModal;
