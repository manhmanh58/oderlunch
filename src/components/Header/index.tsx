import React, { useEffect, useRef } from 'react'
import {
  StyleHeader,
  StyleMenuItems,
  StyleProfile,
  StyleAvatar,
  StylePadding,
  StyleIcon,
  ListDetails,
  Profile,
  Span,
  AvatarDetails,
  Logout,
  Icons,
  HeaderProfile,
  IconLogout,
  ImageLogo,
} from './styles'
import SearchModal from './SearchModal'
import { useState } from 'react'
import SearchIcon from '@mui/icons-material/Search'
import TranslateIcon from '@mui/icons-material/Translate'
import { useRouter } from 'next/router'
import Avatar from '@mui/material/Avatar'
import { useAuthValue } from '../authContext/AuthContext'
import useHover from '@/src/hooks/useHover'
import Loading from '../Loading'
import logo from '/public/images/logoo.png'
import Dropdown from '../Dropdown'
import { FormattedMessage } from 'react-intl'
import useOnClickOutside from '@/src/hooks/useOnClickOutside'
import useLocalStorage from '@/src/hooks/useLocalStorage'
const Header = () => {
  const [modalOpen, setModalOpen] = useState(false)
  const router = useRouter()
  const [isLoggedIn, setIsLoggedIn] = useState()
  const [loading, setLoading] = useState(false)
  const { profile, clearProfile } = useAuthValue()
  const refMore = useRef(null)
  const [isOpen, setIsOpen] = useState(false)
  const [lsOrdereds, lsSetOrdereds] = useLocalStorage('ordereds', [])

  useOnClickOutside(refMore, () => {
    setIsOpen(false)
  })
  const logout = async () => {
    setLoading(true)
    localStorage.removeItem('token')
    await router.push('/login', undefined, { scroll: true })
    setIsOpen(false)
    clearProfile()
    lsSetOrdereds([])
    setLoading(false)
  }

  const handleProfile = async () => {
    setLoading(true)
    const timer = setTimeout(() => {
      setLoading(false)
      window.scrollTo(0, 0)
    }, 500)
    setIsOpen(false)
    router.push('/profile')
    return () => clearTimeout(timer)
  }

  useEffect(() => {
    if (typeof window !== 'undefined') {
      // Perform localStorage action
      return setIsLoggedIn(localStorage['token'])
    }
  })

  return (
    <>
      <StyleHeader>
        {loading && <Loading />}
        <StyleMenuItems>
          <a href={`/${router.locale}`}>
            <ImageLogo src={logo.src} alt="logo" />
          </a>
        </StyleMenuItems>
        <StyleProfile>
          <Dropdown>
            <StyleIcon>
              <TranslateIcon />
            </StyleIcon>
          </Dropdown>
          <StyleIcon>
            <SearchIcon>
              <SearchIcon onClick={() => setModalOpen(true)} />
            </SearchIcon>
          </StyleIcon>
          <StyleAvatar ref={refMore}>
            <AvatarDetails onClick={() => setIsOpen(!isOpen)}>
              <Avatar
                src={profile?.avatar}
                sx={{ width: 35, height: 35, marginRight: 0 }}
              />
            </AvatarDetails>
            {isOpen && (
              <ListDetails>
                <HeaderProfile>
                  <Avatar
                    src={profile?.avatar}
                    sx={{ width: 35, height: 35, marginRight: 0 }}
                  />
                  <Span>{profile?.fullName}</Span>
                </HeaderProfile>
                <Profile onClick={handleProfile}>
                  {' '}
                  <FormattedMessage id="header.profile" />
                </Profile>
                {!isLoggedIn ? (
                  <></>
                ) : (
                  <Icons onClick={logout}>
                    <Logout>
                      {' '}
                      <FormattedMessage id="header.logOut" />
                    </Logout>
                    <IconLogout />
                  </Icons>
                )}
              </ListDetails>
            )}
          </StyleAvatar>
        </StyleProfile>
      </StyleHeader>
      <StylePadding />
      {modalOpen && <SearchModal setModalOpen={setModalOpen} />}
    </>
  )
}

export default Header
