import authAPI from '@/src/services/AuthAPI'
import React, {
  useContext,
  createContext,
  ReactNode,
  useEffect,
  useState,
} from 'react'

const AuthContext = createContext({})

type Props = {
  children?: ReactNode
  value?: any
}

interface CurrentUser {
  avatar?: string
  bankNumber?: number
  bankOwner?: string
  email?: string
  fullName?: string
  phoneNumber?: number
  qrCode?: string
  username?: string
  bankName?: string
  name?: any
}

export function AuthProvider({ children, value }: Props) {
  const [data, setData] = useState<CurrentUser | null>(null)
  const [details, setDetails] = useState<CurrentUser | null>(null)
  const [urlAvt, setUrlAvt] = useState('')
  const [urlQR, setUrlQR] = useState('')

  useEffect(() => {
    handleSaveChanges
    getProfile()
  }, [])

  const handleSaveChanges = async (dt: any) => {
    dt.email = data?.email
    dt.username = data?.username
    dt.avatar = urlAvt
    dt.qrCode = urlQR
    const result = await authAPI.updateProfile(dt)
    setDetails(result.data.currentUser)
  }

  const getProfile = () => {
    const getCharacters = async () => {
      const response = await authAPI.getProfile()
      setData(response.data.currentUser)
      setUrlAvt(response.data.currentUser.avatar)
      setUrlQR(response.data.currentUser.qrCode)
    }
    getCharacters()
      // make sure to catch any error
      .catch(console.error)
  }

  const clearProfile = () => {
    setData(null)
  }

  return (
    <AuthContext.Provider
      value={{
        details: details,
        profile: data,
        getProfile,
        clearProfile,
      }}
    >
      {children}
    </AuthContext.Provider>
  )
}

export function useAuthValue(): any {
  return useContext(AuthContext)
}
