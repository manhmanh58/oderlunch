import styled from 'styled-components'

const StyleDeleteBtn = styled.div`
  cursor: pointer;
  color: ${(props: any) => props.theme.colors.danger};
  opacity: 0.7;
  &:hover {
    opacity: 1;
  }
  margin-bottom: 35px;
`

const StyleAddImage = styled.label`
  margin: auto;
  border-radius: 8px;
  display: flex;
  align-items: center;
  justify-content: center;
  background: #cecece;
  height: 75px;
  width: 138px;

  cursor: pointer;
  img {
    width: 138px;
    height: 75px;
    border-radius: 8px;
  }
  input {
    display: none;
  }
`

export { StyleDeleteBtn, StyleAddImage }
