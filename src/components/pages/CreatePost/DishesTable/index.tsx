import React, { useState } from 'react'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import Paper from '@mui/material/Paper'
import Button from 'components/Button'
import AddIcon from '@mui/icons-material/Add'
import Dish from './Dish'
import { Dishes } from 'utils/type/dishes'
import { FormattedMessage } from 'react-intl'
type DishesTableProps = {
  dishes: Array<Dishes>
  addDishes: Function
  deleteDishes: Function
  onChangeValue: Function
  setDisheError: Function
  dishesError: String
}

export default function DishesTable({
  dishes,
  addDishes,
  deleteDishes,
  onChangeValue,
  setDisheError,
  dishesError,
}: DishesTableProps) {
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="caption table">
        <caption>
          <Button variant="contained" onClick={() => addDishes()}>
            <AddIcon />
            <FormattedMessage id="dishTable.add" />
          </Button>
        </caption>
        <TableHead>
          <TableRow>
            <TableCell sx={{ width: '50%' }}>
              <FormattedMessage id="dishTable.foodName" />
            </TableCell>
            <TableCell align="right" sx={{ width: '20%' }}>
              <FormattedMessage id="dishTable.price" />
            </TableCell>
            <TableCell align="center" sx={{ width: '20%' }}>
              <FormattedMessage id="dishTable.image" />
            </TableCell>
            <TableCell align="center" sx={{ width: '10%' }}></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {dishes.map((dish: Dishes, index: number) => (
            <Dish
              setDisheError={setDisheError}
              deleteDishes={deleteDishes}
              index={index}
              key={index}
              dish={dish}
              dishesError={dishesError}
              onChangeValue={onChangeValue}
            />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
}
