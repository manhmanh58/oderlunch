import {
  createTheme,
  FormHelperText,
  TableCell,
  TableRow,
  ThemeProvider,
} from '@mui/material'
import React, { useEffect, useRef, useState } from 'react'
import { StyleDeleteBtn, StyleAddImage } from './style'
import DeleteIcon from '@mui/icons-material/Delete'
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate'
import { Dishes } from '@/src/utils/type/dishes'
import InputField from '@/src/components/InputField'
import { FormattedMessage, useIntl } from 'react-intl'
import { getTranslate } from '@/src/utils/share/translate'

declare module '@mui/material/styles' {
  interface BreakpointOverrides {
    xs: false
    sm: false
    md: false
    lg: false
    xl: false
    mobile: true
    laptop: true
    desktop: true
  }
}

const theme = createTheme({
  breakpoints: {
    values: {
      mobile: 0,
      laptop: 1024,
      desktop: 1280,
    },
  },
})

type DishesType = Dishes & {
  erorrs?: {
    name?: string
    price?: string
    image?: string
  }
}
type DishProps = {
  deleteDishes: Function
  onChangeValue: Function
  index: number
  dish: DishesType
  setDisheError: Function
  dishesError: String
}

export default function Dish({
  dish,
  deleteDishes,
  onChangeValue,
  index,
  setDisheError,
  dishesError,
}: DishProps) {
  const { formatMessage } = useIntl()
  const [errorMsgImg, setErrorMsgImg] = useState('')

  const check = useRef(false)

  const handleOnchange = (e: any) => {
    const tempFile = e.target.files[0]
    const MAX_FILE_SIZE = 10 // 10MB
    const imageMimeType = /image\/(png|jpg|jpeg)/i

    if (tempFile) {
      const fileSizeKiloBytes = tempFile.size / 1024 / 1024 //MB

      if (fileSizeKiloBytes > MAX_FILE_SIZE) {
        setErrorMsgImg(formatMessage({ id: 'createPost.errorImgSize' }))
        return
      }
      if (!tempFile?.type.match(imageMimeType)) {
        setErrorMsgImg(formatMessage({ id: 'edit.type' }))
        return
      }
      onChangeValue(tempFile, index, 'image')

      setErrorMsgImg('')
    }
    if (tempFile) {
    }
  }

  const handleOnDelete = (index: number) => {
    setDisheError('')
    deleteDishes(index)
  }
  const handleOnBlur = (e: any, index: number) => {
    if (e.target.value === '') {
      return
    }
    if (!check.current) {
      e.target.value = +e.target.value * 1000

      check.current = true
      e.target.nextSibling.data = ''
    }
    onChangeValue(e, index)
  }

  const handleOnChangePrice = (e: any, index: number) => {
    e.target.value = e.target.value.replace(/\D/g, '')
    onChangeValue(e, index)
  }
  return (
    <ThemeProvider theme={theme}>
      <TableRow>
        <TableCell sx={{ minWidth: 450, maxWidth: 450 }}>
          <InputField
            name="name"
            value={dish.name}
            onChange={(e: any) => onChangeValue(e, index)}
          />
          {dish?.erorrs?.name && (
            <FormHelperText sx={{ color: 'red', marginBottom: '-24px' }}>
              {getTranslate(dish?.erorrs?.name as string)}
            </FormHelperText>
          )}
          {!dish?.name && dishesError && !dish?.erorrs?.name && (
            <FormHelperText sx={{ color: 'red', marginBottom: '-24px' }}>
              <FormattedMessage id="createPost.food" />
            </FormHelperText>
          )}
        </TableCell>
        <TableCell sx={{ minWidth: 200, maxWidth: 200 }}>
          <InputField
            inputProps={{
              min: 0,
              style: { textAlign: 'right', paddingtop: '40px' },
            }}
            name="price"
            value={dish.price}
            onChange={(e: any) => handleOnChangePrice(e, index)}
            endAdornment="000"
            onBlur={(e: any) => handleOnBlur(e, index)}
          />
          {dish?.erorrs?.price && (
            <FormHelperText sx={{ color: 'red', marginBottom: '-24px' }}>
              {getTranslate(dish?.erorrs?.price as string)}
            </FormHelperText>
          )}
          {!dish?.price && dishesError && !dish?.erorrs?.price && (
            <FormHelperText sx={{ color: 'red', marginBottom: '-24px' }}>
              <FormattedMessage id="createPost.priceRequire" />
            </FormHelperText>
          )}
        </TableCell>
        <TableCell align="center">
          <StyleAddImage htmlFor={'upload' + index + dish.name}>
            {dish?.image?.url ? (
              <img src={URL.createObjectURL(dish.image.url)} alt="" />
            ) : (
              <AddPhotoAlternateIcon />
            )}
            <input
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                handleOnchange(e)
              }
              type="file"
              id={'upload' + index + dish.name}
            />
          </StyleAddImage>
          <FormHelperText
            sx={{
              color: 'red',
              width: '100%',
              height: '40px',
              marginLeft: { mobile: '0', laptop: '9%', desktop: '15%' },
            }}
          >
            {errorMsgImg}
          </FormHelperText>
        </TableCell>
        <TableCell align="center">
          <StyleDeleteBtn onClick={() => handleOnDelete(index)}>
            <DeleteIcon />
          </StyleDeleteBtn>
        </TableCell>
      </TableRow>
    </ThemeProvider>
  )
}
