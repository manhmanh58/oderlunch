import styled from "styled-components";

const StyleDefaultImage = styled.label`
  cursor: pointer;
  min-height: ${(props:{height:number})=>props.height?props.height+'px':'300px'} ;
  width: 100%;
  background: ${props => props.theme.colors.gray};
  border-radius: 12px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 20px;
  svg {
    font-size: 50px;
  }
  input {
    display: none;
  }
  img {
    width: 100%;
    object-fit: cover;
    border-radius: 12px;
  }
`;

const StyleSubmit = styled.input`
  margin-bottom: 50px;
  width: 100%;
  outline: 0;
  border: 0;
  color: #fff;
  background: ${props => props.theme.colors.primary};
  box-shadow: 0px 3px 1px -2px rgb(0 0 0 / 20%), 0px 2px 2px 0px rgb(0 0 0 / 14%), 0px 1px 5px 0px rgb(0 0 0 / 12%);
  font-family: "Roboto","Helvetica","Arial",sans-serif;
  font-weight: 500;
  font-size: 0.875rem;
  line-height: 1.75;
  letter-spacing: 0.02857em;
  text-transform: uppercase;
  min-width: 64px;
  padding: 6px 16px;
  border-radius: 4px;
  cursor: pointer;
  &:hover{
    box-shadow: 0px 2px 4px -1px rgb(0 0 0 / 20%), 0px 4px 5px 0px rgb(0 0 0 / 14%), 0px 1px 10px 0px rgb(0 0 0 / 12%)
  }
`

export { StyleDefaultImage, StyleSubmit };
