import styled from "styled-components";

const StyleWrapper = styled.div`
  position: relative;
  ${(props: { ref: any }) => null};
`;

const StyleDefaultImage = styled.label`
  cursor: pointer;
  min-height: 300px;
  width: 100%;
  background: ${props => props.theme.color.gray};
  border-radius: 12px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 20px;
  svg {
    font-size: 50px;
  }
  input {
    display: none;
  }
  img {
    width: 100%;
    object-fit: cover;
    border-radius: 12px;
  }
`;
const StyleImage = styled.div`
  width: 100%;
  position: relative;
  .MuiSvgIcon-root {
    font-size: 30px;
  }
`;
const StyleHover = styled.div`
  position: absolute;
  background: rgba(255, 255, 255, 0.5);
  top: 30px;
  right: 20px;
  bottom: 30px;
  border-radius: 10px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export { StyleDefaultImage, StyleImage, StyleHover, StyleWrapper };
