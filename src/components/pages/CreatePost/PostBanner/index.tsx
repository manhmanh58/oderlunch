import React, { useEffect, useState } from 'react'
import { StyleDefaultImage } from '../style'
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate'
import { StyleImage, StyleWrapper } from './style'
import useHover from 'hooks/useHover'
import { FormattedMessage, useIntl } from 'react-intl'

type PostBannerProps = {
  setBannerFile: Function
  bannerCuurent?: string
  setIsSuccess?: Function
  setErrorMsg?: Function
  height?: number
  style?: object
}

export default function PostBanner({
  setBannerFile,
  bannerCuurent,
  setIsSuccess,
  setErrorMsg,
  height,
  style,
}: PostBannerProps) {
  const [banner, setBanner] = useState('')
  const [hoverRef, isHovered] = useHover()
  const { formatMessage } = useIntl()

  const onInputClick = (
    event: React.MouseEvent<HTMLInputElement, MouseEvent>
  ) => {
    const element = event.target as HTMLInputElement
    element.value = ''
  }

  const id = (+new Date()).toString()
  const handleOnChange = (e: any) => {
    const file = e.target.files[0]
    const MAX_FILE_SIZE = 10 // 10MB
    const imageMimeType = /image\/(png|jpg|jpeg)/i
    if (!file) {
      // setErrorMsg && setIsSuccess && setIsSuccess(false)
      return
    }
    if (file) {
      if (setErrorMsg && setIsSuccess) {
        const fileSizeKiloBytes = file.size / 1024 / 1024 //MB

        if (fileSizeKiloBytes > MAX_FILE_SIZE) {
          setErrorMsg(formatMessage({ id: 'createPost.errorImgSize' }))
          setIsSuccess(false)
          return
        }
        if (!file?.type.match(imageMimeType)) {
          setErrorMsg(formatMessage({ id: 'edit.type' }))
          setIsSuccess(false)
          return
        }

        setErrorMsg('')
        setIsSuccess(true)
      }
      setBannerFile(file)
      setBanner(URL.createObjectURL(file))
    }
  }

  useEffect(() => {
    if (bannerCuurent) {
      setBanner(bannerCuurent)
      setBannerFile(bannerCuurent)
    }
  }, [])

  return (
    <StyleWrapper ref={hoverRef}>
      <StyleDefaultImage
        htmlFor={id || 'upload'}
        height={height ? height : 400}
        style={style || {}}
      >
        {banner || bannerCuurent ? (
          <StyleImage>
            <img src={banner} alt="" height={height ? height : 400} />
          </StyleImage>
        ) : (
          <AddPhotoAlternateIcon />
        )}
        <input
          onChange={(e: any) => handleOnChange(e)}
          onClick={onInputClick}
          type="file"
          id={id || 'upload'}
        />
      </StyleDefaultImage>
    </StyleWrapper>
  )
}
