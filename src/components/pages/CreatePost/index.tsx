import {
  Alert,
  Box,
  Collapse,
  InputLabel,
  Snackbar,
  Typography,
} from '@mui/material'
import { Container, Stack } from '@mui/system'
import React, { useState } from 'react'
import DishesTable from './DishesTable'
import PostBanner from './PostBanner'
import InputField from 'components/InputField'
import useCreateDishes from '@/src/hooks/useCreateDishs'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import { postSchema } from '@/src/constants/postSchema'
import { FormHelperText } from '@mui/material'
import { StyleSubmit } from './style'
import postsApi from '@/src/services/postsApi'
import { getURLImage } from 'utils/share/getURLImage'
import Loading from 'components/Loading'
import { useRouter } from 'next/router'
import { FormattedMessage, useIntl } from 'react-intl'
import { getTranslate } from '@/src/utils/share/translate'
import SelectField from '../../SelectField'
export default function CreatePort() {
  const [bannerFile, setBannerFile] = useState<Blob>()
  const { dishes, addDishes, deleteDishes, onChangeValue } = useCreateDishes()
  const [dishesError, setDisheError] = useState('')
  const [loading, setLoading] = useState(false)
  const [createFail, setCreateFail] = useState('')
  const [errorMsg, setErrorMsg] = useState<string>()
  const [isSuccess, setIsSuccess] = useState(false)
  const [open, setOpen] = useState<boolean>(false)

  const { formatMessage } = useIntl()
  const router = useRouter()
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(postSchema),
    mode: 'onChange',
  })

  const getError = () => {
    for (let i = 0; i < dishes.length; i++) {
      if (!dishes[i].name) {
        return {
          name: formatMessage({ id: 'createPost.title' }),
          row: i + 1,
        }
      }
      if (+dishes[i].price < 1) {
        return {
          name: formatMessage({ id: 'createPost.price' }),
          row: i + 1,
        }
      }
      if (isNaN(+dishes[i].price)) {
        return {
          name: formatMessage({ id: 'createPost.price' }),
          row: i + 1,
        }
      }
    }
  }

  const handleClick = () => {
    dishesError && setDisheError('')
    let errorObject = getError()
    if (!bannerFile) {
      setErrorMsg(formatMessage({ id: 'createPost.errorImgEmpty' }))
      setIsSuccess(false)
    }
    if (dishes.length === 0) {
      setDisheError(formatMessage({ id: 'createPost.dishError' }))
    }
    if (errorObject) {
      setDisheError(
        formatMessage(
          { id: 'createPost.dishValidation' },
          { name: `${errorObject.name}` }
        ) +
          formatMessage(
            { id: 'createPost.dishRow' },
            { number: `${errorObject.row}` }
          ) +
          ` ${
            errorObject.name === formatMessage({ id: 'createPost.price' })
              ? formatMessage({ id: 'createPost.dishPrice' })
              : ''
          }`
      )
      setIsSuccess(false)
    }
    if (bannerFile && dishes.length > 0 && !errorObject) {
      setIsSuccess(true)
    }
  }

  const onSubmit = async (data: any) => {
    if (!isSuccess) return
    setLoading(true)
    setOpen(true)
    try {
      const coverNumber = await Promise.all(
        dishes.map(async (x) => {
          const dishUrl = await getURLImage(x.image?.url)
          return {
            name: x.name,
            price: +x.price,
            image: {
              url: dishUrl,
            },
          }
        })
      )

      const URLbanner = bannerFile ? await getURLImage(bannerFile) : null
      let tzoffset = new Date().getTimezoneOffset() * 60000
      const date = new Date()
      let endTimeDate: any = new Date(date.getTime() + data.endTime * 60000)
      let localISOTime = new Date(endTimeDate - tzoffset).toISOString()
      const datas = await postsApi.create({
        title: data.title,
        endTime: localISOTime,
        thumbnail: {
          url: URLbanner,
        },
        dishes: coverNumber,
      })

      if (datas) {
        router.push('/')
      }
      setLoading(false)
    } catch (error: any) {
      setLoading(false)
      setCreateFail(error.message)
    }
  }

  const options = [
    {
      value: 10,
      key: 10,
    },
    {
      value: 20,
      key: 20,
    },
    {
      value: 30,
      key: 30,
    },
    {
      value: 40,
      key: 40,
    },
    {
      value: 50,
      key: 50,
    },
    {
      value: 60,
      key: 60,
    },
  ]
  return (
    <>
      {loading && <Loading />}
      <Container>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Typography mt={10} variant="h4" textAlign="center">
            <FormattedMessage id="createPost.pageName" />
          </Typography>
          <Box sx={{ position: 'relative' }}>
            <PostBanner
              setBannerFile={setBannerFile}
              setIsSuccess={setIsSuccess}
              setErrorMsg={setErrorMsg}
            />
            <Collapse in={!!errorMsg}>
              <Alert
                severity={'error'}
                sx={{ width: '32%', position: 'absolute', bottom: 0, right: 0 }}
                onClose={() => setErrorMsg('')}
              >
                {errorMsg}
              </Alert>
            </Collapse>
          </Box>

          <Stack spacing={5} mt={5} direction="column">
            <Stack>
              <InputLabel htmlFor="title">
                {' '}
                <FormattedMessage id="createPost.title" />
              </InputLabel>
              <FormattedMessage id="createPost.titleName">
                {(label) => (
                  <InputField
                    fullWidth
                    id="title"
                    type="text"
                    {...register('title')}
                  />
                )}
              </FormattedMessage>
              {errors.title && (
                <FormHelperText sx={{ color: 'red' }}>
                  {getTranslate(errors.title.message as string)}
                </FormHelperText>
              )}
            </Stack>
            <Stack>
              <InputLabel>
                <FormattedMessage id="createPost.time" />
              </InputLabel>
              <FormattedMessage id="createPost.endTime">
                {(label) => (
                  <SelectField
                    id="endTime"
                    fullWidth
                    type="time"
                    placeholder={label}
                    items={options}
                    {...register('endTime')}
                  />
                )}
              </FormattedMessage>

              {errors.endTime && (
                <FormHelperText sx={{ color: 'red' }}>
                  {getTranslate(errors.endTime.message as string)}
                </FormHelperText>
              )}
            </Stack>
          </Stack>
          <Stack mt={5} mb={5}>
            <InputLabel sx={{ mb: 1 }}>
              {' '}
              <FormattedMessage id="createPost.addDish" />
            </InputLabel>
            <DishesTable
              setDisheError={setDisheError}
              addDishes={addDishes}
              deleteDishes={deleteDishes}
              onChangeValue={onChangeValue}
              dishesError={dishesError}
              dishes={dishes}
            />
          </Stack>
          <FormattedMessage id="createPost.create">
            {(label: any) => (
              <StyleSubmit type="submit" value={label} onClick={handleClick} />
            )}
          </FormattedMessage>
        </form>
        <Snackbar
          open={open}
          autoHideDuration={100000}
          onClose={() => setOpen(false)}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          sx={{ marginTop: 10 }}
        >
          <Alert
            onClose={() => setOpen(false)}
            severity={createFail ? 'error' : 'success'}
            sx={{ width: '100%' }}
          >
            {createFail ? (
              <FormattedMessage id="createPost.error" />
            ) : (
              <FormattedMessage id="createPost.success" />
            )}
          </Alert>
        </Snackbar>
      </Container>
    </>
  )
}
