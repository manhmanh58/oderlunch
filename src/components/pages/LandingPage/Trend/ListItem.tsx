import React, { useEffect, useRef, useState } from 'react'
import { StyleListItem, StyleItemInfo } from './style'
import PersonOutlineIcon from '@mui/icons-material/PersonOutline'
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth'
import { Stack } from '@mui/system'
import { Box, Button, Typography } from '@mui/material'
import { DEFAULT_IMAGE } from '@/src/constants/common'
import { Post } from 'utils/type/post'
import HighlightOffIcon from '@mui/icons-material/HighlightOff'
import BorderColorIcon from '@mui/icons-material/BorderColor'
import Edit from './Edit'
import Popup from '@/src/components/Popup'
import { StyleConfirm } from '../FoodList/style'
import jwt from 'jwt-decode'
import MoreHorizIcon from '@mui/icons-material/MoreHoriz'
import { useRouter } from 'next/router'
import { FormattedMessage, useIntl } from 'react-intl'
import useOnClickOutside from '@/src/hooks/useOnClickOutside'
import TurnedInNotIcon from '@mui/icons-material/TurnedInNot'
import TurnedInIcon from '@mui/icons-material/TurnedIn'
import postsApi from '@/src/services/postsApi'
import { Snackbar } from '@material-ui/core'
import Alert from '@mui/material/Alert'

type ListItemProps = {
  setTab: Function
  setEndTime: Function
  trend: Post
  index: number
  tab?: string
  setBanner: Function
  setUserId: Function
  handleDeleteParent: Function
  handleUdaptePostParent: Function
}

export default function ListItem({
  trend,
  setTab,
  setEndTime,
  tab,
  setBanner,
  setUserId,
  handleDeleteParent,
  handleUdaptePostParent,
}: ListItemProps) {
  const [open, setOpen] = useState(false)
  const [openSave, setOpenSave] = useState(false)
  const [modalDelete, setModalDelete] = useState(false)
  const [openMore, setOpenMore] = useState(false)
  const [saved, setSaved] = useState(trend?.saved)
  const [error, setError] = useState('')
  const { formatMessage } = useIntl()
  const comfirmBox = formatMessage({ id: 'listItem.title' })
  const comfirm = formatMessage({ id: 'listItem.comfirm' })
  const cancel = formatMessage({ id: 'listItem.cancel' })
  const deletEdit = formatMessage({ id: 'editPost.delete' })
  const refMore = useRef(null)
  const editEdit = formatMessage({ id: 'editPost.edit' })
  const saveText = formatMessage({ id: 'trend.save' })
  const unSaveText = formatMessage({ id: 'trend.delete' })
  const saveError = formatMessage({ id: 'trend.saveError' })
  const router = useRouter()

  useOnClickOutside(refMore, () => {
    setOpenMore(false)
  })
  const handleOnSave = async () => {
    if (openSave) {
      setOpenSave(false)
    }
    try {
      await postsApi.updatePost(trend.id, { saved: !saved })

      setSaved(!saved)
    } catch (error) {
      setError(saveError)
    }
    setOpenSave(true)
  }

  const isPoster =
    trend.user.id === (jwt(localStorage.getItem('token')!) as { id: number }).id
  const handleOpen = () => {
    setOpen(true)
  }
  const onChange = () => {
    setBanner(trend.image.url)
    setEndTime(trend.endTime)
    setTab(trend.id)
    setUserId(trend.user.id)
  }

  const handleProfile = async () => {
    router.push(`/detail/${trend.user.id}`)
  }
  const handleOnChange = () => {
    onChange()
  }
  useEffect(() => {
    if (trend.id === tab) {
      onChange()
    }
  }, [])

  const handleOnclick = () => {
    setModalDelete(true)
  }

  const handleDelete = () => {
    setModalDelete(false)
    setOpenMore(false)
    handleDeleteParent(trend.id)
  }
  const handleUpdatePost = (props: any) => {
    handleUdaptePostParent(props)
    setOpenMore(false)
  }

  const handleClickMore = () => {
    setOpenMore(!openMore)
  }
  useEffect(() => {
    setOpenMore(false)
  }, [tab])

  return (
    <>
      {
        <Snackbar
          open={openSave}
          autoHideDuration={3000}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          onClose={() => setOpenSave(false)}
          style={{ marginTop: 10 }}
        >
          <Alert
            onClose={() => setOpenSave(false)}
            severity={error ? 'error' : 'success'}
            sx={{ width: '100%' }}
          >
            <FormattedMessage
              id={error ? error : !saved ? 'trend.unsave' : 'trend.saveSuccess'}
            />
          </Alert>
        </Snackbar>
      }

      {modalDelete && (
        <Popup setModalOpen={setModalDelete}>
          <StyleConfirm>{comfirmBox}</StyleConfirm>
          <Stack direction="row" mt={3} mb={2}>
            <Button
              variant="contained"
              sx={{ marginLeft: 'auto' }}
              onClick={handleDelete}
            >
              {comfirm}
            </Button>
            <Button
              variant="contained"
              sx={{ marginLeft: '20px', background: 'red !important' }}
              onClick={() => setModalDelete(false)}
            >
              {cancel}
            </Button>
          </Stack>
        </Popup>
      )}
      {open && (
        <Edit
          open={open}
          setOpen={setOpen}
          title={trend.title}
          id={trend.id}
          endTime={trend.endTime}
          handleUdaptePostParent={handleUpdatePost}
          urlCurrent={trend.image.url}
        />
      )}

      <StyleListItem active={trend.id == tab}>
        {isPoster && (
          <>
            <Box ref={refMore}>
              <MoreHorizIcon
                sx={{ position: 'absolute', top: '2%', right: '2%' }}
                onClick={handleClickMore}
              />

              {openMore && (
                <Box
                  sx={{
                    position: 'absolute',
                    padding: '2%',
                    right: '-34%',
                    width: '100px',
                    top: '0',
                    background: 'white',
                    zIndex: '200',
                    rotate: '20px',
                    boxShadow: 2,
                    borderRadius: 2,
                  }}
                >
                  <Box
                    onClick={handleOnclick}
                    sx={{
                      display: 'flex',
                      color: 'black',
                      fontSize: '0.875rem',
                      fontWeight: '700',
                      alignItems: 'center',
                    }}
                  >
                    <HighlightOffIcon
                      sx={{
                        color: 'red',
                        cursor: 'pointer',
                        marginRight: '10px',
                      }}
                    />
                    {deletEdit}
                  </Box>
                  <Box
                    sx={{
                      color: 'black',
                      display: 'flex',
                      width: '40px',
                      height: '30px',
                      fontSize: '0.875rem',
                      fontWeight: '700',
                      alignItems: 'center',
                    }}
                    onClick={handleOpen}
                  >
                    <BorderColorIcon
                      sx={{ marginRight: '10px', cursor: 'pointer' }}
                    />
                    {editEdit}
                  </Box>
                  <Box
                    sx={{
                      color: 'black',
                      display: 'flex',
                      width: '100%',
                      height: '30px',
                      fontSize: '0.875rem',
                      fontWeight: '700',
                      alignItems: 'center',
                      cursor: 'pointer',
                    }}
                    onClick={() => handleOnSave()}
                  >
                    {!saved ? (
                      <>
                        <TurnedInNotIcon sx={{ marginRight: '10px' }} />
                        {saveText}
                      </>
                    ) : (
                      <>
                        <TurnedInIcon sx={{ marginRight: '10px' }} />
                        {unSaveText}
                      </>
                    )}
                  </Box>
                </Box>
              )}
            </Box>
          </>
        )}
        {trend.image?.url ? (
          <img src={trend.image.url} onClick={() => handleOnChange()} alt="" />
        ) : (
          <img onClick={() => handleOnChange()} src={DEFAULT_IMAGE} />
        )}
        <StyleItemInfo>
          <Typography
            style={{ cursor: 'pointer' }}
            fontSize={14}
            fontWeight={600}
            variant="h3"
            onClick={() => handleOnChange()}
          >
            {trend.title}
          </Typography>
          <Stack>
            <Stack
              direction="row"
              alignItems="center"
              spacing={1}
              onClick={() => handleProfile()}
              sx={{
                '&:hover': {
                  cursor: 'pointer',
                },
              }}
            >
              <PersonOutlineIcon style={{ fontSize: 16 }} />
              <Typography variant="h6" fontSize={12}>
                {trend.user.fullName}
              </Typography>
            </Stack>
            <Stack mt={0} direction="row" alignItems="center" spacing={1}>
              <CalendarMonthIcon style={{ fontSize: 16 }} />
              <Typography variant="h6" fontSize={12}>
                {trend.endTime.split('T')[0] +
                  ' ' +
                  trend.endTime.split('T')[1].split('.')[0]}
              </Typography>
            </Stack>
          </Stack>
        </StyleItemInfo>
      </StyleListItem>
    </>
  )
}
