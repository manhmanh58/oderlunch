import { Pagination, Typography } from '@mui/material'
import React from 'react'
import ListItem from './ListItem'
import { StylePagination, StyleTrend } from './style'
import { Post } from 'utils/type/post'
import { FormattedMessage } from 'react-intl'

type TrendProps = {
  setTab: Function
  allPosts?: Array<Post>
  setEndTime: Function
  tab?: string
  setBanner: Function
  page: number
  setPage: Function
  count?: number
  setUserId: Function
  handleDeleteParent: Function
  handleUdaptePostParent: Function
}

export default function Trend({
  setTab,
  allPosts,
  setEndTime,
  tab,
  setBanner,
  page,
  setPage,
  count,
  setUserId,
  handleDeleteParent,
  handleUdaptePostParent,
}: TrendProps) {
  const handleDelete = (props: number) => {
    handleDeleteParent(props)
  }
  const handleUdaptePost = (props: any) => {
    handleUdaptePostParent(props)
  }

  return (
    <StyleTrend>
      <Typography
        fontSize={18}
        fontWeight={600}
        variant="h3"
        position="relative"
        bottom="24px"
      >
        <FormattedMessage id="trend.trend" />
      </Typography>
      {allPosts &&
        allPosts.map((trend: Post, index: number) => (
          <ListItem
            setUserId={setUserId}
            index={index}
            tab={tab}
            setTab={setTab}
            setEndTime={setEndTime}
            key={index}
            trend={trend}
            setBanner={setBanner}
            handleDeleteParent={handleDelete}
            handleUdaptePostParent={handleUdaptePost}
          />
        ))}
      <StylePagination>
        <Pagination
          page={page}
          onChange={(e, value) => setPage(value)}
          sx={{ width: '100%' }}
          count={count}
          defaultPage={1}
          siblingCount={0}
        />
      </StylePagination>
    </StyleTrend>
  )
}
