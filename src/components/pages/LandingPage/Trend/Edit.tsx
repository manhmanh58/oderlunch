import InputField from '@/src/components/InputField'
import {
  Box,
  Button,
  CircularProgress,
  FormHelperText,
  InputLabel,
  Modal,
  Stack,
  TextField,
  Typography,
} from '@mui/material'
import React, { useEffect, useState } from 'react'
import PostBanner from '../../CreatePost/PostBanner'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import { postSchema } from '@/src/constants/postSchema'
import { getURLImage } from '@/src/utils/share/getURLImage'
import { FormattedMessage } from 'react-intl'
import { getTranslate } from '@/src/utils/share/translate'
import SelectField from '@/src/components/SelectField'
import { Dishes } from '@/src/utils/type/dishes'
interface Iprops {
  open: boolean
  setOpen: Function
  title: string
  id: string
  endTime: string
  handleUdaptePostParent: Function
  urlCurrent: string
  clone?: boolean
  handleOnClone?: Function
  index?: number
}
function Edit(props: Iprops) {
  const [title, setTitle] = useState<string>(props.title)
  const [timeEnd, setTimeEnd] = useState<string>(props?.endTime.slice(11, 16))
  const [bannerFile, setBannerFile] = useState<boolean>()
  const [loading, setLoading] = useState<boolean>(false)
  const [errorMsg, setErrorMsg] = useState<string>()
  const [isSuccess, setIsSuccess] = useState(false)

  const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  }
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(postSchema),
    mode: 'onChange',
  })

  const handleClose = () => props.setOpen(false)

  const handleClickSubmit = async (data: any) => {
    setLoading(true)
    let URLbanner = null
    if (typeof bannerFile === 'string') {
      URLbanner = bannerFile
    } else if (typeof bannerFile === 'object') {
      URLbanner = await getURLImage(bannerFile)
    }
    let tzoffset = new Date().getTimezoneOffset() * 60000
    const date = new Date()
    let endTimeDate: any = new Date(date.getTime() + data.endTime * 60000)
    let localISOTime = new Date(endTimeDate - tzoffset).toISOString()

    let infomationUpdate = {
      id: props.id,
      title: data.title,
      endTime: localISOTime,
      url: URLbanner,
    }

    props.handleUdaptePostParent(infomationUpdate)
    setLoading(false)
    props.setOpen(false)
  }
  const options = [
    {
      value: 10,
      key: 10,
    },
    {
      value: 20,
      key: 20,
    },
    {
      value: 30,
      key: 30,
    },
    {
      value: 40,
      key: 40,
    },
    {
      value: 50,
      key: 50,
    },
    {
      value: 60,
      key: 60,
    },
  ]

  return (
    <Modal
      open={props.open}
      onClose={handleClose}
      aria-labelledby="parent-modal-title"
      aria-describedby="parent-modal-description"
    >
      <Box sx={style}>
        <Typography variant="h5" component="h2" sx={{ color: 'black' }}>
          <FormattedMessage id="edit.editPost" />
        </Typography>
        <>
          <form onSubmit={handleSubmit(handleClickSubmit)}>
            <PostBanner
              setBannerFile={setBannerFile}
              bannerCuurent={props?.urlCurrent}
              setIsSuccess={setIsSuccess}
              setErrorMsg={setErrorMsg}
              height={250}
            />
            <FormHelperText sx={{ color: 'red' }}>{errorMsg}</FormHelperText>

            <Stack spacing={2} mt={2} direction="column">
              <Stack>
                <InputLabel htmlFor="title">
                  {' '}
                  <FormattedMessage id="edit.postTitle" />
                </InputLabel>
                <FormattedMessage id="edit.enterTitle">
                  {(label: any) => (
                    <InputField
                      fullWidth
                      defaultValue={title}
                      id="title"
                      type="text"
                      placeholder={label}
                      {...register('title')}
                    />
                  )}
                </FormattedMessage>

                {errors.title && (
                  <FormHelperText sx={{ color: 'red' }}>
                    {getTranslate(errors.title.message as string)}
                  </FormHelperText>
                )}
              </Stack>
              <Stack>
                <InputLabel>
                  {' '}
                  <FormattedMessage id="edit.editTime" />
                </InputLabel>
                <FormattedMessage id="edit.editTime">
                  {(label: any) => (
                    <SelectField
                      id="endTime"
                      defaultValue={10}
                      fullWidth
                      type="time"
                      placeholder={label}
                      items={options}
                      {...register('endTime')}
                    />
                  )}
                </FormattedMessage>
                {errors.endTime && (
                  <FormHelperText sx={{ color: 'red' }}>
                    {getTranslate(errors.endTime.message as string)}
                  </FormHelperText>
                )}
              </Stack>
            </Stack>
            <Box sx={{ display: 'flex', justifyContent: 'end', mt: '20px' }}>
              {loading ? (
                <CircularProgress size={30} color="success" />
              ) : (
                <>
                  <Button
                    variant="outlined"
                    color="error"
                    sx={{ mr: '10px' }}
                    onClick={handleClose}
                  >
                    <FormattedMessage id="edit.cancel" />
                  </Button>
                  {!props.clone ? (
                    <Button variant="contained" type="submit">
                      <FormattedMessage id="edit.savePost" />
                    </Button>
                  ) : (
                    <Button
                      variant="contained"
                      onClick={() =>
                        props.handleOnClone && props.handleOnClone(props.index)
                      }
                    >
                      <FormattedMessage id="saved.table.edit" />
                    </Button>
                  )}
                </>
              )}
            </Box>
          </form>
        </>
      </Box>
    </Modal>
  )
}

export default Edit
