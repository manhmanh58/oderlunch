import styled from 'styled-components'

const StyleTrend = styled.div`
  margin-top: 20px;
  padding: 30px 0px;
  min-width: 300px;
  max-width: 300px;
`

const StyleListItem = styled.div`
  box-shadow: 0 0 3px rgb(97 96 136 / 20%);
  margin-top: 10px;
  position: relative;
  display: flex;
  padding: 15px;
  border-radius: 8px;
  ${(props: { active?: boolean; theme: any; reverse?: boolean }) =>
    `${
      props.active && `;background: ${props.theme.colors.primary}; color: #fff;`
    }
    ${
      props.reverse &&
      `;justify-content: space-between; flex-direction: row-reverse;`
    }
    `};
  img {
    width: 120px;
    min-width: 120px;
    max-width: 120px;
    height: 82px;
    border-radius: 8px;
    margin-right: 10px;
    cursor: pointer;
  }
`

const StyleItemInfo = styled.div`
  width: 100%;
  padding: 3px 0px;
  display: flex;
  justify-content: space-between;
  flex-direction: column;
`

const StylePagination = styled.div`
  margin-top: 20px;
`

export { StyleTrend, StyleListItem, StyleItemInfo, StylePagination }
