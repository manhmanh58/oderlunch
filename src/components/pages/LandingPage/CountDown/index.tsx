import { Typography } from '@mui/material'
import React, { useState, useEffect } from 'react'
import {
  StyleCountDown,
  StyleFlipCard,
  StyleFlipClock,
  StyleFlipUnitContainer,
  StyleItemFlipLower,
  StyleItemFlipUper,
  StyleLowerCard,
  StyleUpperCard,
  StyleContainers,
} from './style'
import { FormattedMessage } from 'react-intl'

type CountDownProps = {
  time: number
  setExpired: Function
  tab: string
}

export default function CountDown({ time, tab, setExpired }: CountDownProps) {
  const [timeRun, setTimeRun] = useState('')
  const [changeSecond, setChangeSecond] = useState(false)
  const [changeMinute, setChangeMinute] = useState(false)
  const [changeHour, setChangeHour] = useState(false)
  useEffect(() => {
    const milisecond = Math.floor(
      +new Date(+new Date(time) - +new Date(25200000) - +new Date())
    )
    if (milisecond < 0) {
      setTimeRun('')
      setExpired(true)
      return
    }
    setExpired(false)
    let minutes: number
    let hour = Math.floor(milisecond / 60 / 60 / 1000)
    if (milisecond % (60 * 1000) === 0) {
      minutes = 59
      hour--
    } else {
      minutes = Math.floor(milisecond / 60 / 1000) - hour * 60
    }
    let second = 59
    const handleCountDown = (secondCurrent: number) => {
      if (secondCurrent <= 0 && minutes <= 0 && hour <= 0) {
        setExpired(true)
        setTimeRun('')
        return
      }
      if (minutes <= 0) {
        if (hour > 0) {
          hour--
          minutes = 59
          setChangeHour((prev) => !prev)
        } else {
          minutes = 0
        }
      }
      if (secondCurrent <= 0) {
        second = 59
        minutes--
        setChangeMinute((prev) => !prev)
      }
      setTimeRun(
        (hour > 9 ? hour : '0' + hour) +
          ':' +
          (minutes > 9 ? minutes : '0' + minutes) +
          ':' +
          (secondCurrent > 9 ? secondCurrent : '0' + secondCurrent)
      )
    }
    const interval = setInterval(async () => {
      setChangeSecond((prev) => !prev)
      handleCountDown(second--)
    }, 1000)
    return () => clearInterval(interval)
  }, [time, tab])
  return (
    <>
      {timeRun ? (
        <>
          <Typography mt={5} variant="h5" align="center">
            <FormattedMessage id="countDown.title" />
          </Typography>
          <StyleContainers>
            <StyleFlipClock>
              <StyleFlipUnitContainer>
                <StyleUpperCard>
                  <StyleItemFlipUper>{timeRun.split(':')[0]}</StyleItemFlipUper>
                </StyleUpperCard>
                <StyleLowerCard>
                  <StyleItemFlipLower>
                    {timeRun.split(':')[0]}
                  </StyleItemFlipLower>
                </StyleLowerCard>
                <StyleFlipCard fold={changeHour}>
                  <span>{timeRun.split(':')[0]}</span>
                </StyleFlipCard>
                <StyleFlipCard fold={!changeHour}>
                  <span>{timeRun.split(':')[0]}</span>
                </StyleFlipCard>
              </StyleFlipUnitContainer>
              <StyleFlipUnitContainer>
                <StyleUpperCard>
                  <StyleItemFlipUper>{timeRun.split(':')[1]}</StyleItemFlipUper>
                </StyleUpperCard>
                <StyleLowerCard>
                  <StyleItemFlipLower>
                    {timeRun.split(':')[1]}
                  </StyleItemFlipLower>
                </StyleLowerCard>
                <StyleFlipCard fold={changeMinute}>
                  <span>{timeRun.split(':')[1]}</span>
                </StyleFlipCard>
                <StyleFlipCard fold={!changeMinute}>
                  <span>{timeRun.split(':')[1]}</span>
                </StyleFlipCard>
              </StyleFlipUnitContainer>
              <StyleFlipUnitContainer>
                <StyleUpperCard>
                  <StyleItemFlipUper>{timeRun.split(':')[2]}</StyleItemFlipUper>
                </StyleUpperCard>
                <StyleLowerCard>
                  <StyleItemFlipLower>
                    {timeRun.split(':')[2]}
                  </StyleItemFlipLower>
                </StyleLowerCard>
                <StyleFlipCard fold={changeSecond}>
                  <span>{timeRun.split(':')[2]}</span>
                </StyleFlipCard>
                <StyleFlipCard fold={!changeSecond}>
                  <span>{timeRun.split(':')[2]}</span>
                </StyleFlipCard>
              </StyleFlipUnitContainer>
            </StyleFlipClock>
          </StyleContainers>
        </>
      ) : (
        <StyleCountDown error>
          <FormattedMessage id="countDown.error" />
        </StyleCountDown>
      )}
    </>
  )
}
