import styled, { css, keyframes } from 'styled-components'

const StyleCountDown = styled.p`
  text-align: center;
  margin-top: 30px;
  font-size: 32px;
  width: 100%;
  ${(props: { error?: boolean }) => props.error && 'color: ${(props)=>props.theme.colors.primary}'}
`

const unfold = keyframes`
  from {
    transform: rotateX(0deg);
  }
  to {
    transform: rotateX(-180deg);
  }
`

const fold = keyframes`
  from {
    transform: rotateX(180deg);
  }
  to {
    transform: rotateX(0deg);
  }
`

const StyleFlipClock = styled.div`
  display: flex;
  justify-content: space-between;
  width: 3 * 120px + 80px;
  color: #fff;
  font-size: 30px;
`

const StyleFlipUnitContainer = styled.div`
  display: block;
  position: relative;
  width: 60px;
  height: 50px;
  perspective-origin: 50% 50%;
  perspective: 300px;
  background-color: ${(props)=>props.theme.colors.primary};
  border-radius: 3px;
  box-shadow: 0px 10px 10px -10px grey;
`

const StyleUpperCard = styled.div`
  display: flex;
  position: relative;
  justify-content: center;
  width: 100%;
  height: 50%;
  overflow: hidden;
  align-items: flex-end;
  border-top-left-radius: 3px;
  border-top-right-radius: 3px;
`

const StyleLowerCard = styled.div`
  display: flex;
  position: relative;
  justify-content: center;
  width: 100%;
  height: 50%;
  overflow: hidden;
`

const StyleItemFlipUper = styled.span`
  transform: translateY(50%);
`

const StyleItemFlipLower = styled.span`
  transform: translateY(-50%);
  position: absolute;
  top: 0%;
`

const StyleFlipCard = styled.div`
  display: flex;
  justify-content: center;
  position: absolute;
  left: 0;
  width: 100%;
  height: 50%;
  overflow: hidden;
  backface-visibility: hidden;
  ${(props: { fold: boolean; theme: any }) =>
    props.fold
      ? css`
          top: 0%;
          align-items: flex-end;
          transform-origin: 50% 100%;
          transform: rotateX(0deg);
          background-color: ${(props)=>props.theme.colors.primary};
          border-top-left-radius: 3px;
          border-top-right-radius: 3px;
          transform-style: preserve-3d;
          animation: ${unfold} 0.5s cubic-bezier(0.455, 0.03, 0.515, 0.955) 0s 1
            normal forwards;

          span {
            transform: translateY(50%);
          }
        `
      : css`
          top: 50%;
          align-items: flex-start;
          transform-origin: 50% 0%;
          transform: rotateX(180deg);
          background-color: ${(props)=>props.theme.colors.primary};
          border-bottom-left-radius: 3px;
          border-bottom-right-radius: 3px;
          transform-style: preserve-3d;
          animation: ${fold} 0.5s cubic-bezier(0.455, 0.03, 0.515, 0.955) 0s 1
            normal forwards;
          span {
            transform: translateY(-50%);
          }
        `}
`
const StyleContainers = styled.div`
  width: 16%;
  margin: 20px auto;
`
export {
  StyleCountDown,
  StyleFlipCard,
  StyleFlipClock,
  StyleFlipUnitContainer,
  StyleItemFlipLower,
  StyleItemFlipUper,
  StyleLowerCard,
  StyleUpperCard,
  StyleContainers,
}
