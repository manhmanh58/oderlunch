import styled from 'styled-components'

const StyleWrapper = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
`

const StyleBanner = styled.img`
  width: 100%;
  height: 450px;
  object-fit: cover;
  border-radius: 12px;
`
const StyleDefautlBanner = styled.div`
  width: 100%;
  height: 300px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 12px;
  background: ${(props) => props.theme.colors.gray};
  svg {
    width: 50px;
    height: 50px;
  }
`
const StyleFullHeight = styled.div`
  height: calc(100vh - 420px);
  width: 100vw;
`
export { StyleWrapper, StyleBanner, StyleDefautlBanner, StyleFullHeight }
