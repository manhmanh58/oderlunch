import { Typography } from '@mui/material'
import styled, { keyframes } from 'styled-components'

const StyleFoodList = styled.div`
  margin-top: 23px;
  position: relative;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  border: 1px solid ${(props) => props.theme.colors.primary};
  padding: 30px 10px;
  border-radius: 12px;
  box-shadow: 0 3px 3px rgb(97 96 136 / 12%);
  width: 100%;
`
const StyleFoodListItem = styled.div`
  ${(props: { ref: any }) => ''}
  margin: 10px;
  position: relative;
  padding: 10px;
  border-radius: 12px;
  box-shadow: 0 0 5px rgb(97 96 136 / 30%);
  border: 1px solid #d0c8b6;
  display: flex;
  flex-direction: column;
  min-width: 270px;
  max-width: 270px;
  justify-content: space-between;
  transition: all 0.3s ease-in-out;
  cursor: pointer;
  img {
    height: 150px;
    border-radius: 12px;
  }
  &:hover {
    box-shadow: 0 0 20px rgb(97 96 136 / 12%);
    transform: scale(1.05, 1.05);
  }
`
const StyleDes = styled(Typography)`
  margin-top: 10px;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  overflow: hidden;
`

const slide = keyframes`
  from {
    top: 10%;
    opacity: 0.3;
  }
  to {
    transform: top 30%;
    opacity: 1;
  }
`

const StyleOrders = styled.div`
  display: flex;
  align-items: center;
  width: 80%;
  padding: 15px 20px;
  border-radius: 8px;
  background: rgba(255, 255, 255, 0.95);
  position: absolute;
  top: 50px;
  left: 50%;
  transform: translateX(-50%);
  animation: ${slide} 0.1s linear forwards;
  button {
    margin-left: 10px;
    background: ${(props) => props.theme.colors.primary};
    &:hover {
      background: ${(props) => props.theme.colors.primary};
    }
  }
  input {
    border-bottom: 1px solid ${(props) => props.theme.colors.primary};
    opacity: 0.7;
    &:focus {
      border-bottom: 2px solid ${(props) => props.theme.colors.primary};
      opacity: 1;
    }
  }
`

const StyleEditDelete = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 80%;
  padding: 15px 20px;
  border-radius: 8px;
  z-index: 100;
  background: rgba(255, 255, 255, 0.95);
  position: absolute;
  top: 50px;
  left: 50%;
  transform: translateX(-50%);
  animation: ${slide} 0.1s linear forwards;
`

const StyleConfirm = styled.p`
  margin-top: 20px;
  text-align: center;
  font-weight: 600;
  font-size: 18px;
  b {
    color: ${(props) => props.theme.colors.primary};
  }
`

export {
  StyleFoodList,
  StyleFoodListItem,
  StyleDes,
  StyleOrders,
  StyleConfirm,
  StyleEditDelete,
}
