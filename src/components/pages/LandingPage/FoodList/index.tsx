import Loading from '@/src/components/Loading'
import postsApi from '@/src/services/postsApi'
import { Stack, Typography } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { StyleTabPanel, StyleTabPanelItem } from './Tab/style'
import FoodListItem from './FoodListItem'
import { StyleFoodList } from './style'
import ChoosenTab from './Tab/ChoosenTab'
import { OrderedType } from '@/src/utils/type/orders'
import SentimentSatisfiedAltIcon from '@mui/icons-material/SentimentSatisfiedAlt'
import { theme } from '@/src/theme'
import jwt from 'jwt-decode'

import Tab from './Tab/PaidTab'
import Snackbar from '@mui/material/Snackbar'
import Alert from '@mui/material/Alert'
import { FormattedMessage, useIntl } from 'react-intl'
import PosterPaidTab from './Tab/PosterPaidTab'
type FoodListProps = {
  tab: string
  expired: boolean
  setOrdereds: Function
  lsSetOrdereds: Function
  ordereds: Array<OrderedType>
  userId: number
}

export default function FoodList({
  tab,
  expired,
  setOrdereds,
  lsSetOrdereds,
  ordereds,
  userId,
}: FoodListProps) {
  const isPoster =
    userId === (jwt(localStorage.getItem('token')!) as { id: number }).id
  const [datas, setDatas] = useState([])
  const [loading, setLoading] = useState(false)
  const [page, setPage] = useState(1)
  const [menu, setMenu] = useState(1)
  const [open, setOpen] = useState(false)
  const { formatMessage } = useIntl()
  const [orderTab, setOrderTab] = useState<
    Array<OrderedType & { index: number }>
  >([])
  const [payDatas, setPayDatas] = useState([])
  const [succsess, setSuccess] = useState('')

  const handleChangeOpen = () => {
    succsess && setSuccess('')

    if (open) {
      setOpen(false)
      setTimeout(() => setOpen(true), 100)
    } else {
      setOpen(true)
    }
    setSuccess(formatMessage({ id: 'foodList.alert' }))
  }
  const fetchData = async () => {
    const { data } = await postsApi.getDetail(`${tab}`)
    setDatas(data.postDetails.dishes)
  }
  const fetchDataPayData = async () => {
    try {
      const { data } = isPoster
        ? await postsApi.getOrdersOfUserByPost(`${tab}`)
        : await postsApi.getOrdered(tab)
      if (data) {
        setPayDatas(data.orders)
      }
    } catch (error) {}
  }
  useEffect(() => {
    setLoading(true)
    setMenu(1)
    fetchData()
    fetchDataPayData()
    const timer = setTimeout(() => {
      setLoading(false)
      window.scrollTo(0, 0)
    }, 500)
    return () => clearTimeout(timer)
  }, [tab])

  useEffect(() => {
    if (ordereds) {
      const orderedsArray = ordereds.map((x, index) => ({
        ...x,
        index,
      }))
      setOrderTab(orderedsArray.filter((x) => x.postId === tab))
    }
  }, [tab, ordereds])
  useEffect(() => {
    fetchDataPayData()
  }, [orderTab])

  const handleDeleteParent = (props: number) => {
    succsess && setSuccess('')

    try {
      const deleteDish = async () => {
        await postsApi.deleteDishes(props)
        fetchData()
      }

      deleteDish()
      setSuccess(formatMessage({ id: 'foodList.deleteSuccess' }))

      if (open) {
        setOpen(false)
        setTimeout(() => setOpen(true), 100)
      } else {
        setOpen(true)
      }
    } catch (error: any) {}
  }

  const handleUdapteDishParent = (props: any) => {
    succsess && setSuccess('')

    try {
      let price = Number(props.price)
      const updateDish = async () => {
        await postsApi.updateDishes(props?.id, {
          name: props?.name,
          price: price,
          image: {
            url: props?.url,
          },
        })
        fetchData()
      }

      updateDish()
      setSuccess(formatMessage({ id: 'foodList.updateSuccess' }))

      if (open) {
        setOpen(false)
        setTimeout(() => setOpen(true), 100)
      } else {
        setOpen(true)
      }
    } catch (error: any) {}
  }

  return (
    <>
      <Snackbar
        open={open}
        autoHideDuration={5000}
        onClose={(event: React.SyntheticEvent | Event, reason?: string) =>
          reason === 'clickaway' ? null : setOpen(false)
        }
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        sx={{ marginTop: 10 }}
      >
        <Alert
          onClose={() => setOpen(false)}
          severity={'success'}
          sx={{ width: '100%' }}
        >
          {succsess}
        </Alert>
      </Snackbar>
      {loading && <Loading />}
      <Stack sx={{ marginLeft: 5, marginTop: 1, width: '100%' }}>
        <StyleTabPanel>
          <StyleTabPanelItem active={menu === 1} onClick={() => setMenu(1)}>
            {!isPoster
              ? formatMessage({ id: 'foodList.confirm' })
              : formatMessage({ id: 'foodList.list' })}{' '}
            ({datas.length})
          </StyleTabPanelItem>
          {!isPoster && (
            <StyleTabPanelItem active={menu === 2} onClick={() => setMenu(2)}>
              <FormattedMessage id="foodList.waiting" /> ({orderTab.length})
            </StyleTabPanelItem>
          )}
          {true && (
            <StyleTabPanelItem active={menu === 3} onClick={() => setMenu(3)}>
              <FormattedMessage id="foodList.status" /> ({payDatas?.length || 0}
              )
            </StyleTabPanelItem>
          )}
        </StyleTabPanel>
        {datas.length > 0 && menu === 1 && (
          <StyleFoodList>
            {datas.map((dish, index) => (
              <FoodListItem
                isPoster={isPoster}
                tab={`${tab}`}
                setOrdereds={setOrdereds}
                key={index + tab}
                dish={dish}
                expired={expired}
                handleChangeOpen={handleChangeOpen}
                handleDeleteParent={handleDeleteParent}
                handleUdapteDishParent={handleUdapteDishParent}
              />
            ))}
          </StyleFoodList>
        )}
        {menu === 2 &&
          (orderTab.length > 0 ? (
            <StyleFoodList>
              <Stack width="100%" alignItems="center">
                <ChoosenTab
                  expired={expired}
                  orderTab={orderTab}
                  page={page}
                  ordereds={ordereds}
                  setOrdereds={setOrdereds}
                  setPage={setPage}
                  tab={tab}
                  lsSetOrdereds={lsSetOrdereds}
                  setSuccess={setSuccess}
                  setOpen={setOpen}
                  succsess={succsess}
                />
              </Stack>
            </StyleFoodList>
          ) : (
            <StyleFoodList>
              <Stack
                sx={{ color: theme.colors.primary }}
                width="100%"
                direction="row"
                alignItems="center"
                justifyContent="center"
              >
                <Typography variant="h4" align="center">
                  <FormattedMessage id="foodList.empty" />
                </Typography>
                <SentimentSatisfiedAltIcon
                  fontSize="large"
                  sx={{ marginLeft: 1 }}
                />
              </Stack>
            </StyleFoodList>
          ))}
        {menu === 3 && (
          <StyleFoodList>
            {!isPoster ? (
              <PosterPaidTab
                postId={tab}
                fetchDataPayData={fetchDataPayData}
                payDatas={payDatas}
                setPayDatas={setPayDatas}
                isPoster={isPoster}
              />
            ) : (
              <Tab
                postId={tab}
                fetchDataPayData={fetchDataPayData}
                payDatas={payDatas}
                setPayDatas={setPayDatas}
                isPoster={isPoster}
              />
            )}
          </StyleFoodList>
        )}
      </Stack>
    </>
  )
}
