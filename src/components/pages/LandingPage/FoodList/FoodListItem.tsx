import { Box, Typography } from '@mui/material'
import { Stack } from '@mui/system'
import React, { useState } from 'react'
import {
  StyleFoodListItem,
  StyleOrders,
  StyleDes,
  StyleConfirm,
  StyleEditDelete,
} from './style'
import PaymentIcon from '@mui/icons-material/Payment'
import useHover from 'hooks/useHover'
import AddIcon from '@mui/icons-material/Add'
import Button from '@/src/components/Button'
import Input from '@mui/material/Input'
import Image from 'next/image'
import defaultImage from 'assets/images/default_food.jpg'
import { OrderedType } from 'utils/type/orders'
import { getCurrency } from '@/src/utils/share/getCurrency'
import Popup from '@/src/components/Popup'
import MoreHorizIcon from '@mui/icons-material/MoreHoriz'
import HighlightOffIcon from '@mui/icons-material/HighlightOff'
import BorderColorIcon from '@mui/icons-material/BorderColor'
import EditListItem from './Tab/EditListItem'
import { isPromise } from 'util/types'
import { useIntl } from 'react-intl'

type FoodListItemProps = {
  dish: {
    name: string
    price: number
    image?: {
      url: string
    }
    id: string
  }
  expired: boolean
  setOrdereds: Function
  tab?: string
  isPoster?: boolean
  handleChangeOpen?: Function
  handleDeleteParent?: Function
  handleUdapteDishParent?: Function
}

export default function FoodListItem({
  dish,
  expired,
  setOrdereds,
  tab,
  isPoster,
  handleDeleteParent,
  handleUdapteDishParent,
  handleChangeOpen = () => {},
}: FoodListItemProps) {
  const [hoverRef, isHovered] = useHover()
  const [quantity, setQuantity] = useState(1)
  const [modalDelete, setModalDelete] = useState(false)
  const [open, setOpen] = useState(false)
  const { formatMessage } = useIntl()
  const comfirmBox = formatMessage({ id: 'listItem.title' })
  const comfirm = formatMessage({ id: 'listItem.comfirm' })
  const cancel = formatMessage({ id: 'listItem.cancel' })
  const deleteEdit = formatMessage({ id: 'editDish.delete' })
  const editEdit = formatMessage({ id: 'editDish.edit' })
  const [succsess, setSuccess] = useState<string>()

  const handleOnAdd = () => {
    if (quantity > 0) {
      handleOnConfirm()
    } else {
      handleChangeOpen()
    }
  }

  const handleOnConfirm = () => {
    if (quantity > 0) {
      setOrdereds((prev: Array<OrderedType>) => {
        if (prev) {
          for (let i = 0; i < prev.length; i++) {
            if (prev[i].id === dish.id) {
              prev[i].quantity = +prev[i].quantity + +quantity
              return [...prev]
            }
          }
          return [
            ...prev,
            {
              id: dish.id,
              postId: tab,
              name: dish.name,
              price: dish.price,
              quantity,
              image: {
                url: dish?.image?.url || null,
              },
            },
          ]
        } else {
          return []
        }
      })
      handleChangeOpen()
    }
  }

  const handleDelete = () => {
    if (handleDeleteParent) {
      handleDeleteParent(dish.id)
      setModalDelete(false)
    }
  }

  const handleOnclick = () => {
    setModalDelete(true)
  }

  const handleOpen = () => {
    setOpen(true)
  }

  const handleUpdateDish = (props: any) => {
    if (handleUdapteDishParent) {
      handleUdapteDishParent(props)
    }
  }

  const handleOnChangeQuantity = (e: any) => {
    setQuantity(e.target.value)
  }

  const handlePaste = (e: any) => {
    e.preventDefault()

    setQuantity(1)
  }

  const handleInput = (e: any) => {
    e.target.value =
      !!e.target.value && Math.abs(e.target.value) > 0
        ? Math.abs(e.target.value)
        : null
  }
  return (
    <>
      {open && (
        <EditListItem
          open={open}
          setOpen={setOpen}
          name={dish.name}
          price={dish.price}
          url={dish.image?.url}
          id={dish?.id}
          handleUdaptePostParent={handleUpdateDish}
        />
      )}

      {modalDelete && (
        <Popup setModalOpen={setModalDelete}>
          <StyleConfirm>{comfirmBox}</StyleConfirm>
          <Stack direction="row" mt={3} mb={2}>
            <Button
              variant="contained"
              sx={{ marginLeft: 'auto' }}
              onClick={handleDelete}
            >
              {comfirm}
            </Button>
            <Button
              variant="contained"
              sx={{ marginLeft: '20px', background: 'red !important' }}
              onClick={() => setModalDelete(false)}
            >
              {cancel}
            </Button>
          </Stack>
        </Popup>
      )}

      <StyleFoodListItem ref={hoverRef}>
        {isHovered && isPoster && handleDeleteParent && handleUdapteDishParent && (
          <>
            <StyleEditDelete>
              <Box
                onClick={handleOnclick}
                sx={{
                  display: 'flex',
                  color: 'black',

                  fontSize: '0.875rem',
                  fontWeight: '700',
                  alignItems: 'center',
                }}
              >
                <HighlightOffIcon
                  sx={{
                    color: 'red',
                    cursor: 'pointer',
                    marginRight: '10px',
                  }}
                />
                {deleteEdit}
              </Box>
              <Box
                sx={{
                  color: 'black',
                  display: 'flex',
                  fontSize: '0.875rem',
                  fontWeight: '700',
                  alignItems: 'center',
                }}
                onClick={handleOpen}
              >
                <BorderColorIcon
                  sx={{ marginRight: '10px', cursor: 'pointer' }}
                />
                {editEdit}
              </Box>
            </StyleEditDelete>
          </>
        )}

        {dish.image?.url ? (
          <img src={dish.image?.url} alt="" />
        ) : (
          <Image
            objectFit="contain"
            width={230}
            height={150}
            src={defaultImage}
            alt=""
          />
        )}
        <Stack sx={{ flex: '1' }}>
          <StyleDes variant="h3" fontWeight={600} fontSize={25}>
            {dish.name}
          </StyleDes>
          <Stack mt={'auto'} spacing={1} alignItems="center" direction="row">
            <PaymentIcon />
            <Typography mt={1}>{getCurrency(dish.price)}</Typography>
          </Stack>
        </Stack>
        {isHovered && !isPoster && (
          <StyleOrders>
            <Input
              onChange={(e: any) => handleOnChangeQuantity(e)}
              onPaste={(e: any) => handlePaste(e)}
              onInput={(e: any) => handleInput(e)}
              value={quantity}
              disableUnderline
              type="number"
              disabled={expired}
              inputProps={{
                min: 1,
              }}
            />
            <Button
              disabled={expired || quantity == 0}
              variant="contained"
              onClick={() => handleOnAdd()}
            >
              <AddIcon />
            </Button>
          </StyleOrders>
        )}
      </StyleFoodListItem>
    </>
  )
}
