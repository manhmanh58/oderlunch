import React, { useState } from 'react'
import { StyleListItem, StyleItemInfo } from '../../Trend/style'
import { Stack } from '@mui/system'
import { Typography } from '@mui/material'
import Image from 'next/image'
import defaultImage from 'assets/images/default_food.jpg'
import { OrderedType } from '@/src/utils/type/orders'
import { getCurrency } from '@/src/utils/share/getCurrency'
import CloseIcon from '@mui/icons-material/Close'
import IconButton from '@mui/material/IconButton'
import Popup from '@/src/components/Popup'
import { StyleConfirm } from '../style'
import Button from '@/src/components/Button'
import { FormattedMessage } from 'react-intl'
type ListItemProps = {
  expired: boolean
  ordered: OrderedType & { index: number }
  setOrdereds: Function
}

export default function ListItemProps({
  ordered,
  setOrdereds,
  expired,
}: ListItemProps) {
  const [modalOpen, setModalOpen] = useState(false)
  const handleOnclick = () => {
    setOrdereds((prev: any) => {
      let dataList = [...prev]
      dataList.splice(ordered.index, 1)
      return dataList
    })
    setModalOpen(false)
  }
  return (
    <>
      {modalOpen && (
        <Popup setModalOpen={setModalOpen}>
          <StyleConfirm>
            <FormattedMessage id="listItem.title" />
          </StyleConfirm>
          <Stack direction="row" mt={3} mb={2}>
            <Button
              variant="contained"
              sx={{ marginLeft: 'auto' }}
              onClick={() => handleOnclick()}
            >
              <FormattedMessage id="listItem.comfirm" />
            </Button>
            <Button
              variant="contained"
              sx={{ marginLeft: '20px', background: 'red !important' }}
              onClick={() => setModalOpen(false)}
            >
              <FormattedMessage id="listItem.cancel" />
            </Button>
          </Stack>
        </Popup>
      )}
      <StyleListItem reverse>
        {ordered.image?.url ? (
          <img src={ordered.image.url} alt="" />
        ) : (
          <Image
            objectFit="contain"
            width={120}
            height={82}
            src={defaultImage}
            alt=""
          />
        )}
        <StyleItemInfo>
          <Typography
            style={{ cursor: 'pointer' }}
            fontSize={14}
            fontWeight={600}
            variant="h3"
          >
            {ordered.name}
          </Typography>
          <Stack mt={1}>
            <Stack direction="row" alignItems="center" spacing={1}>
              <Typography variant="h6" fontSize={12}>
                <FormattedMessage id="listItem.price" />{' '}
                {getCurrency(ordered.price)}
              </Typography>
            </Stack>
            <Stack direction="row" alignItems="center" spacing={1}>
              <Typography variant="h6" fontSize={12}>
                <FormattedMessage id="listItem.quantity" /> {ordered.quantity}
              </Typography>
            </Stack>
            <Stack mt={0} direction="row" alignItems="center" spacing={1}>
              <Typography sx={{ color: 'red' }} variant="h6" fontSize={12}>
                <FormattedMessage id="listItem.total" />{' '}
                {getCurrency(ordered.price * ordered.quantity)}
              </Typography>
            </Stack>
          </Stack>
        </StyleItemInfo>
        <IconButton
          disabled={expired}
          color="info"
          onClick={() => setModalOpen(true)}
        >
          <Stack
            sx={{ cursor: 'pointer' }}
            spacing={1}
            mt={1}
            direction="row"
            alignItems="center"
          >
            <CloseIcon sx={{ color: 'red' }} />
          </Stack>
        </IconButton>
      </StyleListItem>
    </>
  )
}
