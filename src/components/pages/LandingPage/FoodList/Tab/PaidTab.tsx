import postsApi from '@/src/services/postsApi'
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Stack,
  Typography,
  Avatar,
  Tooltip,
} from '@mui/material'
import React, { useEffect, useState } from 'react'
import SentimentSatisfiedAltIcon from '@mui/icons-material/SentimentSatisfiedAlt'
import { theme } from '@/src/theme'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import { getCurrency } from '@/src/utils/share/getCurrency'
import Button from '@/src/components/Button'
import Popup from '@/src/components/Popup'
import { StyleConfirm } from '../style'
import Image from 'next/image'
import defaultImage from 'assets/images/default_food.jpg'
import { StyleButton, StyleDelete } from './style'
import { StyleItemInfo, StyleListItem } from '../../Trend/style'
import { OrderedType } from 'utils/type/orders'
import { User } from 'utils/type/post'
import DoneAllIcon from '@mui/icons-material/DoneAll'
import RemoveDoneIcon from '@mui/icons-material/RemoveDone'
import { Pie } from 'react-chartjs-2'
import 'chart.js/auto'
import { PAID, UNPAID } from '@/src/constants/status'
import Link from 'next/link'
import { FormattedMessage, useIntl } from 'react-intl'
import getDateTimeString from '@/src/utils/share/getDateTimeString'

type PayTabProps = {
  postId: string
  isPoster: boolean
  payDatas?: Array<OrdersData>
  setPayDatas: Function
  fetchDataPayData: Function
}
type Status = {
  id: string
  status: string
  orderIndex: number
  userIndex: number
}
type Orders = {
  id: string
  note: string
  orderQuantities: Array<{ dish: OrderedType; quantity: number }>
  // user: User & { avatar: null | string }
  status: string
  createdAt: string
  totalPrice: number
}
type OrdersData = {
  id: string
  fullName: string
  avatar: string
  orders: Array<Orders>
  totalPrice: number
}
export default function Tab({
  postId,
  isPoster,
  payDatas,
  setPayDatas,
  fetchDataPayData,
}: PayTabProps) {
  const [chartDataPaid, setChartDataPaid]: [Array<number>, Function] = useState(
    []
  )
  const [noteFull, setNoteFull] = useState(false)

  const [isDeleteButton, setIsDeleteButton] = useState(false)
  const [chartDataMoney, setChartDataMoney]: [Array<number>, Function] =
    useState([])
  const handleOnChangStatus = async (
    id: string,
    status: string,
    orderIndex: number,
    userIndex: number
  ) => {
    try {
      const { data } = await postsApi.changeStatus(id, { status })
      if (data) {
        setPayDatas((prev: Array<OrdersData>) => {
          const clar = [...prev]
          clar[userIndex].orders[orderIndex].status = status
          return clar
        })
      }
    } catch (error) {}
  }

  const { formatMessage } = useIntl()
  const optionChart = (title: string, isUnit?: boolean) => {
    return {
      indexAxis: 'y' as const,
      elements: {
        bar: {
          borderWidth: 2,
        },
      },
      responsive: true,
      plugins: {
        legend: {
          position: 'bottom' as const,
        },
        title: {
          display: true,
          text: title,
        },
        ...(isUnit && {
          tooltip: {
            callbacks: {
              label: function (context: { label: string; parsed: number }) {
                return context.label + ' ' + getCurrency(context.parsed)
              },
            },
          },
        }),
      },
    }
  }
  const [modalOpen, setModalOpen] = useState(false)
  const [orderId, setOrderId] = useState('')
  const [status, setStatus] = useState<Status>({
    id: '',
    status: '',
    orderIndex: 0,
    userIndex: 0,
  })
  const handleCancel = () => {
    setIsDeleteButton(false), setModalOpen(false)
  }
  const handleOnclick = (
    id: string,
    status: string,
    orderIndex: number,
    userIndex: number
  ) => {
    setModalOpen(true)
    setStatus({
      id: id,
      status: status,
      orderIndex: orderIndex,
      userIndex: userIndex,
    })
  }
  const handleDeleteButton = (id: string) => {
    setModalOpen(true), setIsDeleteButton(true), setOrderId(id)
  }
  const handleDelete = async (id: string) => {
    try {
      const { data } = await postsApi.deleteOrder(id)
      if (data) {
        fetchDataPayData()
      }
    } catch (error) {}
  }

  useEffect(() => {
    fetchDataPayData()
  }, [postId])

  useEffect(() => {
    if (payDatas) {
      const handleFilterDataChart = () => {
        let sumPaid = 0
        let sumUnPaid = 0
        let sumMoney = 0
        let sumUnMoney = 0
        for (let i = 0; i < payDatas.length; i++) {
          for (let j = 0; j < payDatas[i].orders?.length; j++) {
            if (payDatas[i].orders[j].status === 'PAID') {
              sumPaid++
              sumMoney += payDatas[i].orders[j].totalPrice
            } else {
              sumUnPaid++
              sumUnMoney += payDatas[i].orders[j].totalPrice
            }
          }
        }
        return {
          paid: [sumUnPaid, sumPaid],
          money: [sumUnMoney, sumMoney],
        }
      }
      setChartDataPaid(handleFilterDataChart().paid)
      setChartDataMoney(handleFilterDataChart().money)
    }
  }, [payDatas])

  return (
    <>
      {chartDataPaid.filter((x) => x > 0).length ? (
        <Stack
          mt={3}
          alignItems="center"
          justifyContent="space-between"
          width="80%"
          direction="row"
          sx={{ margin: 'auto' }}
        >
          <Stack width="43%">
            <Pie
              datasetIdKey="id"
              options={optionChart(formatMessage({ id: 'payTab.title' }))}
              width="200px"
              height="200px"
              data={{
                labels: [
                  formatMessage({ id: 'payTab.unpaidLabel' }),
                  formatMessage({ id: 'payTab.paidLabel' }),
                ],
                datasets: [
                  {
                    data: chartDataPaid,
                    backgroundColor: [
                      theme.colors.primary,
                      theme.colors.success,
                    ],
                    hoverOffset: 4,
                  },
                ],
              }}
            />
          </Stack>
          <Stack width="43%">
            <Pie
              datasetIdKey="id"
              options={optionChart(
                formatMessage({ id: 'payTab.moneyChartTitle' }),
                true
              )}
              width="200px"
              height="200px"
              data={{
                labels: [
                  formatMessage({ id: 'payTab.unpaidMoney' }),
                  formatMessage({ id: 'payTab.paidMoney' }),
                ],
                datasets: [
                  {
                    label: '',
                    data: chartDataMoney,
                    backgroundColor: [theme.colors.money, theme.colors.debt],
                    hoverOffset: 4,
                  },
                ],
              }}
            />
          </Stack>
        </Stack>
      ) : null}
      <Stack mt={3} width="100%">
        {payDatas?.length !== 0 ? (
          payDatas?.map((or: any, userIndex: number) => (
            <Stack key={userIndex} width="100%" mt={1} mb={1}>
              <Accordion
                sx={{
                  boxShadow:
                    '0px 2px 1px 1px rgb(0 0 0 / 0%), 0px 1px 1px 0px rgb(0 0 0 / 1%), 0px 1px 3px 0px rgb(0 0 0 / 12%)',
                  paddingRight: '20px',
                }}
              >
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  sx={{
                    width: '100%',
                  }}
                >
                  <Stack
                    width="100%"
                    direction="row"
                    alignItems="center"
                    justifyContent="space-around"
                    paddingLeft="30px"
                  >
                    {payDatas && (
                      <Link href={`/detail/${or.id}`} legacyBehavior>
                        <Stack
                          spacing={1}
                          width="35%"
                          direction="row"
                          alignItems="center"
                        >
                          {or.avatar ? (
                            <Avatar src={or.avatar} />
                          ) : (
                            <Avatar>{or?.fullName?.[0]}</Avatar>
                          )}
                          <Typography variant="subtitle1">
                            {or.fullName}
                          </Typography>
                        </Stack>
                      </Link>
                    )}
                    <Stack
                      paddingTop="23px"
                      width="100%"
                      direction="row"
                      alignItems="center"
                      justifyContent="flex-end"
                    >
                      <Stack width="35%">
                        <Stack spacing={5} direction="row" alignItems="center">
                          <Stack>
                            <FormattedMessage id="payTab.totalQuantity" />
                            <Typography sx={{ color: theme.colors.primary }}>
                              {`${or.totalQuantity}`}
                            </Typography>
                          </Stack>
                        </Stack>
                      </Stack>
                      <Stack width="35%">
                        <Stack spacing={5} direction="row" alignItems="center">
                          <Stack>
                            <FormattedMessage id="payTab.orders" />
                            <Typography sx={{ color: theme.colors.primary }}>
                              {or.orders?.length}
                            </Typography>
                          </Stack>
                        </Stack>
                      </Stack>
                      <Stack width="35%">
                        <Stack spacing={5} direction="row" alignItems="center">
                          <Stack>
                            <FormattedMessage id="payTab.total" />
                            <Typography sx={{ color: theme.colors.primary }}>
                              {`${or.totalPrice} VND`}
                            </Typography>
                          </Stack>
                        </Stack>
                      </Stack>
                    </Stack>
                  </Stack>
                </AccordionSummary>
                <AccordionDetails
                  sx={{ paddingRight: '0px', marginRight: '-10px' }}
                >
                  {or?.orders?.map((order: any, orderIndex: number) => (
                    <Stack key={orderIndex} width="100%" mt={1} mb={1}>
                      <Accordion
                        defaultExpanded={true}
                        sx={{
                          boxShadow:
                            '0px 2px 1px 1px rgb(0 0 0 / 0%), 0px 1px 1px 0px rgb(0 0 0 / 1%), 0px 1px 3px 0px rgb(0 0 0 / 12%)',
                          paddingRight: '10px',
                        }}
                      >
                        <AccordionSummary
                          expandIcon={<ExpandMoreIcon />}
                          sx={{
                            width: '100%',
                          }}
                        >
                          <React.Fragment key={orderIndex}>
                            <Stack
                              width="100%"
                              direction="row"
                              alignItems="center"
                              justifyContent="space-around"
                            >
                              <Tooltip
                                title={
                                  or.status === PAID
                                    ? formatMessage({
                                        id: 'payTab.paidLabel',
                                      })
                                    : formatMessage({
                                        id: 'payTab.unpaidLabel',
                                      })
                                }
                                placement="top"
                              >
                                {order.status === PAID ? (
                                  <DoneAllIcon
                                    sx={{
                                      marginRight: '20px',
                                      marginLeft: '20px',
                                      color: theme.colors.success,
                                    }}
                                  />
                                ) : (
                                  <RemoveDoneIcon
                                    sx={{
                                      marginRight: '20px',
                                      marginLeft: '20px',
                                      color: theme.colors.warning,
                                    }}
                                  />
                                )}
                              </Tooltip>
                              <Stack
                                width="100%"
                                direction="row"
                                alignItems="center"
                                justifyContent="flex-end"
                              >
                                {order.note && (
                                  <Stack
                                    width="30%"
                                    marginLeft="18px"
                                    paddingTop="20px"
                                  >
                                    <Typography variant="subtitle1">
                                      Note:
                                      <Typography
                                        onClick={() => {
                                          setNoteFull(!noteFull)
                                        }}
                                        sx={{
                                          color: theme.colors.primary,
                                          display: '-webkit-box',
                                          overflow: 'hidden',
                                          WebkitBoxOrient: 'vertical',
                                          WebkitLineClamp: !noteFull ? 2 : 4,
                                          wordBreak: 'break-word',
                                        }}
                                      >
                                        {order.note}
                                      </Typography>
                                    </Typography>
                                  </Stack>
                                )}
                                <Stack width="30%" marginLeft="20px">
                                  <Stack>
                                    <FormattedMessage id="payTab.create" />
                                    <Typography
                                      sx={{
                                        color: theme.colors.primary,
                                        margin: '0',
                                      }}
                                    >
                                      {getDateTimeString(order.createdAt)}
                                    </Typography>
                                  </Stack>
                                </Stack>
                                <Stack width="26.5%">
                                  <Stack direction="row" alignItems="center">
                                    <Stack>
                                      <FormattedMessage id="payTab.total" />
                                      <Typography
                                        sx={{ color: theme.colors.primary }}
                                      >
                                        {`${order.totalPrice} VND`}
                                      </Typography>
                                    </Stack>
                                  </Stack>
                                </Stack>
                              </Stack>
                            </Stack>
                          </React.Fragment>
                        </AccordionSummary>
                        <AccordionDetails sx={{ paddingRight: '0px' }}>
                          {order.orderQuantities.map(
                            (ordered: any, index: number) => (
                              <React.Fragment key={index}>
                                <StyleListItem reverse>
                                  {ordered.dish.image?.url ? (
                                    <img
                                      src={ordered?.dish.image?.url}
                                      alt=""
                                    />
                                  ) : (
                                    <Image
                                      objectFit="contain"
                                      width={120}
                                      height={82}
                                      src={defaultImage}
                                      alt=""
                                    />
                                  )}
                                  <StyleItemInfo>
                                    <Typography
                                      style={{ cursor: 'pointer' }}
                                      fontSize={14}
                                      fontWeight={600}
                                      variant="h3"
                                    >
                                      {ordered.name}
                                    </Typography>
                                    <Stack mt={1}>
                                      <Stack>
                                        <Typography variant="h6" fontSize={12}>
                                          {ordered.dish.name}
                                        </Typography>
                                      </Stack>
                                      <Stack
                                        direction="row"
                                        alignItems="center"
                                        spacing={1}
                                      >
                                        <Typography variant="h6" fontSize={12}>
                                          <FormattedMessage id="payTab.price" />{' '}
                                          {getCurrency(ordered.dish.price)}
                                        </Typography>
                                      </Stack>
                                      <Stack
                                        direction="row"
                                        alignItems="center"
                                        spacing={1}
                                      >
                                        <Typography variant="h6" fontSize={12}>
                                          <FormattedMessage id="payTab.quantity" />{' '}
                                          {ordered.quantity}
                                        </Typography>
                                      </Stack>
                                      <Stack
                                        mt={0}
                                        direction="row"
                                        alignItems="center"
                                        spacing={1}
                                      >
                                        <Typography
                                          sx={{ color: 'red' }}
                                          variant="h6"
                                          fontSize={12}
                                        >
                                          <FormattedMessage id="payTab.total" />
                                          {getCurrency(
                                            ordered.dish.price *
                                              ordered.quantity
                                          )}
                                        </Typography>
                                      </Stack>
                                    </Stack>
                                  </StyleItemInfo>
                                </StyleListItem>
                              </React.Fragment>
                            )
                          )}
                          {isPoster && (
                            <StyleButton
                              variant="contained"
                              onClick={() =>
                                handleOnclick(
                                  order.id,
                                  order.status,
                                  orderIndex,
                                  userIndex
                                )
                              }
                              disabled={order.status === UNPAID ? false : true}
                            >
                              {order.status === UNPAID
                                ? formatMessage({ id: 'payTab.unpaidLabel' })
                                : formatMessage({ id: 'payTab.paidLabel' })}
                            </StyleButton>
                          )}
                          <StyleDelete
                            variant="contained"
                            onClick={() => {
                              handleDeleteButton(order.id)
                            }}
                          >
                            <FormattedMessage id="payTab.deleteButton" />
                          </StyleDelete>
                        </AccordionDetails>
                      </Accordion>
                    </Stack>
                  ))}
                </AccordionDetails>
              </Accordion>
            </Stack>
          ))
        ) : (
          <Stack
            sx={{ color: theme.colors.primary }}
            width="100%"
            direction="row"
            alignItems="center"
            justifyContent="center"
          >
            <Typography variant="h4" align="center">
              <FormattedMessage id="payTab.empty" />
            </Typography>
            <SentimentSatisfiedAltIcon
              fontSize="large"
              sx={{ marginLeft: 1 }}
            />
          </Stack>
        )}
      </Stack>
      {modalOpen &&
        (isDeleteButton ? (
          <Popup setModalOpen={setModalOpen}>
            <StyleConfirm>
              {' '}
              <FormattedMessage id="payTab.deleteTitle" />
            </StyleConfirm>
            <Stack
              direction="row"
              mt={3}
              mb={2}
              sx={{
                justifyContent: 'space-around',
              }}
            >
              <Button
                variant="contained"
                sx={{ marginLeft: '0 40px' }}
                onClick={() => {
                  handleDelete(orderId),
                    setModalOpen(false),
                    setIsDeleteButton(false)
                }}
              >
                <FormattedMessage id="payTab.delete" />
              </Button>
              <Button
                variant="contained"
                sx={{
                  marginLeft: '20px',
                  background: 'red !important',
                }}
                onClick={() => handleCancel()}
              >
                <FormattedMessage id="payTab.cancel" />
              </Button>
            </Stack>
          </Popup>
        ) : (
          <Popup setModalOpen={setModalOpen}>
            <StyleConfirm>
              <FormattedMessage id="payTab.comfirm" />
            </StyleConfirm>
            <Stack
              direction="row"
              mt={3}
              mb={2}
              sx={{
                justifyContent: 'space-around',
              }}
            >
              <Button
                variant="contained"
                sx={{ marginLeft: '0 40px' }}
                onClick={() => {
                  handleOnChangStatus(
                    status.id,
                    status.status === UNPAID ? PAID : UNPAID,
                    status.orderIndex,
                    status.userIndex
                  ),
                    setModalOpen(false)
                }}
              >
                <FormattedMessage id="payTab.paidLabel" />
              </Button>
              <Button
                variant="contained"
                sx={{
                  marginLeft: '20px',
                  background: 'red !important',
                }}
                onClick={() => handleCancel()}
              >
                <FormattedMessage id="payTab.cancel" />
              </Button>
            </Stack>
          </Popup>
        ))}
    </>
  )
}
