import postsApi from '@/src/services/postsApi'
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Stack,
  Typography,
} from '@mui/material'
import React, { useEffect, useState } from 'react'
import SentimentSatisfiedAltIcon from '@mui/icons-material/SentimentSatisfiedAlt'
import { theme } from '@/src/theme'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import { getCurrency } from '@/src/utils/share/getCurrency'
import Image from 'next/image'
import defaultImage from 'assets/images/default_food.jpg'
import { StyleItemInfo, StyleListItem } from '../../Trend/style'
import { OrderedType } from 'utils/type/orders'
import { User } from 'utils/type/post'
import { FormattedMessage } from 'react-intl'
interface PayTabProps {
  postId: string
}

export default function ConFirmed({ postId }: PayTabProps) {
  const [orders, setOrders] = useState([])
  const fetchData = async () => {
    try {
      const { data } = await postsApi.getOrdered(postId)
      if (data) {
        setOrders(data.orders)
      }
    } catch (error) {}
  }
  useEffect(() => {
    fetchData()
  }, [])
  return (
    <>
      {orders && orders.length > 0 ? (
        orders.map(
          (
            or: {
              id: number
              note: string
              orderQuantities: Array<{ dish: OrderedType; quantity: number }>
              user: User & { avatar: null | string }
            },
            index: number
          ) => (
            <Stack key={index} width="100%" mt={1} mb={1}>
              <Accordion
                sx={{
                  boxShadow:
                    '0px 2px 1px 1px rgb(0 0 0 / 0%), 0px 1px 1px 0px rgb(0 0 0 / 1%), 0px 1px 3px 0px rgb(0 0 0 / 12%)',
                }}
              >
                <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                  <Stack width="100%" direction="row" alignItems="center">
                    <Stack width="30%">
                      <Stack spacing={10} direction="row" alignItems="center">
                        <Typography>
                          <FormattedMessage id="comFirm.amount" />
                          <Typography sx={{ color: theme.colors.primary }}>
                            {or.orderQuantities.length}
                          </Typography>
                        </Typography>
                        <Typography>
                          <FormattedMessage id="comFirm.total" />
                          <Typography sx={{ color: theme.colors.primary }}>
                            {getCurrency(
                              or.orderQuantities.reduce(
                                (previousValue, currentValue) =>
                                  previousValue +
                                  +currentValue.quantity *
                                    +currentValue.dish.price,
                                0
                              )
                            )}
                          </Typography>
                        </Typography>
                      </Stack>
                    </Stack>
                    <Stack width="30%">
                      {or.note && (
                        <Typography variant="subtitle1">
                          Note:
                          <Typography sx={{ color: theme.colors.primary }}>
                            {or.note}
                          </Typography>
                        </Typography>
                      )}
                    </Stack>
                  </Stack>
                </AccordionSummary>
                <AccordionDetails>
                  <Stack>
                    {or.orderQuantities.map((ordered: any, index: number) => (
                      <React.Fragment key={index}>
                        <StyleListItem reverse>
                          {ordered.dish.image?.url ? (
                            <img src={ordered?.dish.image?.url} alt="" />
                          ) : (
                            <Image
                              objectFit="contain"
                              width={120}
                              height={82}
                              src={defaultImage}
                              alt=""
                            />
                          )}
                          <StyleItemInfo>
                            <Typography
                              style={{ cursor: 'pointer' }}
                              fontSize={14}
                              fontWeight={600}
                              variant="h3"
                            >
                              {ordered.name}
                            </Typography>
                            <Stack mt={1}>
                              <Stack>
                                <Typography variant="h6" fontSize={12}>
                                  {ordered.dish.name}
                                </Typography>
                              </Stack>
                              <Stack
                                direction="row"
                                alignItems="center"
                                spacing={1}
                              >
                                <Typography variant="h6" fontSize={12}>
                                  <FormattedMessage id="comFirm.price" />{' '}
                                  {getCurrency(ordered.dish.price)}
                                </Typography>
                              </Stack>
                              <Stack
                                direction="row"
                                alignItems="center"
                                spacing={1}
                              >
                                <Typography variant="h6" fontSize={12}>
                                  <FormattedMessage id="comFirm.quantity" />{' '}
                                  {ordered.quantity}
                                </Typography>
                              </Stack>
                              <Stack
                                mt={0}
                                direction="row"
                                alignItems="center"
                                spacing={1}
                              >
                                <Typography
                                  sx={{ color: 'red' }}
                                  variant="h6"
                                  fontSize={12}
                                >
                                  <FormattedMessage id="comFirm.total" />
                                  {getCurrency(
                                    ordered.dish.price * ordered.quantity
                                  )}
                                </Typography>
                              </Stack>
                            </Stack>
                          </StyleItemInfo>
                        </StyleListItem>
                      </React.Fragment>
                    ))}
                  </Stack>
                </AccordionDetails>
              </Accordion>
            </Stack>
          )
        )
      ) : (
        <Stack
          sx={{ color: theme.colors.primary }}
          width="100%"
          direction="row"
          alignItems="center"
          justifyContent="center"
        >
          <Typography variant="h4" align="center">
            <FormattedMessage id="comFirm.error" />
          </Typography>
          <SentimentSatisfiedAltIcon fontSize="large" sx={{ marginLeft: 1 }} />
        </Stack>
      )}
    </>
  )
}
