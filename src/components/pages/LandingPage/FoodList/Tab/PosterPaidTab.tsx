import postsApi from '@/src/services/postsApi'
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Stack,
  Typography,
  Avatar,
  Tooltip,
} from '@mui/material'
import React, { useEffect, useState } from 'react'
import SentimentSatisfiedAltIcon from '@mui/icons-material/SentimentSatisfiedAlt'
import { theme } from '@/src/theme'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import { getCurrency } from '@/src/utils/share/getCurrency'
import Button from '@/src/components/Button'
import Popup from '@/src/components/Popup'
import { StyleConfirm } from '../style'
import Image from 'next/image'
import defaultImage from 'assets/images/default_food.jpg'
import { StyleButton } from './style'
import { StyleItemInfo, StyleListItem } from '../../Trend/style'
import { OrderedType } from 'utils/type/orders'
import { User } from 'utils/type/post'
import DoneAllIcon from '@mui/icons-material/DoneAll'
import RemoveDoneIcon from '@mui/icons-material/RemoveDone'
import { Pie } from 'react-chartjs-2'
import 'chart.js/auto'
import { PAID, UNPAID } from '@/src/constants/status'
import Link from 'next/link'
import { FormattedMessage, useIntl } from 'react-intl'

import dateFormat, { masks } from 'dateformat'

type PayTabProps = {
  postId: string
  isPoster: boolean
  payDatas?: Array<Orders>
  setPayDatas: Function
  fetchDataPayData: Function
}
type Status = {
  id: number
  status: string
  orderIndex: number
}
type Orders = {
  id: number
  note: string
  orderQuantities: Array<{ dish: OrderedType; quantity: number }>
  status: string
  createdAt: string
  totalPrice: number
}
type OrdersData = {
  id: string
  fullName: string
  avatar: string
  orders: Array<Orders>
}
export default function PosterPaidTab({
  postId,
  isPoster,
  payDatas,

  fetchDataPayData,
}: PayTabProps) {
  const [chartDataPaid, setChartDataPaid]: [Array<number>, Function] = useState(
    []
  )
  const [chartDataMoney, setChartDataMoney]: [Array<number>, Function] =
    useState([])
  const [noteFull, setNoteFull] = useState(false)
  const { formatMessage } = useIntl()
  const optionChart = (title: string, isUnit?: boolean) => {
    return {
      indexAxis: 'y' as const,
      elements: {
        bar: {
          borderWidth: 2,
        },
      },
      responsive: true,
      plugins: {
        legend: {
          position: 'bottom' as const,
        },
        title: {
          display: true,
          text: title,
        },
        ...(isUnit && {
          tooltip: {
            callbacks: {
              label: function (context: { label: string; parsed: number }) {
                return context.label + ' ' + getCurrency(context.parsed)
              },
            },
          },
        }),
      },
    }
  }
  useEffect(() => {
    fetchDataPayData()
  }, [postId])
  useEffect(() => {
    if (payDatas) {
      const handleFilterDataChart = () => {
        let sumPaid = 0
        let sumUnPaid = 0
        let sumMoney = 0
        let sumUnMoney = 0
        for (let i = 0; i < payDatas.length; i++) {
          if (payDatas[i].status === 'PAID') {
            sumPaid++
            sumMoney += payDatas[i].totalPrice
          } else {
            sumUnPaid++
            sumUnMoney += payDatas[i].totalPrice
          }
        }
        return {
          paid: [sumUnPaid, sumPaid],
          money: [sumUnMoney, sumMoney],
        }
      }
      setChartDataPaid(handleFilterDataChart().paid)
      setChartDataMoney(handleFilterDataChart().money)
    }
  }, [payDatas])

  return (
    <>
      {chartDataPaid.filter((x) => x > 0).length ? (
        <Stack
          mt={3}
          alignItems="center"
          justifyContent="space-between"
          width="80%"
          direction="row"
          sx={{ margin: 'auto' }}
        >
          <Stack width="43%">
            <Pie
              datasetIdKey="id"
              options={optionChart(formatMessage({ id: 'payTab.title' }))}
              width="200px"
              height="200px"
              data={{
                labels: [
                  formatMessage({ id: 'payTab.unpaidLabel' }),
                  formatMessage({ id: 'payTab.paidLabel' }),
                ],
                datasets: [
                  {
                    data: chartDataPaid,
                    backgroundColor: [
                      theme.colors.primary,
                      theme.colors.success,
                    ],
                    hoverOffset: 4,
                  },
                ],
              }}
            />
          </Stack>
          <Stack width="43%">
            <Pie
              datasetIdKey="id"
              options={optionChart(
                formatMessage({ id: 'payTab.moneyChartTitle' }),
                true
              )}
              width="200px"
              height="200px"
              data={{
                labels: [
                  formatMessage({ id: 'payTab.unpaidMoney' }),
                  formatMessage({ id: 'payTab.paidMoney' }),
                ],
                datasets: [
                  {
                    label: '',
                    data: chartDataMoney,
                    backgroundColor: [theme.colors.money, theme.colors.debt],
                    hoverOffset: 4,
                  },
                ],
              }}
            />
          </Stack>
        </Stack>
      ) : null}
      <Stack mt={3} width="100%">
        {payDatas?.length !== 0 ? (
          payDatas?.map((order: any, orderIndex: number) => (
            <Stack key={orderIndex} width="100%" mt={1} mb={1}>
              <Accordion
                sx={{
                  boxShadow:
                    '0px 2px 1px 1px rgb(0 0 0 / 0%), 0px 1px 1px 0px rgb(0 0 0 / 1%), 0px 1px 3px 0px rgb(0 0 0 / 12%)',
                }}
              >
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  sx={{
                    width: '100%',
                  }}
                >
                  <React.Fragment key={orderIndex}>
                    <Stack
                      width="100%"
                      direction="row"
                      alignItems="center"
                      justifyContent="space-around"
                    >
                      <Tooltip
                        title={
                          order?.status === PAID
                            ? formatMessage({
                                id: 'payTab.paidLabel',
                              })
                            : formatMessage({
                                id: 'payTab.unpaidLabel',
                              })
                        }
                        placement="top"
                      >
                        {order?.status === PAID ? (
                          <DoneAllIcon
                            sx={{
                              marginRight: '20px',
                              marginLeft: '20px',
                              color: theme.colors.success,
                            }}
                          />
                        ) : (
                          <RemoveDoneIcon
                            sx={{
                              marginRight: '20px',
                              marginLeft: '20px',
                              color: theme.colors.warning,
                            }}
                          />
                        )}
                      </Tooltip>
                      <Stack width="30%" marginLeft="60px">
                        <Typography variant="subtitle1">
                          <FormattedMessage id="payTab.quantity" />
                          <Typography sx={{ color: theme.colors.primary }}>
                            {order?.totalQuantity}
                          </Typography>
                        </Typography>
                      </Stack>
                      <Stack width="30%" marginLeft="20px">
                        <Typography variant="subtitle1">
                          <FormattedMessage id="payTab.create" />
                          <Typography sx={{ color: theme.colors.primary }}>
                            {dateFormat(order?.createdAt, 'hh:MM dd/mm/yyyy ')}
                          </Typography>
                        </Typography>
                      </Stack>
                      <Stack width="35%">
                        <Stack
                          direction="row"
                          alignItems="center"
                          marginLeft="60px"
                        >
                          <Stack>
                            <FormattedMessage id="payTab.total" />
                            <Typography
                              sx={{
                                color: theme.colors.primary,
                                marginTop: '12px',
                              }}
                            >
                              {`${order.totalPrice} VND`}
                            </Typography>
                          </Stack>
                        </Stack>
                      </Stack>
                      <Stack width="30%" marginTop="10px">
                        {order.note && (
                          <Typography variant="subtitle1">
                            Note:
                            <Typography
                              onClick={() => {
                                setNoteFull(!noteFull)
                              }}
                              sx={{
                                color: theme.colors.primary,
                                display: '-webkit-box',
                                overflow: 'hidden',
                                WebkitBoxOrient: 'vertical',
                                WebkitLineClamp: !noteFull ? 2 : 4,
                                wordBreak: 'break-word',
                              }}
                            >
                              {order.note}
                            </Typography>
                          </Typography>
                        )}
                      </Stack>
                    </Stack>
                  </React.Fragment>
                </AccordionSummary>
                <AccordionDetails>
                  {order.orderQuantities?.map((ordered: any, index: number) => (
                    <React.Fragment key={index}>
                      <StyleListItem reverse>
                        {ordered.dish.image?.url ? (
                          <img src={ordered?.dish.image?.url} alt="" />
                        ) : (
                          <Image
                            objectFit="contain"
                            width={120}
                            height={82}
                            src={defaultImage}
                            alt=""
                          />
                        )}
                        <StyleItemInfo>
                          <Typography
                            style={{ cursor: 'pointer' }}
                            fontSize={14}
                            fontWeight={600}
                            variant="h3"
                          >
                            {ordered.name}
                          </Typography>
                          <Stack mt={1}>
                            <Stack>
                              <Typography variant="h6" fontSize={12}>
                                {ordered.dish.name}
                              </Typography>
                            </Stack>
                            <Stack
                              direction="row"
                              alignItems="center"
                              spacing={1}
                            >
                              <Typography variant="h6" fontSize={12}>
                                <FormattedMessage id="payTab.price" />{' '}
                                {getCurrency(ordered.dish.price)}
                              </Typography>
                            </Stack>
                            <Stack
                              direction="row"
                              alignItems="center"
                              spacing={1}
                            >
                              <Typography variant="h6" fontSize={12}>
                                <FormattedMessage id="payTab.quantity" />{' '}
                                {ordered.quantity}
                              </Typography>
                            </Stack>
                            <Stack
                              mt={0}
                              direction="row"
                              alignItems="center"
                              spacing={1}
                            >
                              <Typography
                                sx={{ color: 'red' }}
                                variant="h6"
                                fontSize={12}
                              >
                                <FormattedMessage id="payTab.total" />
                                {getCurrency(
                                  ordered.dish.price * ordered.quantity
                                )}
                              </Typography>
                            </Stack>
                          </Stack>
                        </StyleItemInfo>
                      </StyleListItem>
                    </React.Fragment>
                  ))}
                </AccordionDetails>
              </Accordion>
            </Stack>
          ))
        ) : (
          <Stack
            sx={{ color: theme.colors.primary }}
            width="100%"
            direction="row"
            alignItems="center"
            justifyContent="center"
          >
            <Typography variant="h4" align="center">
              <FormattedMessage id="payTab.empty" />
            </Typography>
            <SentimentSatisfiedAltIcon
              fontSize="large"
              sx={{ marginLeft: 1 }}
            />
          </Stack>
        )}
      </Stack>
    </>
  )
}
