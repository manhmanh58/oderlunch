import InputField from '@/src/components/InputField'
import { postSchema } from '@/src/constants/postSchema'
import { Dishes } from '@/src/utils/type/dishes'
import { yupResolver } from '@hookform/resolvers/yup'
import {
  Box,
  Button,
  CircularProgress,
  FormHelperText,
  InputLabel,
  Modal,
  Stack,
  Typography,
} from '@mui/material'
import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useIntl } from 'react-intl'
import { getURLImage } from '@/src/utils/share/getURLImage'
import PostBanner from '../../../CreatePost/PostBanner'
import { dishSchema } from '@/src/constants/dishSchema'
import { getTranslate } from '@/src/utils/share/translate'
interface Iprops {
  open: boolean
  setOpen: Function
  name: string
  price: number
  url: string | undefined
  handleUdaptePostParent: Function
  id: string
}
function EditListItem(props: Iprops) {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(dishSchema),
    mode: 'onChange',
  })
  const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  }
  const { formatMessage } = useIntl()
  const [loading, setLoading] = useState<boolean>(false)
  const [bannerFile, setBannerFile] = useState<boolean>()
  const nameDish = formatMessage({ id: 'dishTable.foodName' })
  const priceDish = formatMessage({ id: 'dishTable.price' })
  const editDish = formatMessage({ id: 'editDish.title' })
  const comfirm = formatMessage({ id: 'listItem.comfirm' })
  const cancel = formatMessage({ id: 'listItem.cancel' })
  const title = formatMessage({ id: 'edit.enterTitle' })
  const price = formatMessage({ id: 'edit.enterPrice' })
  const [errorMsg, setErrorMsg] = useState<string>()
  const [isSuccess, setIsSuccess] = useState(false)
  const handleClose = () => props.setOpen(false)

  const handleClickSubmit = async (data: any) => {
    setLoading(true)

    let URLbanner = null
    if (typeof bannerFile === 'string') {
      URLbanner = bannerFile
    } else if (typeof bannerFile === 'object') {
      URLbanner = await getURLImage(bannerFile)
    }
    let infomationUpdateDish = {
      id: props.id,
      name: data.name,
      price: data.price,
      url: URLbanner,
    }
    props.handleUdaptePostParent(infomationUpdateDish)
    setLoading(false)
    props.setOpen(false)
  }

  return (
    <Modal
      open={props.open}
      onClose={handleClose}
      aria-labelledby="parent-modal-title"
      aria-describedby="parent-modal-description"
    >
      <Box sx={style}>
        <Typography variant="h5" component="h2" sx={{ color: 'black' }}>
          {editDish}
        </Typography>
        <PostBanner
          setBannerFile={setBannerFile}
          setIsSuccess={setIsSuccess}
          setErrorMsg={setErrorMsg}
          bannerCuurent={props.url}
          height={250}
        />
        <FormHelperText sx={{ color: 'red' }}>{errorMsg}</FormHelperText>

        <form onSubmit={handleSubmit(handleClickSubmit)}>
          <Stack spacing={5} mt={5} direction="column">
            <Stack>
              <InputLabel htmlFor="title">{nameDish}</InputLabel>
              <InputField
                fullWidth
                defaultValue={props.name}
                id="name"
                type="text"
                placeholder={title}
                {...register('name')}
              />
              {errors.name && (
                <FormHelperText sx={{ color: 'red' }}>
                  {getTranslate(errors.name.message as string)}
                </FormHelperText>
              )}
            </Stack>
            <Stack>
              <InputLabel>{priceDish}</InputLabel>
              <InputField
                fullWidth
                defaultValue={props.price}
                id="price"
                type="text"
                placeholder={price}
                {...register('price')}
              />

              {errors.price && (
                <FormHelperText sx={{ color: 'red' }}>
                  {getTranslate(errors.price.message as string)}
                </FormHelperText>
              )}
            </Stack>
          </Stack>

          <Box sx={{ display: 'flex', justifyContent: 'end', mt: '20px' }}>
            {loading ? (
              <CircularProgress size={30} color="success" />
            ) : (
              <>
                <Button
                  variant="outlined"
                  color="error"
                  sx={{ mr: '10px' }}
                  onClick={handleClose}
                >
                  {cancel}
                </Button>
                <Button variant="contained" type="submit">
                  {comfirm}
                </Button>
              </>
            )}
          </Box>
        </form>
      </Box>
    </Modal>
  )
}

export default EditListItem
