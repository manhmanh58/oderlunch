import styled from 'styled-components'
import { Button } from '@material-ui/core'

const MyButton = styled(Button)`
  &&& {
    &.Mui-disabled {
      color: #fff;
      background: #000;
    }
  }
`

const StyleOrdered = styled.div`
  width: 300px;
  margin-top: 30px;
`
const StylePagination = styled.div`
  position: absolute;
  bottom: 20px;
  right: 10px;
`

const StyleTabPanelItem: any = styled.div`
  padding: 10px 20px;
  font-size: 16px;
  font-weight: 600;
  cursor: pointer;
  ${(props: { active?: boolean; theme: any }) =>
    props.active &&
    `
    border-bottom: 3px solid ${props.theme.colors.primary};
    color: ${props.theme.colors.primary}
  ;`}
`

const StyleTabPanel = styled.div`
  display: flex;
  align-items: center;
  border-bottom: 1px solid ${(props) => props.theme.colors.gray};
  margin-top: 10px;
`

const StyleTabContent = styled.div`
  margin-top: 20px;
`
const StyleButton = styled(Button)`
  &&& {
    width: 100%;
    margin-top: 20px;
    margin-bottom: 20px;
    color: #fff;
    background: orange;
    &.Mui-disabled {
      color: #a7a6a6;
      background: #d2d0d0;
    }
  }
`
const StyleDelete = styled(Button)`
  &&& {
    width: 100%;
    margin-top: 0px;
    margin-bottom: 20px;
    color: #fff;
    background: #ff1717;
    &.Mui-disabled {
      color: #a7a6a6;
      background: #d2d0d0;
    }
  }
`
export {
  StyleOrdered,
  StylePagination,
  StyleTabPanelItem,
  StyleTabPanel,
  StyleTabContent,
  StyleButton,
  StyleDelete,
}
