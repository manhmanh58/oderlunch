import React, { useState } from 'react'
import { OrderedType } from '@/src/utils/type/orders'
import { Typography, Stack, FormHelperText } from '@mui/material'
import ListItem from './ListItem'
import Button from '@/src/components/Button'
import InputField from '@/src/components/InputField'
import postsApi from '@/src/services/postsApi'
import Popup from '@/src/components/Popup'
import { StyleConfirm } from '../style'
import { FormattedMessage, useIntl } from 'react-intl'
import { useRouter } from 'next/router'

type ChoosenTabProps = {
  expired: boolean
  orderTab: Array<OrderedType>
  page: number
  ordereds?: Array<OrderedType>
  setOrdereds: Function
  setPage: Function
  lsSetOrdereds: Function
  tab?: string
  setSuccess: Function
  setOpen: Function
  succsess: string
}

export default function ChoosenTab({
  orderTab,
  page,
  setOrdereds,
  tab,
  lsSetOrdereds,
  expired,
  setSuccess,
  setOpen,
  succsess,
}: ChoosenTabProps) {
  const [note, setNote] = useState('')
  const [modalOpen, setModalOpen] = useState(false)
  const [modalDelete, setModalDelete] = useState(false)
  const [errorNote, setErrorNote] = useState(false)
  const [errorOrders, setErrorOrders] = useState(false)
  const { formatMessage } = useIntl()
  const router = useRouter()

  const handleConfirm = async () => {
    try {
      const { data } = await postsApi.createOrder({
        postId: orderTab[0].postId,
        data: {
          note: note,
          dishes: orderTab.map((x: any) => ({
            dishId: x.id,
            quantity: +x.quantity,
          })),
        },
      })
      setSuccess(formatMessage({ id: 'foodList.orderConfirmationSuccess' }))
      setOpen(true)
      if (data) {
        setOrdereds((prev: Array<OrderedType & { index: number }>) => {
          const clar = prev.filter((x: OrderedType) => x.postId !== tab)
          lsSetOrdereds(clar)
          return clar
        })
      }
    } catch (error) {
      setErrorOrders(true)
    }
    setModalOpen(false)
  }

  const handleDeleteAll = () => {
    setModalDelete(true)
  }

  const handleConfirmDelete = () => {
    succsess && setSuccess('')
    setModalDelete(false)
    setOpen(true)
    setSuccess(formatMessage({ id: 'foodList.deleteAllSuccess' }))
    setOrdereds((prev: Array<OrderedType & { index: number }>) => {
      const clar = prev.filter((x: OrderedType) => x.postId !== tab)
      lsSetOrdereds(clar)
      return clar
    })
  }

  return (
    <>
      {errorOrders && (
        <Popup setModalOpen={setErrorOrders}>
          <StyleConfirm>
            <FormattedMessage id="choosenTab.errorOrders" />
          </StyleConfirm>
          <Stack mt={5} mb={2} alignItems="center">
            <Button
              onClick={() =>
                router.push({
                  pathname: '/profile/report',
                  query: { from: 'chooseTab' },
                })
              }
              sx={{ width: 150 }}
              variant="contained"
            >
              <FormattedMessage id="choosenTab.confirmOrders" />
            </Button>
          </Stack>
        </Popup>
      )}
      {modalDelete && (
        <Popup setModalOpen={setModalDelete}>
          <StyleConfirm>
            <FormattedMessage id="listItem.title" />
          </StyleConfirm>
          <Stack direction="row" mt={3} mb={2}>
            <Button
              variant="contained"
              sx={{ marginLeft: 'auto' }}
              onClick={() => handleConfirmDelete()}
            >
              <FormattedMessage id="listItem.comfirm" />
            </Button>
            <Button
              variant="contained"
              sx={{ marginLeft: '20px', background: 'red !important' }}
              onClick={() => setModalDelete(false)}
            >
              <FormattedMessage id="listItem.cancel" />
            </Button>
          </Stack>
        </Popup>
      )}
      {modalOpen && (
        <Popup setModalOpen={setModalOpen}>
          <StyleConfirm>
            <FormattedMessage id="choosenTab.comfirmBox" />
          </StyleConfirm>
          <Stack direction="row" mt={3} mb={2}>
            <Button
              variant="contained"
              sx={{ marginLeft: 'auto' }}
              onClick={() => handleConfirm()}
            >
              <FormattedMessage id="choosenTab.comfirmButton" />
            </Button>
            <Button
              variant="contained"
              sx={{ marginLeft: '20px', background: 'red !important' }}
              onClick={() => setModalOpen(false)}
            >
              <FormattedMessage id="choosenTab.cancelButton" />
            </Button>
          </Stack>
        </Popup>
      )}
      <Stack width="45%">
        {orderTab && orderTab.length > 0 && (
          <Typography
            sx={{ color: 'red', cursor: 'pointer', margin: '10px 0px' }}
            fontSize={18}
            fontWeight={600}
            variant="h3"
            align="right"
            onClick={() => handleDeleteAll()}
          >
            <FormattedMessage id="choosenTab.deleteButton" />
          </Typography>
        )}
        <FormattedMessage id="choosenTab.note">
          {(placeholder) => (
            <InputField
              disabled={expired}
              value={note}
              onChange={(e: any) => setNote(e.target.value)}
              placeholder={placeholder}
            />
          )}
        </FormattedMessage>
        <FormHelperText sx={{ color: 'red' }}>
          {errorNote && <FormattedMessage id="choosenTab.errorNote" />}
        </FormHelperText>

        {orderTab &&
          orderTab
            .slice((page - 1) * 5, page * 5)
            .map((ordered: any, index: number) => (
              <ListItem
                expired={expired}
                setOrdereds={setOrdereds}
                key={index}
                ordered={ordered}
              />
            ))}
        {orderTab.length > 0 && (
          <Stack mt={3}>
            {!expired ? (
              <Button variant="contained" onClick={() => setModalOpen(true)}>
                <FormattedMessage id="choosenTab.comfirmButton" />
              </Button>
            ) : (
              <Button disabled={expired} variant="contained">
                <FormattedMessage id="choosenTab.timeOutButton" />
              </Button>
            )}
          </Stack>
        )}
      </Stack>
    </>
  )
}
