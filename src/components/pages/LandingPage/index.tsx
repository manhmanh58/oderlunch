import postsApi from '@/src/services/postsApi'
import Container from 'components/Container'
import React, { useEffect, useState } from 'react'
import CountDown from './CountDown'
import FoodList from './FoodList'
import {
  StyleWrapper,
  StyleBanner,
  StyleDefautlBanner,
  StyleFullHeight,
} from './style'
import Trend from './Trend'
import { Post } from '@/src/utils/type/post'
import HideImageIcon from '@mui/icons-material/HideImage'
import withRoute from '@/src/hocs/withRoute'
import FloatButton from 'components/FloatButton'
import { OrderedType } from '@/src/utils/type/orders'
import useLocalStorage from '@/src/hooks/useLocalStorage'
import Popup from '../../Popup'
import { Typography, Stack, Snackbar, Alert } from '@mui/material'
import SentimentVeryDissatisfiedIcon from '@mui/icons-material/SentimentVeryDissatisfied'
import Button from 'components/Button'
import { useRouter } from 'next/router'
import Loading from '../../Loading'
import { FormattedMessage, useIntl } from 'react-intl'

export default withRoute(function LandingPage() {
  const { formatMessage } = useIntl()
  const url = new URL(window.location.href)
  const initialPage = url?.searchParams?.get('page')
    ? parseInt(url?.searchParams?.get('page')!)
    : 0
  const [tab, setTab] = useState('')
  const [expired, setExpired] = useState(false)
  const [posts, setPosts] = useState<Array<Post>>()
  const [endTime, setEndTime] = useState()
  const [banner, setBanner] = useState()
  const [ordereds, setOrdereds] = useState<Array<OrderedType>>([])
  const [page, setPage] = useState(initialPage || 1)
  const [count, setCount] = useState()
  const [lsOrdereds, lsSetOrdereds] = useLocalStorage('ordereds', [])
  const [userId, setUserId] = useState(0)
  const [loading, setLoading] = useState(true)
  const router = useRouter()
  const [open, setOpen] = useState(false)
  const [error, setError] = useState<string>()
  const [succsess, setSuccess] = useState<string>()

  const handleClose = (
    event?: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === 'clickaway') {
      return
    }
    setOpen(false)
  }
  const cleanStorage = () => {
    if (posts) {
      const newArray = lsOrdereds.filter((x: { postId: string }) =>
        posts.some((y) => y.id === x.postId)
      )
      lsSetOrdereds(newArray)
    }
  }
  const fetchData = async () => {
    try {
      const { data } = await postsApi.getList(page || 1)

      const initialPostId = url.searchParams.get('id')
        ? url.searchParams.get('id')
        : false
      setCount(data.pages)
      setPosts(data.posts)
      setTab(
        initialPostId &&
          data.posts.find((post: Post) => post.id === initialPostId)
          ? url.searchParams.get('id')
          : data.posts?.[0]?.id
      )
      setUserId(data.posts?.[0]?.userId)
      setEndTime(data.posts?.[0]?.endTime)
      setBanner(data.posts?.[0]?.image?.url)
      setLoading(false)
    } catch (error) {}
  }

  const fetchDataPostUpdate = async (props: any) => {
    try {
      const { data } = await postsApi.getList(page)
      setCount(data.pages)
      setPosts(data.posts)
      setTab(props.id)
      setEndTime(props?.endTime)
      setBanner(props?.url)
      setLoading(false)
    } catch (error) {}
  }
  useEffect(() => {
    lsSetOrdereds(ordereds)
  }, [ordereds])
  useEffect(() => {
    fetchData()
  }, [page])
  useEffect(() => {
    if (lsOrdereds) {
      setOrdereds(lsOrdereds)
    }
    if (posts) {
      cleanStorage()
    }
  }, [])
  const handleDeleteParent = (props: number) => {
    error && setError('')
    succsess && setSuccess('')
    try {
      const deletePost = async () => {
        await postsApi.deletePost(props)
        fetchData()
      }

      deletePost()
      setSuccess(formatMessage({ id: 'trend.deleteSuccess' }))
      setOpen(true)
    } catch (error: any) {
      setError(error.response?.data?.message)
      setOpen(true)
    }
  }
  const handleUdaptePostParent = (props: any) => {
    error && setError('')
    succsess && setSuccess('')

    try {
      const updatePost = async () => {
        await postsApi.updatePost(props?.id, {
          title: props?.title,
          endTime: props?.endTime,
          thumbnail: {
            url: props?.url,
          },
        })
        fetchDataPostUpdate(props)
      }
      updatePost()
      setSuccess(formatMessage({ id: 'trend.updateSuccess' }))

      setOpen(true)
    } catch (error: any) {
      setError(error.response?.data?.message)
      setOpen(true)
    }
  }

  useEffect(() => {
    router.replace({
      query: { id: tab, page: page },
    })
  }, [tab, page])
  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <>
          {!(posts && posts?.length > 0) ? (
            <>
              <Popup setModalOpen={() => {}}>
                <Stack mt={1} justifyContent="center" alignItems="center">
                  <Typography align="center" variant="h5">
                    <FormattedMessage id="trend.alert" />
                  </Typography>
                  <SentimentVeryDissatisfiedIcon
                    sx={{ fontSize: 100, color: '#f0a970', margin: '20px 0px' }}
                  />
                </Stack>
                <Stack
                  mt={1}
                  mb={1}
                  direction="row"
                  justifyContent="space-between"
                >
                  <Button
                    variant="contained"
                    onClick={() => router.push('/create-post')}
                  >
                    <FormattedMessage id="trend.post" />
                  </Button>
                  <Button
                    variant="contained"
                    onClick={() => router.push('/profile')}
                  >
                    <FormattedMessage id="trend.page" />
                  </Button>
                </Stack>
              </Popup>
              <StyleFullHeight />
            </>
          ) : (
            <>
              <Container>
                {banner ? (
                  <StyleBanner src={banner} alt="" />
                ) : (
                  <StyleDefautlBanner>
                    <HideImageIcon />
                  </StyleDefautlBanner>
                )}
                {endTime && tab && (
                  <CountDown tab={tab} time={endTime} setExpired={setExpired} />
                )}
                {router?.query?.id && (
                  <StyleWrapper>
                    <Trend
                      count={count}
                      page={page}
                      setPage={setPage}
                      setEndTime={setEndTime}
                      setTab={setTab}
                      tab={tab}
                      allPosts={posts}
                      setBanner={setBanner}
                      setUserId={setUserId}
                      handleDeleteParent={handleDeleteParent}
                      handleUdaptePostParent={handleUdaptePostParent}
                    />
                    {tab && (
                      <FoodList
                        setOrdereds={setOrdereds}
                        expired={expired}
                        tab={tab}
                        lsSetOrdereds={lsSetOrdereds}
                        ordereds={ordereds}
                        userId={userId}
                      />
                    )}
                  </StyleWrapper>
                )}

                <Snackbar
                  open={open}
                  autoHideDuration={4000}
                  onClose={handleClose}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                >
                  {!error ? (
                    <Alert
                      onClose={handleClose}
                      severity="success"
                      sx={{ width: '100%' }}
                    >
                      {succsess}
                    </Alert>
                  ) : (
                    <Alert severity="error" color="error">
                      {error}
                    </Alert>
                  )}
                </Snackbar>
              </Container>
              <FloatButton />
            </>
          )}
        </>
      )}
    </>
  )
})
