import Popup from '@/src/components/Popup'
import React, { useEffect, useState } from 'react'
import { Container, Stack } from '@mui/system'
import {
  Button,
  CircularProgress,
  FormControlLabel,
  FormLabel,
  InputLabel,
  MenuItem,
  Radio,
  RadioGroup,
  Typography,
} from '@material-ui/core'
import PostBanner from '../../CreatePost/PostBanner'
import InputField from '@/src/components/InputField'
import Dish from './Dish'
import { Dishes } from '@/src/utils/type/dishes'
import useCreateDishes from '@/src/hooks/useCreateDishs'
import getEndTimeOptions from '@/src/utils/share/getEndTimeOption'
import { END_TIME_OPTIONS } from '@/src/constants'
import Select from '@/src/components/Select'
import { theme } from '@/src/theme'
import { postSchema } from '@/src/constants/postSchema'
import { getURLImage } from '@/src/utils/share/getURLImage'
import { FormattedMessage, useIntl } from 'react-intl'

type ClonePopUpProps = {
  handleOnClose: Function
  data: any
  index: number
  width?: string
  handleOnClone: Function
}

export default function ClonePopUp({
  handleOnClose,
  data,
  width,
  index,
  handleOnClone,
}: ClonePopUpProps) {
  const [bannerFile, setBannerFile] = useState()
  const { dishes, addDishes, deleteDishes, onChangeValue } = useCreateDishes(
    data.dishes
  )
  const [endTime, setEndTime] = useState(END_TIME_OPTIONS['start'])
  const [validateTitle, setValidateTitle] = useState('')
  const [showError, setShowError] = useState()
  const [isSuccess, setIsSuccess] = useState()
  const [postTitle, setPostTitle] = useState()
  const [isLoading, setIsLoading] = useState(false)
  const [errorMsg, setErrorMsg] = useState('')
  const [isSave, setIsSave] = useState(0)
  const { formatMessage } = useIntl()
  const handleOnChange = async (e: any) => {
    setPostTitle(e.target.value)
    try {
      const validate = await postSchema.validateAt('title', {
        title: e.target.value,
      })
      setValidateTitle('')
    } catch (errors: any) {
      setValidateTitle(
        formatMessage({ id: errors.toString().split(':')[1].trim() })
      )
    }
  }

  const isErrorDishes = () => {
    errorMsg && setErrorMsg('')
    for (let i = 0; i < dishes.length; i++) {
      if (dishes[i]?.erorrs?.name || dishes[i]?.erorrs?.price) {
        return true
      }
      if (!dishes[i]?.name || !dishes[i]?.price) {
        setErrorMsg(formatMessage({ id: 'saved.popup.validate.priceAndName' }))
        return true
      }
    }

    return false
  }

  const onClone = async () => {
    setIsLoading(true)
    const URLbanner =
      typeof bannerFile !== 'string'
        ? await getURLImage(bannerFile)
        : bannerFile
    if (!isErrorDishes() && !validateTitle && !showError) {
      const coverNumber = await Promise.all(
        dishes.map(async (x) => {
          const dishUrl =
            typeof x?.image?.url !== 'string'
              ? await getURLImage(x?.image?.url)
              : x?.image?.url
          return {
            name: x.name,
            price: +x.price,
            image: {
              url: dishUrl,
            },
          }
        })
      )
      let tzoffset = new Date().getTimezoneOffset() * 60000
      const date = new Date()
      let endTimeDate: any = new Date(date.getTime() + endTime * 60000)
      let localISOTime = new Date(endTimeDate - tzoffset).toISOString()
      const body = {
        title: postTitle || data.title,
        endTime: localISOTime,
        thumbnail: {
          url: URLbanner,
        },
        dishes: coverNumber,
        ...(isSave ? { saved: true } : { saved: false }),
      }
      await handleOnClone(body)
    }
    setIsLoading(false)
  }
  return (
    <Popup width={width} setModalOpen={handleOnClose}>
      <Stack style={{ width: '100%', height: 450 }}>
        <Stack direction={'row'}>
          <Stack style={{ width: '30%', marginRight: 10 }}>
            <PostBanner
              height={150}
              setBannerFile={setBannerFile}
              bannerCuurent={data?.image?.url}
              setErrorMsg={setShowError}
              setIsSuccess={setIsSuccess}
            />
            {
              <Typography
                align="left"
                style={{ color: 'red' }}
                variant="subtitle2"
              >
                {showError}
              </Typography>
            }

            <Typography style={{ marginTop: 5 }} align="left">
              <FormattedMessage id="saved.popup.title" />
            </Typography>
            <InputField
              defaultValue={data.title}
              onChange={(e: any) => handleOnChange(e)}
              value={postTitle}
            />
            {validateTitle && (
              <Typography
                align="left"
                style={{ color: 'red' }}
                variant="subtitle2"
              >
                {validateTitle}
              </Typography>
            )}

            <Typography
              style={{ marginTop: 15, marginBottom: 15 }}
              align="left"
            >
              <FormattedMessage id="saved.popup.endTime" />
            </Typography>
            <Select
              value={endTime}
              onChange={(e: any) => setEndTime(e.target.value)}
            >
              <>
                {getEndTimeOptions(10, 10, 60).map((value) => (
                  <option value={value}>{value}</option>
                ))}
              </>
            </Select>
            <Typography style={{ marginTop: 10 }} align="left">
              <FormattedMessage id="saved.popup.savePost" />
            </Typography>
            <RadioGroup
              row
              aria-labelledby="save"
              defaultValue="0"
              name="radio-buttons-group"
              onChange={(e: any) => setIsSave(e.target.value)}
            >
              <FormControlLabel
                value="1"
                control={<Radio />}
                label={formatMessage({ id: 'saved.popup.save' })}
              />
              <FormControlLabel
                value="0"
                control={<Radio />}
                label={formatMessage({ id: 'saved.popup.noSave' })}
              />
            </RadioGroup>
          </Stack>
          <hr />
          <Stack
            style={{
              width: '70%',
              marginLeft: 10,
              overflow: 'auto',
              height: 400,
            }}
          >
            <Typography style={{ marginBottom: 20 }} variant="h6">
              <FormattedMessage id="saved.popup.listOfDish" />
            </Typography>
            {dishes.length ? (
              dishes.map((dish: any, index: number) => (
                <Dish
                  handleOnDelete={deleteDishes}
                  index={index}
                  dish={dish}
                  onChangeValue={onChangeValue}
                  key={dish.id}
                  errorMsg={errorMsg}
                />
              ))
            ) : (
              <></>
            )}
            <Button
              onClick={() => addDishes()}
              variant="contained"
              color="primary"
              style={{
                marginLeft: 'auto',
                marginTop: 1,
                width: '100%',
                color: '#fff',
                background: theme.colors.primary,
              }}
            >
              <FormattedMessage id="saved.popup.addDish" />
            </Button>
          </Stack>
        </Stack>
      </Stack>
      <Stack spacing={2} direction="row">
        {isLoading ? (
          <CircularProgress
            style={{
              marginLeft: 'auto',
            }}
            size={30}
          />
        ) : (
          <>
            <Button
              style={{
                width: 100,
                marginLeft: 'auto',
                color: '#fff',
                background: theme.colors.primary,
              }}
              onClick={() => onClone()}
            >
              <FormattedMessage id="saved.popup.clone" />
            </Button>
            <Button
              style={{ width: 100, background: 'red', color: '#fff' }}
              onClick={() => handleOnClose(null)}
              variant="contained"
            >
              <FormattedMessage id="saved.popup.cancel" />
            </Button>
          </>
        )}
      </Stack>
    </Popup>
  )
}
