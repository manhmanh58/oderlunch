import React, { useState } from 'react'
import { Stack } from '@mui/system'
import { Typography } from '@material-ui/core'
import InputField from '@/src/components/InputField'
import CloseIcon from '@mui/icons-material/Close'
import { StyleAddImage } from '../../CreatePost/DishesTable/style'
import { Dishes } from '@/src/utils/type/dishes'
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate'
import { FormattedMessage, useIntl } from 'react-intl'
import { getTranslate } from '@/src/utils/share/translate'
type DishProps = {
  dish: Dishes & {
    erorrs?: {
      name: string
      price: string
      image: string
    }
  }
  index: number
  handleOnDelete: Function
  onChangeValue: Function
  errorMsg: string | undefined
}

export default function Dish({
  dish,
  index,
  handleOnDelete,
  onChangeValue,
  errorMsg,
}: DishProps) {
  const [error, setError] = useState('')
  const { formatMessage } = useIntl()
  const handleOnchange = (e: any) => {
    const file = e.target.files[0]
    const MAX_FILE_SIZE = 10 // 10MB
    const imageMimeType = /image\/(png|jpg|jpeg)/i
    if (!file) {
      return
    }
    const fileSizeKiloBytes = file.size / 1024 / 1024 //MB
    if (fileSizeKiloBytes > MAX_FILE_SIZE) {
      setError(formatMessage({ id: 'createPost.errorImgSize' }))
      return
    }
    if (!file?.type.match(imageMimeType)) {
      setError(formatMessage({ id: 'edit.type' }))
      return
    }
    setError('')
    onChangeValue(file, index, 'image')
  }

  return (
    <>
      <Stack style={{ marginBottom: 20 }} direction={'row'}>
        <Stack style={{ width: 165 }}>
          <StyleAddImage htmlFor={'upload' + index + dish.name}>
            {dish?.image?.url ? (
              <img
                src={
                  typeof dish?.image?.url === 'string'
                    ? dish?.image?.url
                    : URL.createObjectURL(dish.image.url)
                }
                alt=""
              />
            ) : (
              <AddPhotoAlternateIcon />
            )}
            <input
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                handleOnchange(e)
              }
              accept="image/*"
              type="file"
              id={'upload' + index + dish.name}
            />
          </StyleAddImage>
          {error && (
            <Typography
              align="left"
              style={{ color: 'red' }}
              variant="subtitle2"
            >
              {error}
            </Typography>
          )}
        </Stack>
        <Stack style={{ marginLeft: 5, width: '100%' }}>
          <Stack>
            <Typography align="left">
              <FormattedMessage id="saved.popup.dishName" />
            </Typography>
            <InputField
              name={'name'}
              onChange={(e: any) => onChangeValue(e, index)}
              defaultValue={dish.name}
              fullWidth
            />
          </Stack>
          {dish.erorrs?.name && (
            <Typography
              align="left"
              style={{ color: 'red' }}
              variant="subtitle2"
            >
              {getTranslate(dish.erorrs.name)}
            </Typography>
          )}
        </Stack>
        <Stack style={{ marginLeft: 5, width: '100%' }}>
          <Stack>
            <Typography align="left">
              <FormattedMessage id="saved.popup.price" />
            </Typography>
            <InputField
              name={'price'}
              onChange={(e: any) => onChangeValue(e, index)}
              defaultValue={dish.price}
            />
          </Stack>
          {dish.erorrs?.price && (
            <Typography
              align="left"
              style={{ color: 'red' }}
              variant="subtitle2"
            >
              {getTranslate(dish.erorrs.price)}
            </Typography>
          )}
        </Stack>
        <Stack style={{ marginLeft: 5 }} onClick={() => handleOnDelete(index)}>
          <CloseIcon style={{ color: 'red', cursor: 'pointer' }} />
        </Stack>
      </Stack>

      {!dish.name && !dish.price && !dish.erorrs?.price && !dish.erorrs?.name && (
        <Typography align="left" style={{ color: 'red' }} variant="subtitle2">
          {errorMsg}
        </Typography>
      )}
    </>
  )
}
