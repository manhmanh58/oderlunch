import Head from 'next/head'
import { useEffect, useState } from 'react'
import { Container, Content, Details } from 'styles/profile'
import {
  ContainerProfile,
  FormEdit,
  StyleTitle,
  Title,
  TextInfo,
  EditInput,
  TitleBank,
  ContentLeft,
  ContentRight,
  Bank,
  BankNumber,
  BankCode,
  FieldInput,
  Image,
  FieldInputInfo,
  ButtonImg,
} from 'styles/profile/edit'
import UserProfile from './UserDetail'
import authAPI from '@/src/services/AuthAPI'
import { useForm } from 'react-hook-form'
import { Skeleton } from '@mui/material'
import { yupResolver } from '@hookform/resolvers/yup'
import { editSchema } from '@/src/constants/editSchema'
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate'
import { FormattedMessage } from 'react-intl'
import { useRouter } from 'next/router'

interface CurrentUser {
  avatar?: string
  bankNumber?: number
  bankOwner?: string
  email?: string | undefined
  fullName?: string
  phoneNumber?: number
  qrCode?: string
  username?: string
  bankName?: string
  name?: string | any
}

function EditProfile() {
  const [urlAvt, setUrlAvt] = useState()
  const [urlQR, setUrlQR] = useState()
  const [statusQr, setStatusQr] = useState<string>('none')
  const [data, setData] = useState<CurrentUser>()
  const router = useRouter()
  const id = router.query.id as string

  const {
    register,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(editSchema),
  })

  useEffect(() => {
    const getCharacters = async () => {
      const response = await authAPI.getUserProfile(id)
      setData(response.data.user)
      setUrlAvt(response.data.user.avatar)
      setUrlQR(response.data.user.qrCode)
    }
    getCharacters()
      // make sure to catch any error
      .catch(console.error)
  }, [id])

  return (
    <Container>
      <Details>
        <UserProfile />
      </Details>
      <Content>
        <Head>
          <title>User Profile</title>
        </Head>
        <ContainerProfile>
          <ContentLeft>
            <StyleTitle>
              <Title>
                <FormattedMessage id="userProfile.information" />
              </Title>
            </StyleTitle>
            <FormEdit>
              <TitleBank style={{ color: 'orange' }}>
                <FormattedMessage id="userProfile.edit" />
              </TitleBank>
              <EditInput>
                <FormattedMessage id="userProfile.userName">
                  {(label) => (
                    <FieldInputInfo
                      autoFocus
                      {...register('username')}
                      id="outlined-required"
                      label={label}
                      value={data?.username || ''}
                      disabled
                    />
                  )}
                </FormattedMessage>
              </EditInput>
              <EditInput>
                <FormattedMessage id="userProfile.fullName">
                  {(label) => (
                    <FieldInputInfo
                      {...register('fullName')}
                      id="outlined-required"
                      label={label}
                      value={data?.fullName || ''}
                      disabled
                    />
                  )}
                </FormattedMessage>
              </EditInput>
              <EditInput>
                <FieldInputInfo
                  {...register('email')}
                  id="outlined-required"
                  label="Email"
                  value={data?.email || ''}
                  disabled
                />
              </EditInput>
              <EditInput style={{ marginBottom: 5 }}>
                <FormattedMessage id="userProfile.phone">
                  {(label) => (
                    <FieldInputInfo
                      {...register('phoneNumber')}
                      id="outlined-required"
                      label={label}
                      value={data?.phoneNumber || ''}
                      disabled
                    />
                  )}
                </FormattedMessage>
              </EditInput>
              <Bank>
                <BankNumber>
                  <TitleBank style={{ color: 'orange' }}>
                    <FormattedMessage id="userProfile.bank" />
                  </TitleBank>
                  <EditInput>
                    <FormattedMessage id="userProfile.number">
                      {(label) => (
                        <FieldInput
                          id="outlined-required"
                          label={label}
                          {...register('bankNumber')}
                          value={data?.bankNumber || ''}
                          disabled
                        />
                      )}
                    </FormattedMessage>
                  </EditInput>
                  <EditInput>
                    <FormattedMessage id="userProfile.bankName">
                      {(label) => (
                        <FieldInput
                          id="outlined-required"
                          label={label}
                          {...register('bankName')}
                          value={data?.bankName || ''}
                          disabled
                        />
                      )}
                    </FormattedMessage>
                  </EditInput>
                  <EditInput>
                    <FormattedMessage id="userProfile.owner">
                      {(label) => (
                        <FieldInput
                          id="outlined-required"
                          label={label}
                          {...register('bankOwner')}
                          value={data?.bankOwner || ''}
                          disabled
                        />
                      )}
                    </FormattedMessage>
                  </EditInput>
                </BankNumber>
              </Bank>
            </FormEdit>
          </ContentLeft>
          <ContentRight>
            <BankCode>
              <TitleBank style={{ color: 'blue', paddingBottom: 15 }}>
                QR Code
              </TitleBank>
              <ButtonImg>
                {statusQr === 'loading' && (
                  <Skeleton
                    variant="rectangular"
                    width="200px"
                    height="180px"
                  />
                )}
                {urlQR ? (
                  <>
                    {statusQr !== 'loading' && (
                      <Image src={urlQR} alt="image code QR" />
                    )}
                  </>
                ) : (
                  <>
                    {statusQr !== 'loading' && (
                      <AddPhotoAlternateIcon sx={{ width: 200, height: 180 }} />
                    )}
                  </>
                )}
              </ButtonImg>
            </BankCode>
          </ContentRight>
        </ContainerProfile>
      </Content>
    </Container>
  )
}

export default EditProfile
