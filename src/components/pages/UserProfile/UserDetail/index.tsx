import { Info } from 'styles/profile/content'
import { EditProfile, Name, StyleImage } from 'styles/profile/details'

import Avatar from '@mui/material/Avatar'

import { useEffect, useState } from 'react'

import authAPI from '@/src/services/AuthAPI'

import { Skeleton } from '@mui/material'
import { useRouter } from 'next/router'

function UserProfile() {
  const [name, setName] = useState('')

  const [statusImg, setStatusImg] = useState<string>('none')
  const [url, setUrl] = useState('')
  const router = useRouter()
  const id = router.query.id as string
  useEffect(() => {
    const getCharacters = async () => {
      const response = await authAPI.getUserProfile(id)
      setUrl(response.data.user.avatar)
      setName(response.data.user.fullName)
    }
    getCharacters()
      // make sure to catch any error
      .catch(console.error)
  }, [])

  return (
    <Info>
      <StyleImage>
        {statusImg === 'loading' && (
          <Skeleton variant="circular" width="70px" height="70px" />
        )}
        {statusImg !== 'loading' && (
          <Avatar src={url} sx={{ width: 70, height: 70 }} />
        )}
        <EditProfile>
          <Name>{name}</Name>
        </EditProfile>
      </StyleImage>
    </Info>
  )
}

export default UserProfile
