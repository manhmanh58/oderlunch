import React, { forwardRef } from 'react'
import { MenuItem, Select } from '@mui/material'

const SelectField = forwardRef((props: any, ref: any) => {
  const { label, items, ...render } = props
  const timeSlect = items
    ? items.map((item: any) => (
        <MenuItem key={item.key} value={item.value}>
          {item.value}
        </MenuItem>
      ))
    : undefined
  return (
    <Select disableUnderline {...render} ref={ref}>
      {timeSlect}
    </Select>
  )
})
export default SelectField
