import React, { forwardRef } from 'react'
import { StyleInput } from './style'
import Input from '@mui/material/Input'

const InputField = forwardRef((props: any, ref: any) => {
  return (
    <StyleInput>
      <Input fullWidth disableUnderline {...props} ref={ref} />
    </StyleInput>
  )
})
export default InputField
