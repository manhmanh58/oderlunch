import styled from "styled-components";

const StyleInput = styled.div`
  border-bottom: 1px solid ${(props) => props.theme.colors.primary};
  opacity: 1;
  outline: 0;
  &:hover {
    border-bottom: 2px solid ${(props) => props.theme.colors.primary};
  };
`;

export {
  StyleInput
}
