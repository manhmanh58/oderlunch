import styled from 'styled-components'

const StyleOverLay = styled.div`
  position: fixed;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.4);
  z-index: 9999;
`

const StyleModal = styled.div`
  position: absolute;
  max-height: 600px;
  min-width: 500px;
  ${(props: { width?: string }) =>
    props.width
      ? `
    width: ${props.width}; max-width: ${props.width}
  ;`
      : `max-width:'600px';`}
  background: #fff;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  border-radius: 8px;
  padding: 15px;
`

export { StyleModal, StyleOverLay }
