import useOnClickOutside from "hooks/useOnClickOutside";
import React, { useEffect } from "react";
import { useRef } from "react";
import {
  StyleOverLay,
  StyleModal,
} from "./styles";


interface PopupProps {
  setModalOpen: Function;
  children: any;
  width?: string
}

const Popup = ({ setModalOpen, children, width }: PopupProps) => {
  const ref = useRef(null);
  useOnClickOutside(ref, () => setModalOpen(false));
  function disableScrolling() {
    var x = window.scrollX;
    var y = window.scrollY;
    window.onscroll = function () { window.scrollTo(x, y); };
  }

  function enableScrolling() {
    window.onscroll = null;
  }
  useEffect(() => {
    disableScrolling();
    return () => enableScrolling();
  }, [])
  return (
    <StyleOverLay>
      <StyleModal width={width} ref={ref}>
        {
          children
        }
      </StyleModal>
    </StyleOverLay>
  );
};

export default Popup;
