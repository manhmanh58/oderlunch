import * as React from 'react'
import Footer from '../components/Footer'
import Header from '../components/Header'

export interface LayoutProps {}

interface Props {
  children: React.ReactNode
}

const Layouts: React.FC<Props> = ({ children }) => {
  const [isLoggedIn, setIsLoggedIn] = React.useState()

  React.useEffect(() => {
    if (typeof window !== 'undefined') {
      // Perform localStorage action
      return setIsLoggedIn(localStorage['token'])
    }
  })

  return (
    <>
      {!isLoggedIn ? (
        <div className="app-root">{children}</div>
      ) : (
        <>
          <div className="app-root">
            <Header />
            {children}
            <Footer />
          </div>
        </>
      )}
    </>
  )
}

export default Layouts
