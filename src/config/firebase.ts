import { initializeApp } from 'firebase/app'
import { getFirestore } from 'firebase/firestore/lite'
import { getAuth } from 'firebase/auth'
import { getStorage } from "firebase/storage";

const firebaseConfig = {
    apiKey: 'AIzaSyBsFhLttKhk5vvMnES9V968FTkZ2u2Ha9s',
    authDomain: 'ool-techchain.firebaseapp.com',
    projectId: 'ool-techchain',
    storageBucket: 'ool-techchain.appspot.com',
    messagingSenderId: '586877265423',
    appId: '1:586877265423:web:379ebdaf5dfa947099e57a',
}

const firebaseApp = initializeApp(firebaseConfig)
export const db = getFirestore(firebaseApp)
export const storage = getStorage(firebaseApp)
export const auth = getAuth(firebaseApp)
