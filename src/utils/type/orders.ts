type OrderedType = {
  id: string
  name: string
  price: number
  image?: {
    url: any
  }
  postId: string
  quantity: number
}

export type { OrderedType }
