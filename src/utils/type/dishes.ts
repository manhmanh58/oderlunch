type Dishes = {
  name: string
  price: string
  image?: {
    url?: any
  }
}
export type { Dishes }
