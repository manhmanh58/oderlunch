type Post = {
  id: string
  endTime: string
  title: string
  image: {
    url?: any
  }
  user: User
  createdAt: string
  saved: boolean
}

type User = {
  fullName: string
  id: number
}
export type { Post, User }
