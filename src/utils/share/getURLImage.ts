import { ref, uploadBytes, getDownloadURL } from 'firebase/storage'
import { storage } from '@/src/config/firebase'

export const getURLImage = async (file: any, folder?: string) => {
  try {
    const storageRef = ref(
      storage,
      `/${folder || 'items'}/${file.name + new Date().getTime()}`
    )
    await uploadBytes(storageRef, file)
    const url = await getDownloadURL(storageRef)
    if (url) {
      return url
    }
  } catch (error) {
    return null
  }
}
