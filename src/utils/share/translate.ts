import { useIntl } from 'react-intl'

export function getTranslate(key: string) {
  const intl = useIntl()
  return intl.formatMessage({ id: key })
}
