export function getCurrency(num: number) {
  return num?.toLocaleString('it-IT', {
    style: 'currency',
    currency: 'VND',
  })
}
