export default function getEndTimeOptions(start:number, step:number, end:number){
  const result = []
  for(let i = start; i <=end; i+=step ){
    result.push(i)
  }
  return result
}