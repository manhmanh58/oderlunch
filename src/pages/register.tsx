import { registerSchema } from '@/src/constants'
import {
  FormRegister,
  StyleRegister,
  BtnLogin,
  Button,
  Error,
  Input,
  LabelLogin,
  Option,
  Question,
  SpanEyeConfirm,
  SpanEyeRegister,
  ContentInput,
  LinkAuth,
  LoginDiv,
  SpanLogin,
} from '@/src/styles/register'
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye'
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import { useState } from 'react'
import Head from 'next/head'
import { useRouter } from 'next/router'
import authAPI from '../services/AuthAPI'
import withRoute from '../hocs/withRoute'
import { Alert, Snackbar, Stack } from '@mui/material'
import Loading from '../components/Loading'
import { FormattedMessage, useIntl } from 'react-intl'
import { getTranslate } from '@/src/utils/share/translate'

function Register() {
  const [showPass, setShowPass] = useState(false)
  const [showPassConfirm, setShowPassConfirm] = useState(false)
  const router = useRouter()
  const [open, setOpen] = useState(false)
  const [error, setError] = useState<string>()
  const [loading, setLoading] = useState<boolean>(false)
  const { locale } = useRouter()
  const { formatMessage } = useIntl()

  const handleClose = (
    event?: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === 'clickaway') {
      return
    }
    setOpen(false)
  }

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(registerSchema) })

  const handleSignup = async (dt: any) => {
    error && setError('')
    delete dt.confirmPassword
    try {
      setLoading(true)

      const { data: token } = await authAPI.register(dt)
      setOpen(true)

      setTimeout(() => {
        router.push('/login')

        setLoading(false)
      }, 700)
    } catch (error: any) {
      setError(formatMessage({ id: 'register.emailError' }))
      setLoading(false)
      setOpen(true)
    }
  }

  return (
    <>
      {loading ? <Loading /> : null}
      <FormRegister onSubmit={handleSubmit(handleSignup)} autoComplete="off">
        <Head>
          <title>Order office lunch</title>
        </Head>
        <StyleRegister>
          {locale === 'vi' ? (
            <LoginDiv>
              <SpanLogin style={{ '--item': 1 } as any}>Đ</SpanLogin>
              <SpanLogin style={{ '--item': 2 } as any}>Ă</SpanLogin>
              <SpanLogin style={{ '--item': 3 } as any}>N</SpanLogin>
              <SpanLogin style={{ '--item': 4, paddingRight: 5 } as any}>
                G
              </SpanLogin>
              <SpanLogin style={{ '--item': 5, paddingLeft: 5 } as any}>
                K
              </SpanLogin>
              <SpanLogin style={{ '--item': 6 } as any}>Ý</SpanLogin>
            </LoginDiv>
          ) : (
            <LoginDiv>
              <SpanLogin style={{ '--item': 1 } as any}>R</SpanLogin>
              <SpanLogin style={{ '--item': 2 } as any}>E</SpanLogin>
              <SpanLogin style={{ '--item': 3 } as any}>S</SpanLogin>
              <SpanLogin style={{ '--item': 4 } as any}>I</SpanLogin>
              <SpanLogin style={{ '--item': 5 } as any}>S</SpanLogin>
              <SpanLogin style={{ '--item': 6 } as any}>T</SpanLogin>
              <SpanLogin style={{ '--item': 7 } as any}>E</SpanLogin>
              <SpanLogin style={{ '--item': 8 } as any}>R</SpanLogin>
            </LoginDiv>
          )}
          <LabelLogin>
            {' '}
            <FormattedMessage id="register.fullName" />
          </LabelLogin>
          <FormattedMessage id="register.name">
            {(label: any) => (
              <Input
                placeholder={label}
                type="text"
                autoFocus
                {...register('fullName')}
              />
            )}
          </FormattedMessage>
          {errors.fullName && (
            <Error>{getTranslate(errors.fullName.message as string)}</Error>
          )}
          <LabelLogin>Email</LabelLogin>
          <FormattedMessage id="register.email">
            {(label: any) => (
              <Input placeholder={label} type="text" {...register('email')} />
            )}
          </FormattedMessage>

          {errors.email && (
            <Error>{getTranslate(errors.email.message as string)}</Error>
          )}
          <LabelLogin>
            <FormattedMessage id="register.userName" />
          </LabelLogin>
          <FormattedMessage id="register.account">
            {(label: any) => (
              <Input
                placeholder={label}
                type="text"
                // autoComplete="off"
                autoComplete="new-password"
                {...register('username')}
              />
            )}
          </FormattedMessage>

          {errors.username && (
            <Error>{getTranslate(errors.username.message as string)}</Error>
          )}
          <LabelLogin>
            <FormattedMessage id="register.password" />
          </LabelLogin>
          <ContentInput>
            <FormattedMessage id="register.enterPwd">
              {(label: any) => (
                <Input
                  placeholder={label}
                  defaultValue={''}
                  type={showPass ? 'text' : 'password'}
                  {...register('password')}
                />
              )}
            </FormattedMessage>

            {showPass ? (
              <SpanEyeRegister onClick={() => setShowPass(!showPass)}>
                <VisibilityOffIcon />
              </SpanEyeRegister>
            ) : (
              <SpanEyeRegister onClick={() => setShowPass(!showPass)}>
                <RemoveRedEyeIcon />
              </SpanEyeRegister>
            )}
          </ContentInput>
          {errors.password && (
            <Error>{getTranslate(errors.password.message as string)}</Error>
          )}
          <LabelLogin>
            <FormattedMessage id="register.comfirm" />
          </LabelLogin>
          <ContentInput>
            <FormattedMessage id="register.reEnterPwd">
              {(label: any) => (
                <Input
                  placeholder={label}
                  type={showPassConfirm ? 'text' : 'password'}
                  {...register('confirmPassword')}
                />
              )}
            </FormattedMessage>

            {errors.confirmPassword && (
              <Error>
                {getTranslate(errors.confirmPassword.message as string)}
              </Error>
            )}
            {showPassConfirm ? (
              <SpanEyeConfirm
                onClick={() => setShowPassConfirm(!showPassConfirm)}
              >
                <VisibilityOffIcon />
              </SpanEyeConfirm>
            ) : (
              <SpanEyeConfirm
                onClick={() => setShowPassConfirm(!showPassConfirm)}
              >
                <RemoveRedEyeIcon />
              </SpanEyeConfirm>
            )}
          </ContentInput>
          <BtnLogin>
            <Question>
              <Option>
                <FormattedMessage id="register.noAccount" />
              </Option>
              <LinkAuth onClick={() => router.push('./login')}>
                <FormattedMessage id="register.redirect" />
              </LinkAuth>
            </Question>
          </BtnLogin>
          <Stack spacing={2} sx={{ width: '100%' }}>
            <Button type="submit">
              <FormattedMessage id="register.register" />
            </Button>

            <Snackbar
              open={open}
              autoHideDuration={4000}
              onClose={handleClose}
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
            >
              {!error ? (
                <Alert
                  onClose={handleClose}
                  severity="success"
                  sx={{ width: '100%' }}
                >
                  <FormattedMessage id="register.success" />
                </Alert>
              ) : (
                <Alert severity="error" color="error">
                  {error}
                </Alert>
              )}
            </Snackbar>
          </Stack>
        </StyleRegister>
      </FormRegister>
    </>
  )
}
export default withRoute(Register, { isProtected: false })
