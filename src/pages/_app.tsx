import { AuthProvider } from '@/src/components/authContext/AuthContext'
import type { AppProps } from 'next/app'
import { ThemeProvider } from 'styled-components'
import { theme } from '../theme'
import '../assets/styles/reset.css'
import 'styles/font.css'
import Layouts from 'layouts'
import '../styles/Home.module.css'
import '../styles/globals.css'
import { useEffect } from 'react'
import { useRouter } from 'next/router'
import { useMemo } from 'react'
import English from '../../public/locales/en/common.json'
import Vietnamese from '../../public/locales/vi/common.json'
import { IntlProvider } from 'react-intl'
import flatten from 'flat'

function MyApp({ Component, pageProps }: AppProps) {
  const router = useRouter()
  const { locale, defaultLocale } = useRouter()
  useEffect(() => {
    if (!router.route.endsWith('login') && !router.route.endsWith('register'))
      localStorage.setItem('old_url', router.asPath)
  }, [router])
  const messages = useMemo(() => {
    switch (locale) {
      case 'en':
        return English
      case 'vi':
        return Vietnamese
      default:
        return English
    }
  }, [locale])
  return (
    <IntlProvider
      messages={flatten(messages)}
      locale={locale ?? 'vi'}
      defaultLocale={defaultLocale}
    >
      <AuthProvider>
        <ThemeProvider theme={theme}>
          <Layouts>
            <Component {...pageProps} />
          </Layouts>
        </ThemeProvider>
      </AuthProvider>
    </IntlProvider>
  )
}

export default MyApp
