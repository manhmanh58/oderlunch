import type { NextPage } from 'next'
import Head from 'next/head'
import CheckToken from '../components/checkToken'
import withRoute from '../hocs/withRoute'

const Home: NextPage = () => {
  return (
    <div>
      <div>
        <Head>
          <title>Order office lunch</title>
        </Head>
      </div>
      <CheckToken />
    </div>
  )
}

export default withRoute(Home, { isProtected: false, isHome: true })
