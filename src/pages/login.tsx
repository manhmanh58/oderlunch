import React, { useState } from 'react'
import FacebookIcon from '@mui/icons-material/Facebook'
import GoogleIcon from '@mui/icons-material/Google'
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye'
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff'
import {
  BtnLogin,
  FormLogin,
  Icons,
  IconsFB,
  IconsGG,
  Input,
  LabelLogin,
  Option,
  P,
  Question,
  SignUp,
  SpanEye,
  Button,
  StyleIcons,
  StyleLogin,
  ContentInput,
  Heading,
  LinkAuth,
  LoginDiv,
  SpanLogin,
} from '@/src/styles/login'
import { useRouter } from 'next/router'
import Head from 'next/head'
import { loginSchema_ } from '@/src/constants'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import authAPI from '../services/AuthAPI'
import {
  FacebookAuthProvider,
  GoogleAuthProvider,
  signInWithPopup,
} from 'firebase/auth'
import { auth } from '../config/firebase'
import { useAuthValue } from '../components/authContext/AuthContext'
import withRoute from '../hocs/withRoute'
import { Alert, Snackbar, Stack } from '@mui/material'
import { Error } from '../styles/register'
import Loading from '../components/Loading'
import { FormattedMessage, useIntl } from 'react-intl'

import { getTranslate } from '@/src/utils/share/translate'

function Login() {
  const [showPass, setShowPass] = useState(false)
  const router = useRouter()
  const { locale } = useRouter()
  const [open, setOpen] = useState(false)
  const [error, setError] = useState('')
  const [errorSocial, setErrorSocial] = useState('')
  const [loading, setLoading] = useState(false)
  const { getProfile } = useAuthValue()
  const { formatMessage } = useIntl()

  const handleClose = (
    event?: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === 'clickaway') {
      return
    }
    setOpen(false)
  }
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(loginSchema_),
  })
  const resetNotification = () => {
    setOpen(false)
    setError('')
    setErrorSocial('')
  }
  const onSubmit = async (dt: any) => {
    resetNotification()
    try {
      setLoading(true)
      const { data } = await authAPI.login(dt)
      localStorage.setItem('token', data.tokenData.access_token)
      const old_url = localStorage.getItem('old_url')
      getProfile()
      setLoading(false)
      if (old_url) {
        setTimeout(() => {
          router.push(old_url, undefined, { scroll: true })
        }, 500)
      } else router.push('/', undefined, { scroll: true })
    } catch (error: any) {
      setError(formatMessage({ id: 'login.error' }))
      setLoading(false)
    }
    setOpen(true)
  }

  const SignInWithFarebaseFacebook = () => {
    var provider_facebook = new FacebookAuthProvider()
    signInWithPopup(auth, provider_facebook)
      .then(async (result) => {
        const userResult = await result.user.getIdTokenResult()
        const tokenResult = {
          idToken: userResult.token,
          providerId: userResult.signInProvider,
        }
        setOpen(!open)
        setLoading(true)
        const signInResult: any = await authAPI.withSocial(tokenResult)
        localStorage.setItem('token', signInResult.data.tokenData.access_token)
        const old_url = localStorage.getItem('old_url')
        if (old_url) {
          router.push(old_url, undefined, { scroll: true })
        } else router.push('/', undefined, { scroll: true })
        setLoading(false)
        getProfile()
      })
      .catch((error: any) => {
        setOpen(!open)
        setErrorSocial(error.response?.data?.message)
        setLoading(false)
        switch (error.code) {
          case 'auth/email-already-in-use':
          case 'auth/invalid-email':
            setErrorSocial('Email already in use or invalid email')
            break
          case 'auth/weak-password':
            setErrorSocial('Weak pass word')
            break
          case 'auth/account-exists-with-different-credential':
            setErrorSocial('Account exist with different credential')
        }
      })
  }

  const SignInWithFarebaseGoogle = () => {
    var provider_google = new GoogleAuthProvider()
    signInWithPopup(auth, provider_google)
      .then(async (result) => {
        const userResult = await result.user.getIdTokenResult()
        const tokenResult = {
          idToken: userResult.token,
          providerId: userResult.signInProvider,
        }
        const old_url = localStorage.getItem('old_url')
        setOpen(!open)
        setLoading(true)
        const signInResult: any = await authAPI.withSocial(tokenResult)
        localStorage.setItem('token', signInResult.data.tokenData.access_token)
        if (old_url) {
          router.push(old_url, undefined, { scroll: true })
        } else router.push('/', undefined, { scroll: true })
        setLoading(false)
        getProfile()
      })
      .catch((error: any) => {
        setOpen(!open)
        setErrorSocial(error.response?.data?.message)
        setLoading(false)
        switch (error.code) {
          case 'auth/email-already-in-use':
          case 'auth/invalid-email':
            setErrorSocial('Email already in use or invalid email')
            break
          case 'auth/weak-password':
            setErrorSocial('Weak password')
            break
        }
      })
  }
  return (
    <FormLogin onSubmit={handleSubmit(onSubmit)}>
      {loading && <Loading />}
      <Head>
        <title>Order office lunch</title>
      </Head>
      <StyleLogin>
        {locale === 'vi' ? (
          <LoginDiv>
            <SpanLogin style={{ '--item': 1 } as any}>Đ</SpanLogin>
            <SpanLogin style={{ '--item': 2 } as any}>Ă</SpanLogin>
            <SpanLogin style={{ '--item': 3 } as any}>N</SpanLogin>
            <SpanLogin style={{ '--item': 4, paddingRight: 5 } as any}>
              G
            </SpanLogin>
            <SpanLogin style={{ '--item': 5, paddingLeft: 5 } as any}>
              N
            </SpanLogin>
            <SpanLogin style={{ '--item': 6 } as any}>H</SpanLogin>
            <SpanLogin style={{ '--item': 7 } as any}>Ậ</SpanLogin>
            <SpanLogin style={{ '--item': 8 } as any}>P</SpanLogin>
          </LoginDiv>
        ) : (
          <LoginDiv>
            <SpanLogin style={{ '--item': 1 } as any}>L</SpanLogin>
            <SpanLogin style={{ '--item': 2 } as any}>O</SpanLogin>
            <SpanLogin style={{ '--item': 4, paddingRight: 5 } as any}>
              G
            </SpanLogin>
            <SpanLogin style={{ '--item': 5, paddingLeft: 5 } as any}>
              I
            </SpanLogin>
            <SpanLogin style={{ '--item': 8 } as any}>N</SpanLogin>
          </LoginDiv>
        )}
        <Heading>
          <LabelLogin>Email</LabelLogin>
          <FormattedMessage id="login.enterEmail">
            {(label: any) => (
              <Input
                {...register('email')}
                placeholder={label}
                type="text"
                autoFocus
              />
            )}
          </FormattedMessage>
          {errors.email && (
            <Error>{getTranslate(errors.email?.message as string)}</Error>
          )}
        </Heading>
        <Heading>
          <LabelLogin>
            {' '}
            <FormattedMessage id="login.password" />
          </LabelLogin>
          <ContentInput>
            <FormattedMessage id="login.enterPwd">
              {(label: any) => (
                <Input
                  {...register('password')}
                  placeholder={label}
                  type={showPass ? 'text' : 'password'}
                />
              )}
            </FormattedMessage>
            {errors.password && (
              <Error>{getTranslate(errors.password.message as string)}</Error>
            )}
            {showPass ? (
              <SpanEye onClick={() => setShowPass(!showPass)}>
                <VisibilityOffIcon />
              </SpanEye>
            ) : (
              <SpanEye onClick={() => setShowPass(!showPass)}>
                <RemoveRedEyeIcon />
              </SpanEye>
            )}
          </ContentInput>
        </Heading>
        <Stack spacing={2} sx={{ width: '100%' }}>
          <Button type="submit">
            <FormattedMessage id="login.title" />
          </Button>
          <Snackbar
            open={open}
            autoHideDuration={5000}
            onClose={handleClose}
            anchorOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }}
          >
            {!error && !errorSocial ? (
              <Alert
                onClose={handleClose}
                severity="success"
                sx={{ width: '100%' }}
              >
                <FormattedMessage id="login.success" />
              </Alert>
            ) : (
              <Alert severity="error">
                {error} {errorSocial}
              </Alert>
            )}
          </Snackbar>
        </Stack>
        <StyleIcons>
          <SignUp>
            <FormattedMessage id="login.register" />
          </SignUp>
          <Icons>
            <IconsFB onClick={SignInWithFarebaseFacebook}>
              <FacebookIcon />
              <P>Facebook</P>
            </IconsFB>
            <IconsGG onClick={SignInWithFarebaseGoogle}>
              <GoogleIcon />
              <P>Google</P>
            </IconsGG>
          </Icons>
        </StyleIcons>
        <BtnLogin>
          <Question>
            <Option>
              <FormattedMessage id="login.noAccount" />
            </Option>
            <LinkAuth onClick={() => router.push('./register')}>
              <FormattedMessage id="login.redirect" />
            </LinkAuth>
          </Question>
        </BtnLogin>
      </StyleLogin>
    </FormLogin>
  )
}

export default withRoute(Login, { isProtected: false })
