import Head from 'next/head'
import { useEffect, useRef, useState } from 'react'
import { Container, Content, Details } from 'styles/profile'
import {
  ContainerProfile,
  FormEdit,
  StyleTitle,
  Title,
  TextInfo,
  EditInput,
  TitleBank,
  ContentLeft,
  ContentRight,
  ButtonSave,
  StyleButton,
  Bank,
  BankNumber,
  BankCode,
  InputQR,
  FieldInput,
  Image,
  FieldInputInfo,
  ButtonImg,
  Error,
  Type,
  EditError,
  ButtonDisable,
} from 'styles/profile/edit'
import { storage } from '@/src/config/firebase'
import { getDownloadURL, ref, uploadBytes } from 'firebase/storage'
import DetailProfile from '../details'
import authAPI from '@/src/services/AuthAPI'
import { useForm } from 'react-hook-form'
import { Alert, Skeleton, Snackbar, Stack } from '@mui/material'
import { yupResolver } from '@hookform/resolvers/yup'
import { useAuthValue } from '@/src/components/authContext/AuthContext'
import { editSchema } from '@/src/constants/editSchema'
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate'
import dynamic from 'next/dynamic'
import { FormattedMessage, useIntl } from 'react-intl'
import { getTranslate } from '@/src/utils/share/translate'
import { getURLImage } from '@/src/utils/share/getURLImage'

interface CurrentUser {
  avatar?: string
  bankNumber?: number
  bankOwner?: string
  email?: string | undefined
  fullName?: string
  phoneNumber?: number
  qrCode?: string
  username?: string
  bankName?: string
  name?: string | any
}

function EditProfile() {
  const [imageQR, setImageQR] = useState(null)
  const [urlAvt, setUrlAvt] = useState()
  const [urlQR, setUrlQR] = useState()
  const [statusQr, setStatusQr] = useState<string>('none')
  const [data, setData] = useState<CurrentUser>()
  const [open, setOpen] = useState<boolean>(false)
  const [error, setError] = useState('')
  const [checkType, setCheckType] = useState('')
  const { getProfile } = useAuthValue()
  const [formDisabled, setFormDisabled] = useState(false)
  const inputRef = useRef<HTMLInputElement>()

  const handleClose = (
    event?: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === 'clickaway') {
      return
    }
    setOpen(false)
  }

  const {
    register,
    handleSubmit,
    formState: { errors },
    setValue,
    watch,
  } = useForm({
    resolver: yupResolver(editSchema),
    mode: 'onChange',
  })
  const { formatMessage } = useIntl()
  useEffect(() => {
    const tempObject = watch()
    delete tempObject['email']
    const areTrue = Object.values(tempObject).every((value) => value === '')
    setFormDisabled(areTrue)
  }, [watch()])

  useEffect(() => {
    const getCharacters = async () => {
      const response = await authAPI.getProfile()
      setData(response.data.currentUser)
      setUrlAvt(response.data.currentUser.avatar)
      setUrlQR(response.data.currentUser.qrCode)
    }
    getCharacters()
      // make sure to catch any error
      .catch(console.error)
  }, [])

  const onInputClick = (
    event: React.MouseEvent<HTMLInputElement, MouseEvent>
  ) => {
    const element = event.target as HTMLInputElement
    element.value = ''
  }

  const handleImageQRChange = async (e: any) => {
    const imageMimeType = /image\/(png|jpg|jpeg)/i
    const file = e.target.files[0]
    const MAX_FILE_SIZE = 10485760
    if (file) {
      const fileSizeKiloBytes = file.size
      if (fileSizeKiloBytes > MAX_FILE_SIZE) {
        setCheckType(formatMessage({ id: 'detail.size' }))
        setTimeout(() => {
          setCheckType('')
        }, 2000)
        return
      }
      if (!file?.type.match(imageMimeType)) {
        setCheckType(formatMessage({ id: 'detail.type' }))
        setTimeout(() => {
          setCheckType('')
        }, 2000)
        return
      }

      let image
      if (file) {
        image = file
        setImageQR(image)
        setStatusQr('loading')
      }

      const urlQr = await getURLImage(image, 'qrCode')
      if (urlQr) {
        setUrlQR(urlQr as any)
        setStatusQr('done')
      } else {
        setError('error')
      }
    }
  }
  const onInputChange = (
    e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
  ) => {
    const newData = { ...data }
    newData[e.target.name as keyof CurrentUser] = e.target.value
    setData(newData)
  }

  const handleSaveChanges = async (dt: CurrentUser) => {
    error && setError('')
    dt.avatar = urlAvt!
    dt.qrCode = urlQR!
    try {
      const result = await authAPI.updateProfile(dt as any)
      getProfile()
    } catch (error: any) {
      setError(error.response?.data?.message)
    }
    setOpen(true)
  }
  return (
    <>
      {data && (
        <Container>
          <Details>
            <DetailProfile />
          </Details>
          <Content>
            <Head>
              <title>Order office lunch</title>
            </Head>
            <ContainerProfile>
              <ContentLeft>
                <StyleTitle>
                  <Title>
                    <FormattedMessage id="edit.information" />
                  </Title>
                  <TextInfo>
                    <FormattedMessage id="edit.personalInfo" />
                  </TextInfo>
                </StyleTitle>
                <FormEdit onSubmit={handleSubmit(handleSaveChanges)}>
                  <TitleBank style={{ color: 'orange' }}>
                    {' '}
                    <FormattedMessage id="edit.edit" />
                  </TitleBank>
                  <EditInput>
                    <FormattedMessage id="edit.userName">
                      {(label) => (
                        <FieldInputInfo
                          autoFocus
                          id="outlined-required"
                          label={label}
                          defaultValue={data?.username || ''}
                          {...register('username')}
                        />
                      )}
                    </FormattedMessage>
                    {errors.username && (
                      <EditError>
                        {getTranslate(errors.username.message as string)}
                      </EditError>
                    )}
                  </EditInput>
                  <EditInput>
                    <FormattedMessage id="edit.fullName">
                      {(label) => (
                        <FieldInputInfo
                          id="outlined-required"
                          label={label}
                          defaultValue={data?.fullName || ''}
                          {...register('fullName')}
                        />
                      )}
                    </FormattedMessage>

                    {errors.fullName && (
                      <EditError>
                        {getTranslate(errors.fullName.message as string)}
                      </EditError>
                    )}
                  </EditInput>
                  <EditInput>
                    <FieldInputInfo
                      id="outlined-required"
                      label="Email"
                      defaultValue={data?.email}
                      inputProps={{ readOnly: true }}
                      {...register('email')}
                    />
                    {errors.email && (
                      <EditError>
                        {getTranslate(errors.email.message as string)}
                      </EditError>
                    )}
                  </EditInput>
                  <EditInput style={{ marginBottom: 5 }}>
                    <FormattedMessage id="edit.phone">
                      {(label) => (
                        <FieldInputInfo
                          id="outlined-required"
                          label={label}
                          defaultValue={data?.phoneNumber || ''}
                          {...register('phoneNumber')}
                        />
                      )}
                    </FormattedMessage>

                    {errors.phoneNumber && (
                      <EditError>
                        {getTranslate(errors.phoneNumber.message as string)}
                      </EditError>
                    )}
                  </EditInput>
                  <Bank>
                    <BankNumber>
                      <TitleBank style={{ color: 'orange' }}>
                        <FormattedMessage id="edit.bank" />
                      </TitleBank>
                      <EditInput>
                        <FormattedMessage id="edit.number">
                          {(label) => (
                            <FieldInput
                              id="outlined-required"
                              label={label}
                              defaultValue={data?.bankNumber || ''}
                              {...register('bankNumber')}
                            />
                          )}
                        </FormattedMessage>

                        {errors.bankNumber && (
                          <EditError>
                            {getTranslate(errors.bankNumber.message as string)}
                          </EditError>
                        )}
                      </EditInput>
                      <EditInput>
                        <FormattedMessage id="edit.bankName">
                          {(label) => (
                            <FieldInput
                              id="outlined-required"
                              label={label}
                              defaultValue={data?.bankName || ''}
                              {...register('bankName')}
                            />
                          )}
                        </FormattedMessage>
                        {errors.bankName && (
                          <EditError>
                            {getTranslate(errors.bankName.message as string)}
                          </EditError>
                        )}
                      </EditInput>
                      <EditInput>
                        <FormattedMessage id="edit.owner">
                          {(label) => (
                            <FieldInput
                              id="outlined-required"
                              label={label}
                              defaultValue={data?.bankOwner || ''}
                              {...register('bankOwner')}
                            />
                          )}
                        </FormattedMessage>
                        {errors.bankOwner && (
                          <EditError>
                            {getTranslate(errors.bankOwner.message as string)}
                          </EditError>
                        )}
                      </EditInput>
                    </BankNumber>
                  </Bank>
                  <Stack spacing={2} sx={{ width: '100%' }}>
                    <StyleButton>
                      {formDisabled ? (
                        <ButtonDisable type="submit" disabled>
                          {' '}
                          <FormattedMessage id="edit.save" />
                        </ButtonDisable>
                      ) : (
                        <ButtonSave type="submit">
                          {' '}
                          <FormattedMessage id="edit.save" />
                        </ButtonSave>
                      )}
                    </StyleButton>
                    <Snackbar
                      open={open}
                      autoHideDuration={2000}
                      onClose={handleClose}
                      anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                      }}
                    >
                      {!error ? (
                        <Alert
                          onClose={handleClose}
                          severity="success"
                          sx={{ width: '100%' }}
                        >
                          <FormattedMessage id="edit.comfirm" />
                        </Alert>
                      ) : (
                        <Alert severity="error">
                          <FormattedMessage id="edit.error" />
                        </Alert>
                      )}
                    </Snackbar>
                  </Stack>
                </FormEdit>
              </ContentLeft>
              <ContentRight>
                <BankCode>
                  <TitleBank style={{ color: 'blue', paddingBottom: 15 }}>
                    QR Code
                  </TitleBank>
                  <ButtonImg onClick={() => inputRef.current?.click()}>
                    {statusQr === 'loading' && (
                      <Skeleton
                        variant="rectangular"
                        width="280px"
                        height="250px"
                      />
                    )}
                    {urlQR ? (
                      <>
                        {statusQr !== 'loading' && (
                          <Image src={urlQR} alt="image code QR" />
                        )}
                      </>
                    ) : (
                      <>
                        {statusQr !== 'loading' && (
                          <AddPhotoAlternateIcon
                            sx={{ width: '280px', height: '250px' }}
                          />
                        )}
                      </>
                    )}
                  </ButtonImg>
                  <InputQR
                    type="file"
                    onChange={handleImageQRChange}
                    onClick={onInputClick}
                    style={{ display: 'none' }}
                    ref={inputRef as any}
                  />
                  {checkType && (
                    <Type style={{ marginTop: '0px' }}>{checkType}</Type>
                  )}
                </BankCode>
              </ContentRight>
            </ContainerProfile>
          </Content>
        </Container>
      )}
    </>
  )
}
export default dynamic(() => Promise.resolve(EditProfile), {
  ssr: false,
})
