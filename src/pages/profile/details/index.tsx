import { useRouter } from 'next/router'
import { Info } from 'styles/profile/content'
import {
  Account,
  ContentBottom,
  Edit,
  EditProfile,
  Name,
  StyleImage,
  Text,
  TextAcc,
  ButtonImg,
} from 'styles/profile/details'
import EditIcon from '@mui/icons-material/Edit'
import PersonIcon from '@mui/icons-material/Person'
import AssessmentIcon from '@mui/icons-material/Assessment'
import Avatar from '@mui/material/Avatar'
import { useAuthValue } from '@/src/components/authContext/AuthContext'
import { getDownloadURL, ref, uploadBytes } from 'firebase/storage'
import { storage } from '@/src/config/firebase'
import { useEffect, useRef, useState } from 'react'
import { InputQR } from '@/src/styles/profile/edit'
import authAPI from '@/src/services/AuthAPI'
import ShowChartIcon from '@mui/icons-material/ShowChart'
import BarChartIcon from '@mui/icons-material/BarChart'
import { Alert, Skeleton, Snackbar } from '@mui/material'
import { FormattedMessage, useIntl } from 'react-intl'
import dynamic from 'next/dynamic'
import { getURLImage } from '@/src/utils/share/getURLImage'
import TurnedInIcon from '@mui/icons-material/TurnedIn'

function DetailProfile() {
  const router = useRouter()
  const { profile, getProfile } = useAuthValue()
  const [error, setError] = useState('')
  const [image, setImage] = useState(null)
  const [statusImg, setStatusImg] = useState<string>('none')
  const [url, setUrl] = useState('')
  const [data, setData] = useState([] as any)
  const inputRef = useRef<HTMLInputElement>()
  const [open, setOpen] = useState<boolean>(false)

  const handleClose = (
    event?: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === 'clickaway') {
      return
    }
    setOpen(false)
  }
  const { formatMessage } = useIntl()
  useEffect(() => {
    const getCharacters = async () => {
      const response = await authAPI.getProfile()
      setData(response.data.currentUser)
      setUrl(response.data.currentUser.avatar)
    }
    getCharacters()
      // make sure to catch any error
      .catch(console.error)
  }, [])

  const handleImageChange = async (e: any) => {
    const file = e.target.files[0]
    const MAX_FILE_SIZE = 10485760
    const imageMimeType = /image\/(png|jpg|jpeg|gif)/i
    let image
    if (file) {
      const fileSizeKiloBytes = file?.size
      if (fileSizeKiloBytes > MAX_FILE_SIZE) {
        setError(formatMessage({ id: 'detail.size' }))
        setOpen(true)
        return
      }
      if (!file?.type.match(imageMimeType)) {
        setError(formatMessage({ id: 'detail.type' }))
        setOpen(true)
        return
      }
      setError('')
      image = e.target.files[0]
      setImage(image)
      setStatusImg('loading')
    }

    const urlAvatar = await getURLImage(image, 'avatar')
    if (urlAvatar) {
      setUrl(urlAvatar as any)
      await authAPI.updateProfile({
        email: profile.email,
        fullName: profile.fullName,
        avatar: urlAvatar,
        qrCode: profile.qrCode,
        bankName: profile.bankName,
        bankNumber: profile.bankNumber,
        bankOwner: profile.bankOwner,
        phoneNumber: profile.phoneNumber,
        username: profile.username,
      })
      getProfile()
      setStatusImg('')
      setOpen(true)
    } else {
      setError('error')
    }
  }

  return (
    <Info>
      <StyleImage>
        <ButtonImg
          onClick={() => {
            inputRef.current?.click()
          }}
        >
          {statusImg === 'loading' && (
            <Skeleton variant="circular" width="70px" height="70px" />
          )}
          {statusImg !== 'loading' && (
            <Avatar src={url} sx={{ width: 70, height: 70 }} />
          )}
        </ButtonImg>
        <InputQR
          type="file"
          onChange={handleImageChange}
          style={{ display: 'none' }}
          ref={inputRef as any}
        />
        <Snackbar
          open={open}
          autoHideDuration={2000}
          onClose={handleClose}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
        >
          {!error ? (
            <Alert severity="success" sx={{ width: '100%' }}>
              <FormattedMessage id="detail.alert" />
            </Alert>
          ) : (
            <Alert severity="error">{error}</Alert>
          )}
        </Snackbar>

        <EditProfile>
          <Name>{profile?.fullName}</Name>
        </EditProfile>
      </StyleImage>
      <ContentBottom>
        <Account onClick={() => router.push('/profile/edit')}>
          <PersonIcon style={{ color: 'blue' }} />
          <TextAcc>
            <FormattedMessage id="detail.account" />
          </TextAcc>
        </Account>
        <Account onClick={() => router.push('/profile/report')}>
          <ShowChartIcon style={{ color: 'red' }} />
          <TextAcc>
            <FormattedMessage id="detail.cart" />
          </TextAcc>
        </Account>
        <Account onClick={() => router.push('/profile/posts-saved')}>
          <TurnedInIcon />
          <TextAcc>
            <FormattedMessage id="detail.savedPost" />
          </TextAcc>
        </Account>
      </ContentBottom>
    </Info>
  )
}
export default dynamic(() => Promise.resolve(DetailProfile), {
  ssr: false,
})
