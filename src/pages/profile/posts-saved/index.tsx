import Head from 'next/head'
import React, { useEffect, useState } from 'react'
import { Container, Content, Details } from 'styles/profile'
import DetailProfile from '../details'
import { StyleTitle, TextInfo, Title } from 'styles/profile/edit'
import {
  Paper,
  Snackbar,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from '@material-ui/core'
import { StyleAddImage } from '@/src/styles/profile/report/booked'
import reportAPI from '@/src/services/reportsAPI'
import { FormattedMessage } from 'react-intl'
import { Stack, Alert } from '@mui/material'
import BorderColorIcon from '@mui/icons-material/BorderColor'
import postsApi from '@/src/services/postsApi'
import ImageNotSupportedIcon from '@mui/icons-material/ImageNotSupported'
import TurnedInNotIcon from '@mui/icons-material/TurnedInNot'
import Popup from '@/src/components/Popup'
import Button from '@/src/components/Button'
import getDateTimeString from '@/src/utils/share/getDateTimeString'
import { Dishes } from '@/src/utils/type/dishes'
import ClonePopUp from '@/src/components/pages/UserProfile/PostSaved/ClonePopUp'

type SavePostedType = {
  id: string
  image: {
    url: string
  }
  saved: boolean
  title: string
  userId: string
  createdAt: string
  endTime: string
  dishes: Array<Dishes>
}

export default function PostsSaved() {
  const [data, setData] = useState<Array<SavePostedType>>([])
  const [open, setOpen] = useState<null | number>(null)
  const [modalOpen, setModalOpen] = useState<
    boolean | { id: string; index: number }
  >(false)
  const [alertEdit, setAlertEdit] = useState(false)
  const [alertUnsave, setAlertUnsave] = useState(false)
  const [error, setError] = useState(false)
  const turnOffAlertMutilTime = () => {
    if (alertEdit) {
      setAlertEdit(false)
    }
    if (error) {
      setError(false)
    }
    if (alertUnsave) {
      setAlertUnsave(false)
    }
  }
  const fetchData = async () => {
    try {
      const { data } = await reportAPI.getPostsSaved()
      setData(data.posts)
    } catch (error) {}
  }

  const handleOnUnsave = async () => {
    turnOffAlertMutilTime()
    try {
      if (typeof modalOpen === 'object') {
        const { data } = await postsApi.updatePost(modalOpen.id, {
          saved: false,
        })
        setData((prev) => {
          const tempArray = [...prev]
          tempArray.splice(modalOpen.index, 1)
          return tempArray
        })
        setAlertUnsave(true)
        setModalOpen(false)
      }
    } catch (error) {
      setError(true)
    }
  }

  const hanldeOnUpdate = async (body: {
    title?: string
    endTime?: string
    url?: string
    id: string
  }) => {
    turnOffAlertMutilTime()
    try {
      const payload = {
        ...(body.title && { title: body.title }),
        ...(body.endTime && { endTime: body.endTime }),
        ...(body.url && { thumbnail: { url: body.url } }),
      }
      const { data } = await postsApi.updatePost(body.id, payload)
      setData((prev) => {
        if (typeof open === 'number') {
          const tempArr = [...prev]
          tempArr[open] = {
            ...tempArr[open],
            ...(body.title && { title: body.title }),
            ...(body.endTime && { endTime: body.endTime }),
            ...(body.url && { image: { url: body.url } }),
          }
          return tempArr
        }
        return prev
      })
    } catch (error) {
      setError(true)
    }
    setAlertEdit(true)
  }

  const handleOnClone = async (body: any) => {
    turnOffAlertMutilTime()
    try {
      const { data } = await postsApi.create(body)
      setAlertEdit(true)
      setOpen(null)
      fetchData()
    } catch (error) {
      setError(true)
    }
  }

  useEffect(() => {
    fetchData()
  }, [])

  return (
    <Container>
      <Details>
        <DetailProfile />
      </Details>
      <Content>
        <Head>
          <title>Order office lunch</title>
        </Head>
        <Stack sx={{ padding: '0px 20px' }}>
          <StyleTitle>
            <Title>
              <FormattedMessage id="saved.title" />
            </Title>
            <TextInfo>
              <FormattedMessage id="saved.subtitle" />
            </TextInfo>
          </StyleTitle>
          <TableContainer component={Paper}>
            <Table
              style={{ minWidth: 650 }}
              size="small"
              aria-label="a dense table"
            >
              <TableHead>
                <TableRow style={{ background: '#eff7f6', fontWeight: 'bold' }}>
                  <TableCell>
                    <FormattedMessage id="saved.table.stt" />
                  </TableCell>
                  <TableCell align="right">
                    <FormattedMessage id="saved.table.image" />
                  </TableCell>
                  <TableCell align="center">
                    <FormattedMessage id="saved.table.title" />
                  </TableCell>
                  <TableCell align="center">
                    <FormattedMessage id="saved.table.created" />
                  </TableCell>
                  <TableCell align="center">
                    <FormattedMessage id="saved.table.ended" />
                  </TableCell>
                  <TableCell align="center">
                    <FormattedMessage id="saved.table.saved" />
                  </TableCell>
                  <TableCell align="center">
                    <FormattedMessage id="saved.table.edit" />
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data.length
                  ? data?.map((item: SavePostedType, index: number) => (
                      <React.Fragment key={index}>
                        <TableRow>
                          <TableCell component="th" scope="row">
                            {index + 1}
                          </TableCell>
                          <TableCell>
                            <StyleAddImage style={{ marginLeft: 'auto' }}>
                              {item?.image?.url ? (
                                <img src={item.image.url} alt="images food" />
                              ) : (
                                <ImageNotSupportedIcon />
                              )}
                            </StyleAddImage>
                          </TableCell>
                          <TableCell align="center">{item.title}</TableCell>
                          <TableCell align="center">
                            {getDateTimeString(item.createdAt)}
                          </TableCell>
                          <TableCell align="center">
                            {item.endTime.split('T')[1].split('.')[0] +
                              ' ' +
                              item.endTime
                                .split('T')[0]
                                .split('-')
                                .reverse()
                                .join('/')}
                          </TableCell>
                          <TableCell align="center">
                            <TurnedInNotIcon
                              style={{ cursor: 'pointer' }}
                              onClick={() =>
                                setModalOpen({
                                  id: item.id,
                                  index,
                                })
                              }
                            />
                          </TableCell>
                          <TableCell align="center">
                            <BorderColorIcon
                              style={{ cursor: 'pointer' }}
                              onClick={() => setOpen(index)}
                            />
                            {open === index && (
                              <ClonePopUp
                                handleOnClose={setOpen}
                                data={item}
                                index={index}
                                width={'60%'}
                                handleOnClone={handleOnClone}
                              />
                            )}
                          </TableCell>
                        </TableRow>
                      </React.Fragment>
                    ))
                  : ''}
              </TableBody>
            </Table>
          </TableContainer>
        </Stack>
        <Snackbar
          open={alertEdit}
          autoHideDuration={3000}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          style={{ marginTop: 10 }}
          onClose={() => setAlertEdit(false)}
        >
          <Alert
            onClose={() => setAlertEdit(false)}
            severity={error ? 'error' : 'success'}
            style={{ width: '100%' }}
          >
            {error ? (
              <FormattedMessage id={'saved.table.editError'} />
            ) : (
              <FormattedMessage id={'saved.table.editSucces'} />
            )}
          </Alert>
        </Snackbar>
        <Snackbar
          open={alertUnsave}
          autoHideDuration={3000}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          style={{ marginTop: 10 }}
          onClose={() => setAlertUnsave(false)}
        >
          <Alert
            onClose={() => setAlertUnsave(false)}
            severity={error ? 'error' : 'success'}
            style={{ width: '100%' }}
          >
            {error ? (
              <FormattedMessage id={'saved.table.unsaveError'} />
            ) : (
              <FormattedMessage id={'saved.table.unsaveSuccess'} />
            )}
          </Alert>
        </Snackbar>
        {modalOpen && (
          <Popup setModalOpen={setModalOpen}>
            <Stack sx={{ padding: '10px' }}>
              <Typography variant="h6">
                <FormattedMessage id="saved.table.confirmUnSave" />
              </Typography>
              <Stack direction="row" spacing={2} mt={2}>
                <Button
                  variant="contained"
                  sx={{ marginLeft: 'auto' }}
                  onClick={() => handleOnUnsave()}
                >
                  <FormattedMessage id="saved.table.btnConfirmUnsave" />
                </Button>
                <Button
                  variant="contained"
                  onClick={() => setModalOpen(false)}
                  color="error"
                >
                  <FormattedMessage id="saved.table.btnCancel" />
                </Button>
              </Stack>
            </Stack>
          </Popup>
        )}
      </Content>
    </Container>
  )
}
