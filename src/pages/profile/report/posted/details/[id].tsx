import Tab from '@/src/components/pages/LandingPage/FoodList/Tab/PaidTab'
import PosterPaidTab from '@/src/components/pages/LandingPage/FoodList/Tab/PosterPaidTab'
import { useRouter } from 'next/router'
import styled from 'styled-components'
import ArrowBackIcon from '@mui/icons-material/ArrowBack'
import { Title } from '@/src/styles/profile/report/booked'
import Head from 'next/head'
import { Container, Content, Details } from '@/src/styles/profile'
import DetailProfile from '../../../details'
import { useEffect, useState } from 'react'
import Loading from '@/src/components/Loading'
import { StyleFoodList } from '@/src/components/pages/LandingPage/FoodList/style'
import FoodListItem from '@/src/components/pages/LandingPage/FoodList/FoodListItem'
import postsApi from '@/src/services/postsApi'
import {
  StyleTabPanel,
  StyleTabPanelItem,
} from '@/src/components/pages/LandingPage/FoodList/Tab/style'
import { FormattedMessage } from 'react-intl'
import dynamic from 'next/dynamic'

const StyleContainer = styled.div`
  margin: 30px;
`
const FoodListStyle = styled(StyleFoodList)`
  display: flex;
  justify-content: center;
  justify-content: center;
  flex-wrap: wrap;
  /* background-color: #f2b5d4; */
`
const StyleBack = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 10px;
`

const StyleButton = styled.button`
  background-color: orange;
  border: none;
  display: flex;
  align-items: center;
  border-radius: 5px;
  cursor: pointer;
  &:hover {
    background-color: #d0c8b6;
  }
`
const StyleContent = styled.div`
  background-color: #eff7f6;
`

const DetailsPosted = () => {
  const router = useRouter()
  const [loading, setLoading] = useState(false)
  const [datas, setDatas] = useState([])
  const [menu, setMenu] = useState(1)
  const [payDatas, setPayDatas] = useState([])
  const fetchData = async () => {
    const { data } = await postsApi.getDetail(router.query.id as string)
    setDatas(data.postDetails?.dishes)
  }

  const fetchDataPayData = async () => {
    try {
      const { data } = await postsApi.getOrdersOfUserByPost(
        router.query.id as string
      )
      if (data) {
        setPayDatas(data.orders)
      }
    } catch (error) {}
  }

  useEffect(() => {
    setLoading(true)
    const timer = setTimeout(() => {
      setLoading(false)
      window.scrollTo(0, 0)
    }, 500)
    return () => clearTimeout(timer)
  }, [])

  useEffect(() => {
    fetchData()
    fetchDataPayData()
  }, [router])

  return (
    <Container>
      <Details>
        <DetailProfile />
      </Details>
      {loading && <Loading />}
      {datas && (
        <Content>
          <Head>
            <title>Order office lunch</title>
          </Head>
          <StyleBack>
            <StyleButton onClick={() => router.back()}>
              <ArrowBackIcon sx={{ color: 'white', width: 18 }} />
            </StyleButton>
            <Title>
              <FormattedMessage id="detail.detail" />
            </Title>
          </StyleBack>
          <StyleTabPanel style={{ margin: 10 }}>
            <StyleTabPanelItem active={menu === 1} onClick={() => setMenu(1)}>
              <FormattedMessage id="detail.list" /> ({datas?.length})
            </StyleTabPanelItem>
            <StyleTabPanelItem active={menu === 2} onClick={() => setMenu(2)}>
              <FormattedMessage id="detail.status" /> ({payDatas?.length || 0})
            </StyleTabPanelItem>
          </StyleTabPanel>
          <StyleContainer>
            {datas?.length > 0 && menu === 1 && (
              <FoodListStyle>
                {datas.map((dish, index) => (
                  <FoodListItem
                    isPoster={true}
                    tab={router.query.id as string}
                    setOrdereds={() => {}}
                    key={index + +(router.query.id as string)}
                    dish={dish}
                    expired={false}
                  />
                ))}
              </FoodListStyle>
            )}
            <StyleContent>
              {menu === 2 && (
                <Tab
                  postId={router.query.id as string}
                  fetchDataPayData={fetchDataPayData}
                  payDatas={payDatas}
                  setPayDatas={setPayDatas}
                  isPoster={true}
                />
              )}
            </StyleContent>
          </StyleContainer>
        </Content>
      )}
    </Container>
  )
}

export default dynamic(() => Promise.resolve(DetailsPosted), {
  ssr: false,
})
