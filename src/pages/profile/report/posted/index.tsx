import React, { useState, useEffect } from 'react'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import Paper from '@mui/material/Paper'
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate'
import styled from 'styled-components'
import reportAPI from '@/src/services/reportsAPI'
import { useRouter } from 'next/router'
import Loading from '@/src/components/Loading'
import { FormattedMessage } from 'react-intl'
import getDateTimeString from '@/src/utils/share/getDateTimeString'
const StyleAddImage = styled.label`
  margin: auto;
  border-radius: 8px;
  display: flex;
  align-items: flex-end;
  justify-content: center;
  background: #cecece;
  height: 75px;
  width: 138px;
  cursor: pointer;
  img {
    width: 138px;
    height: 75px;
    border-radius: 8px;
    object-fit: cover;
  }
  input {
    display: none;
  }
`

const StyleTableCell = styled(TableCell)`
  display: flex;
  justify-content: center;
`

interface Posted {
  id: number
  title: string
  createAt: Date
  endTime: Date
  image: {
    url?: string
  }
}
export default function PostedTable() {
  const [data, setData] = useState<Posted[]>()
  const [loading, setLoading] = useState(false)
  const router = useRouter()

  useEffect(() => {
    setLoading(true)
    const getCharacters = async () => {
      const response = await reportAPI.getPosted()
      setLoading(false)
      setData(response.data.posts)
    }
    getCharacters()
      // make sure to catch any error
      .catch(console.error)
  }, [])

  return (
    <TableContainer component={Paper} sx={{ marginTop: 1.5 }}>
      {loading && <Loading />}
      <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
        <TableHead>
          <TableRow sx={{ background: '#eff7f6' }}>
            <TableCell sx={{ fontWeight: 'bold' }}>STT</TableCell>
            <TableCell align="center" sx={{ fontWeight: 'bold' }}>
              <FormattedMessage id="posted.image" />
            </TableCell>
            <TableCell align="center" sx={{ fontWeight: 'bold' }}>
              <FormattedMessage id="posted.title" />
            </TableCell>
            <TableCell align="center" sx={{ fontWeight: 'bold' }}>
              <FormattedMessage id="posted.time" />
            </TableCell>
            <TableCell align="center" sx={{ fontWeight: 'bold' }}>
              <FormattedMessage id="posted.endTime" />
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data &&
            data?.map((item: any, index: number) => (
              <TableRow
                key={index}
                onClick={() =>
                  router.push(`/profile/report/posted/details/${item.id}`)
                }
                sx={{ cursor: 'pointer' }}
              >
                <TableCell component="th" scope="row">
                  {index + 1}
                </TableCell>
                <StyleTableCell>
                  <StyleAddImage>
                    {item?.image?.url ? (
                      <img src={item.image.url} alt="images food" />
                    ) : (
                      <AddPhotoAlternateIcon />
                    )}
                  </StyleAddImage>
                </StyleTableCell>
                <TableCell align="center" sx={{ fontWeight: 600 }}>
                  {item.title}
                </TableCell>
                <TableCell align="center">
                  {getDateTimeString(item.createdAt)}
                </TableCell>
                <TableCell align="center">
                  {item.endTime.split('T')[1].split('.')[0] +
                    ' ' +
                    item.endTime.split('T')[0].split('-').reverse().join('/')}
                </TableCell>
              </TableRow>
            ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
}
