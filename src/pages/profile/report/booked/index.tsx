import React, { useState, useEffect } from 'react'
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Pagination,
} from '@mui/material'
import reportAPI from '@/src/services/reportsAPI'
import DetailsBooked from './detailsBooked'
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate'
import {
  Order,
  StyleAddImage,
  StyleTableCell,
  StyleTableCellName,
  Title,
} from '@/src/styles/profile/report/booked'
import { getCurrency } from '@/src/utils/share/getCurrency'
import styled from 'styled-components'
import TablePagination from '@material-ui/core/TablePagination'
import { FormattedMessage } from 'react-intl'
import getDateTimeString from '@/src/utils/share/getDateTimeString'
import { Box } from '@mui/system'
import PaidStatus from './detailsBooked/PaidStatus'
interface Booked {
  id: number
  createdAt: Date
  endTime: Date
  post: {
    title: string
    image: {
      url: string
    }
  }
  status: string
  note: string
}

export default function BookedTable() {
  const [data, setData] = useState<Booked[]>()
  const [isExpanded, setIsExpanded] = useState(false)
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(5)

  const handleChangePage = (
    event: React.MouseEvent<HTMLButtonElement> | null,
    newPage: number
  ) => {
    setPage(newPage)
  }

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setRowsPerPage(+event.target.value)
    setPage(0)
  }

  useEffect(() => {
    const getCharacters = async () => {
      const response = await reportAPI.getBooked()
      setData(response.data.orders)
    }
    getCharacters()
      // make sure to catch any error
      .catch(console.error)
  }, [])

  const statuses: any = {
    PAID: { text: <FormattedMessage id="detailsBooked.paid" />, color: 'blue' },
    UNPAID: {
      text: <FormattedMessage id="detailsBooked.unPaid" />,
      color: 'red',
    },
  }

  const result = data?.map((orderSum: any, index: number) => {
    let sum = orderSum?.orderQuantities.reduce(
      (sum: any, item: any) => sum + item.dish.price * item.quantity,
      0
    )
    return {
      sum: sum,
      status: orderSum.status,
    }
  })

  const totalOrder = result?.reduce((total, item) => total + item.sum, 0)
  const amountPaid = result?.reduce((total, item) => {
    if (item.status === 'PAID') {
      return (total += item.sum)
    }
    return total
  }, 0)
  const unpaidAmount = () => {
    if (totalOrder) {
      let amountPaidCurrent = amountPaid ? amountPaid : 0
      return totalOrder - amountPaidCurrent
    }
  }

  return (
    <TableContainer
      sx={{ marginTop: 1.5, marginBottom: 1.5 }}
      component={Paper}
    >
      <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
        <TableHead>
          <TableRow sx={{ background: '#eff7f6', fontWeight: 'bold' }}>
            <TableCell></TableCell>
            <TableCell sx={{ fontWeight: 'bold' }}>ID</TableCell>
            <StyleTableCell sx={{ fontWeight: 'bold' }}>
              <FormattedMessage id="detailsBooked.title" />
            </StyleTableCell>
            <StyleTableCell sx={{ fontWeight: 'bold' }}>
              <FormattedMessage id="detailsBooked.name" />
            </StyleTableCell>
            <StyleTableCell align="center" sx={{ fontWeight: 'bold' }}>
              <FormattedMessage id="detailsBooked.date" />
            </StyleTableCell>
            <StyleTableCell align="center" sx={{ fontWeight: 'bold' }}>
              <FormattedMessage id="detailsBooked.status" />
            </StyleTableCell>
            <StyleTableCell align="center" sx={{ fontWeight: 'bold' }}>
              <FormattedMessage id="detailsBooked.note" />
            </StyleTableCell>
            <StyleTableCell align="center" sx={{ fontWeight: 'bold' }}>
              <FormattedMessage id="detailsBooked.total" />
            </StyleTableCell>
          </TableRow>
        </TableHead>
        <TableBody
          sx={{ cursor: 'pointer' }}
          onClick={() => setIsExpanded(!isExpanded)}
        >
          {data &&
            data
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((item: any, index: number) => (
                <DetailsBooked key={index} item={item}>
                  <TableCell>{index + 1}</TableCell>
                  <StyleTableCell align="center">
                    <Order>
                      <StyleAddImage>
                        {item.post.image.url ? (
                          <img src={item.post.image.url} alt="images food" />
                        ) : (
                          <AddPhotoAlternateIcon />
                        )}
                      </StyleAddImage>
                      <Title>{item.post.title}</Title>
                    </Order>
                  </StyleTableCell>
                  <StyleTableCellName>
                    <div>{item.post.user.fullName}</div>
                  </StyleTableCellName>
                  <StyleTableCell align="center">
                    {getDateTimeString(item.createdAt)}
                  </StyleTableCell>

                  <StyleTableCell
                    style={{
                      color: statuses[item?.status as keyof Booked]?.color,
                    }}
                    align="center"
                  >
                    {statuses[item?.status]?.text as keyof Booked}
                  </StyleTableCell>
                  <StyleTableCell align="center">{item.note}</StyleTableCell>
                  <StyleTableCell align="center">
                    {getCurrency(
                      item?.orderQuantities.reduce(
                        (sum: any, item: any) =>
                          sum + item.dish.price * item.quantity,
                        0
                      )
                    )}
                  </StyleTableCell>
                </DetailsBooked>
              ))}

          <PaidStatus
            title={<FormattedMessage id="detailsBooked.unPaid" />}
            prices={unpaidAmount() || 0}
            color={'red'}
          />
          <PaidStatus
            title={<FormattedMessage id="detailsBooked.paid" />}
            prices={amountPaid || 0}
            color={'blue'}
          />
          <PaidStatus
            title={<FormattedMessage id="detailsBooked.total" />}
            prices={totalOrder || 0}
            color={'black'}
          />
        </TableBody>
      </Table>
      <TablePagination
        rowsPerPageOptions={[5]}
        component="div"
        count={data?.length ?? 0}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </TableContainer>
  )
}
