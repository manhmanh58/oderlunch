import React, { useState, useEffect } from 'react'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp'
import { TableRow, TableCell } from '@mui/material'
import { getCurrency } from '@/src/utils/share/getCurrency'
import styled from 'styled-components'
import { Icons } from '@/src/styles/profile/report/booked'
import { FormattedMessage } from 'react-intl'

const StyleItems = styled.div`
  display: flex;
  flex-direction: row;
  border-bottom: 1px solid #d0c8b6;
  border-top: 1px solid #d0c8b6;
  padding: 10px 120px;
  width: 100%;
`
const Item = styled.div`
  display: flex;
`

const Title = styled.div`
  font-weight: 600;
  margin-left: 100px;
  margin-bottom: 5px;
`
const Heading = styled.div`
  font-weight: 600;
  padding-left: 5px;
`

const DetailsBooked = ({
  item,
  children,
  ...otherProps
}: {
  item: any
  children: any
}) => {
  const [isExpanded, setIsExpanded] = useState(false)

  return (
    <>
      <TableRow
        {...otherProps}
        sx={{ cursor: 'pointer' }}
        onClick={() => setIsExpanded(!isExpanded)}
      >
        <TableCell component="th" scope="row">
          <Icons>
            {isExpanded ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </Icons>
        </TableCell>
        {children}
      </TableRow>
      {isExpanded && (
        <TableCell colSpan={8} sx={{ background: '#eff7f6' }}>
          <Title>
            <FormattedMessage id="detailsBooked.detail" />
          </Title>
          {item.orderQuantities &&
            item.orderQuantities.map((booked: any, index: number) => (
              <StyleItems key={index}>
                <Item style={{ width: '2%' }}>{index + 1}</Item>
                <Item style={{ width: '40%' }}>
                  <FormattedMessage id="detailsBooked.food" />{' '}
                  <Heading> {booked.dish.name} </Heading>
                </Item>
                <Item style={{ width: '27.75%' }}>
                  <FormattedMessage id="detailsBooked.price" />{' '}
                  <Heading>{getCurrency(booked.dish.price)}</Heading>
                </Item>
                <Item style={{ width: '27.75%' }}>
                  <FormattedMessage id="detailsBooked.quantity" />{' '}
                  <Heading>{booked.quantity}</Heading>
                </Item>
                <Item style={{ width: '3.5%' }}>
                  <FormattedMessage id="detailsBooked.total" />
                  <Heading>
                    {getCurrency(booked.dish.price * booked.quantity)}
                  </Heading>
                </Item>
              </StyleItems>
            ))}
        </TableCell>
      )}
    </>
  )
}

export default DetailsBooked
