import { getCurrency } from '@/src/utils/share/getCurrency'
import { TableCell, TableRow } from '@mui/material'
import { Box } from '@mui/system'
import React from 'react'

interface Iprops {
  title: any
  prices: number
  color: string
}

function PaidStatus(props: Iprops) {
  return (
    <TableRow>
      <TableCell
        colSpan={8}
        sx={{
          textAlign: 'end',
          fontSize: 16,
          fontWeight: 700,
          paddingRight: 2,
        }}
      >
        <Box
          sx={{
            width: '28%',
            justifyContent: 'start',
            display: 'inline-flex',
          }}
        >
          <Box
            sx={{
              width: '50%',
              textAlign: 'start',
              color: props.color,
            }}
          >
            {props.title}
          </Box>
          <Box sx={{}}>:</Box>
          <Box sx={{ flex: 'auto' }}>{getCurrency(props.prices || 0)}</Box>
        </Box>
      </TableCell>
    </TableRow>
  )
}

export default PaidStatus
