import * as React from 'react'
import Tabs from '@mui/material/Tabs'
import Tab from '@mui/material/Tab'
import Box from '@mui/material/Box'
import Head from 'next/head'
import { Container, Content, Details } from 'styles/profile'
import DetailProfile from '../details'
import { ContainerReport, ContentReport } from '@/src/styles/profile/report'
import { StyleTitle, TextInfo, Title } from '@/src/styles/profile/edit'
import BookedTable from './booked'
import PostedTable from './posted'
import { FormattedMessage, useIntl } from 'react-intl'
import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'

interface TabPanelProps {
  children?: React.ReactNode
  index: number
  value: number
}
function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  }
}
function ReportProfile() {
  const router = useRouter()

  const [value, setValue] = React.useState(router.query?.from ? 1 : 0)
  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue)
  }
  const { formatMessage } = useIntl()
  const post = formatMessage({ id: 'posted.post' })
  const order = formatMessage({ id: 'posted.order' })
  return (
    <Container>
      <Details>
        <DetailProfile />
      </Details>
      <Content>
        <title>Order office lunch</title>
        <ContainerReport>
          <StyleTitle>
            <Title>
              {' '}
              <FormattedMessage id="report.detail" />
            </Title>
            <TextInfo>
              {' '}
              <FormattedMessage id="report.follow" />
            </TextInfo>
          </StyleTitle>
          <ContentReport>
            <Box sx={{ width: '100%' }}>
              <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                <Tabs
                  value={value}
                  onChange={handleChange}
                  aria-label="basic tabs example"
                >
                  <Tab
                    sx={{ padding: '12px 16px !important' }}
                    label={post}
                    {...a11yProps(0)}
                  />

                  <Tab
                    sx={{ padding: '12px 16px !important' }}
                    label={order}
                    {...a11yProps(1)}
                  />
                </Tabs>
              </Box>
              {value === 0 && (
                <div>
                  <PostedTable />
                </div>
              )}
              {value === 1 && (
                <div>
                  <BookedTable />
                </div>
              )}
            </Box>
          </ContentReport>
        </ContainerReport>
      </Content>
    </Container>
  )
}
export default dynamic(() => Promise.resolve(ReportProfile), {
  ssr: false,
})
