import withRoute from '@/src/hocs/withRoute'
import EditProfile from './edit'

function Profile() {
  return (
    <div>
      <EditProfile />
    </div>
  )
}

export default withRoute(Profile)
