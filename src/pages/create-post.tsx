import React from "react";
import { NextPage } from "next";
import CreatePort from "components/pages/CreatePost";
import withRoute from "../hocs/withRoute";

const CreatePortPage: NextPage = () => {
  return (
    <>
      <CreatePort />
    </>
  );
};

export default withRoute(CreatePortPage);
