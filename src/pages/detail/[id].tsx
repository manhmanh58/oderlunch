import React from 'react'
import { NextPage } from 'next'
import UserProfile from '@/src/components/pages/UserProfile/index'
import withRoute from '@/src/hocs/withRoute'

const CreatePortPage: NextPage = () => {
  return (
    <>
      <UserProfile />
    </>
  )
}

export default withRoute(CreatePortPage)
