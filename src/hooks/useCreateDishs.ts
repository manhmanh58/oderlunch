import { Dishes } from '../utils/type/dishes'
import { useRef, useState } from 'react'
import { dishSchema } from '../constants/dishSchema'

type DishesType = Dishes & {
  erorrs: {
    name?: string
    price?: string
    image?: string
  }
}

const useCreateDishes = (initialDishes?: DishesType[], callBack?: Function) => {
  const [dishes, setDishes] = useState<DishesType[]>(initialDishes || [])
  const numberRef = useRef(1)
  const addDishes = () => {
    let newData = [
      ...dishes,
      {
        name: '',
        price: '',
        image: {
          url: null,
        },
        erorrs: {
          name: '',
          price: '',
          image: '',
        },
        id: numberRef.current++,
      },
    ]
    setDishes(newData)
  }

  const deleteDishes = (index: number) => {
    let dataList = [...dishes]
    dataList.splice(index, 1)
    if (callBack) {
      callBack((prev: number) => ++prev)
    }
    setDishes(dataList)
  }

  const onChangeValue = async (
    body: any,
    index: number,
    type: string | undefined
  ) => {
    if (type === 'image') {
      let clonedData: DishesType[] = [...dishes]
      clonedData[index].image!.url = body
      setDishes(clonedData)
    } else {
      const { name, value }: { name: keyof Dishes; value: never } = body.target
      let error = ''
      try {
        const validate = await dishSchema.validateAt(`${name}`, {
          [name]: value,
        })
        error = ''
      } catch (errors: any) {
        error = errors.toString().split(':')?.[1]?.trim()
      }
      let clonedData: DishesType[] = [...dishes]

      clonedData[index][name] = value
      if (!clonedData[index].erorrs) {
        clonedData[index].erorrs = {
          name: '',
          price: '',
          image: '',
        }
      }
      clonedData[index].erorrs[name] = error
      setDishes(clonedData)
    }
  }
  return { dishes, addDishes, deleteDishes, onChangeValue }
}
export default useCreateDishes
