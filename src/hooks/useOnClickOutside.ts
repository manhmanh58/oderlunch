import { useEffect } from 'react'

export default function useOnClickOutside(
  ref: React.RefObject<HTMLElement>,
  handler: Function
) {
  useEffect(() => {
    const listener: any = (event: MouseEvent) => {
      if (!ref.current || ref.current.contains(event.target as HTMLElement)) {
        return
      }
      handler(event)
    }
    document.addEventListener('mousedown', listener)
    document.addEventListener('touchstart', listener)
    return () => {
      document.removeEventListener('mousedown', listener)
      document.removeEventListener('touchstart', listener)
    }
  }, [ref, handler])
}
